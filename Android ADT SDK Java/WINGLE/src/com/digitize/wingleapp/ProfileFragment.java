package com.digitize.wingleapp;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.util.AQUtility;
import com.digitize.wingleapp.imagepart.FileCache;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

public class ProfileFragment extends Fragment {

	ImageView imgEditInterest, imgEditBars;
	EditText edtInterest, edtBars;
	boolean interestEnable = false, barsEnable = false;
	AQuery aQuery = null;
	public int stub_id = R.drawable.noimage;
	private int screenHeight = 0, screenWidth = 0;

	public void getScreenParams() {
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.profile_fragment, null);
		imgEditInterest = (ImageView) view.findViewById(R.id.img_edit_interest);
		imgEditBars = (ImageView) view.findViewById(R.id.img_edit_bars);
		edtInterest = (EditText) view.findViewById(R.id.edt_interests);
		edtBars = (EditText) view.findViewById(R.id.edt_bars_user_go);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		imgEditInterest.setOnClickListener(editInterestListener);
		imgEditBars.setOnClickListener(editBarsListener);
		edtBars.setEnabled(false);
		edtBars.setBackgroundColor(Color.TRANSPARENT);
		edtInterest.setEnabled(false);
		edtInterest.setBackgroundColor(Color.TRANSPARENT);
		aQuery = new AQuery(getActivity());
		getScreenParams();
	}

	public OnClickListener editInterestListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (interestEnable) {
				edtInterest.setBackgroundColor(Color.TRANSPARENT);
				edtInterest.setEnabled(false);
				interestEnable = false;
			} else {
				edtInterest.setEnabled(true);
				edtInterest.setBackgroundResource(R.drawable.input_field_style);
				interestEnable = true;
			}
		}
	};
	public OnClickListener editBarsListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (barsEnable) {
				edtBars.setBackgroundColor(Color.TRANSPARENT);
				edtBars.setEnabled(false);
				barsEnable = false;
			} else {
				edtBars.setEnabled(true);
				edtBars.setBackgroundResource(R.drawable.input_field_style);
				barsEnable = true;
			}
		}
	};

}
