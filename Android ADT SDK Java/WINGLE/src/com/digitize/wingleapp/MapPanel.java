package com.digitize.wingleapp;

import com.digitize.wingleapp.places.PlaceItem;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MapPanel extends LinearLayout {

	private TextView txtDistance;
	private TextView txtPeopleCount;
	private Button txtRefresh;
	TextView txtHeading;
	Button btnComeHere, btnDrawRoot, btnSeePeople;;
	OnClickListener comeHereBtnListener;
	OnClickListener drawRootBtnListener;
	OnClickListener seePeopleBtnListener;
	OnClickListener refreshListener;

	public static final String COMING_HERE_CAPTION = "Coming here";
	public static final String CANCEL_CAPTION = "Cancel";

	private PlaceItem placeItemInstanceVO = null;

	public PlaceItem getPlaceItemInstanceVO() {
		return placeItemInstanceVO;
	}

	public void setPlaceItemInstanceVO(PlaceItem placeItemInstanceVO) {
		this.placeItemInstanceVO = placeItemInstanceVO;
	}

	public void setLocalPlaceId(long localPlaceId) {
		placeItemInstanceVO.setLocalPlaceId(localPlaceId);
	}

	public void setPeopleCount(int count) {
		txtPeopleCount.setText(String.valueOf(count));
	}

	public MapPanel(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public void setDrawRouteButtonText(String text) {
		btnDrawRoot.setText(text);
	}

	public void setComeRouteText(String comeHereText) {
		btnComeHere.setText(comeHereText);
	}

	public void setComeHereListener(OnClickListener comeHereListener) {
		this.comeHereBtnListener = comeHereListener;
	}

	public void setDrawRootListener(OnClickListener drawRootListener) {
		this.drawRootBtnListener = drawRootListener;
	}

	public void setSeePeopleListener(OnClickListener seePeopleListener) {
		this.seePeopleBtnListener = seePeopleListener;
	}

	public void setRefreshListener(OnClickListener seePeopleListener) {
		this.refreshListener = seePeopleListener;
		txtRefresh.setOnClickListener(refreshListener);
	}

	public MapPanel(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();
		((Activity) getContext()).getLayoutInflater().inflate(
				R.layout.map_panel, this);
		txtHeading = (TextView) findViewById(R.id.txt_heading);
		txtDistance = (TextView) findViewById(R.id.txt_distance);
		txtPeopleCount = (TextView) findViewById(R.id.txt_peaople_count);
		txtRefresh = (Button) findViewById(R.id.txt_refresh);
		btnComeHere = (Button) findViewById(R.id.btn_come_here);
		btnDrawRoot = (Button) findViewById(R.id.btn_draw_root);
		btnSeePeople = (Button) findViewById(R.id.btn_see_people);

	}

	public void setDataInFields(String name, String distance, String people,
			String popularity) {
		txtHeading.setText(name);
		txtDistance.setText(distance);
		txtPeopleCount.setText(people);
		txtRefresh.setText(popularity);
		btnComeHere.setOnClickListener(comeHereBtnListener);
		btnDrawRoot.setOnClickListener(drawRootBtnListener);
		btnSeePeople.setOnClickListener(seePeopleBtnListener);
	}

}
