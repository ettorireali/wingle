package com.digitize.wingleapp;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MainNewsAdapter extends BaseAdapter {
	// This is the custom adapter for displaying the labdetails in listview of a
	// dialog inside the activity
	String picsDownloadFolder = Environment.getExternalStorageDirectory() + "/"
			+ "Congress" + "/", imageLinkBuilder;
	private Activity activity;

	private LayoutInflater inflater = null;

	/*
	 * private List<AllNotificationInfo> data; int myPosition; int
	 * totalNumAttackers = 0, totalNumDamage = 0; public Boolean bb[]; int
	 * element; DbUtils dbutil; String imgFolderPath =
	 * Environment.getExternalStorageDirectory() + "/" + "Congress/" ; File
	 * LocImageFile; WindowManager.LayoutParams WMLP; List<AllNotificationInfo>
	 * notificationList = new ArrayList<AllNotificationInfo>();
	 */

	public MainNewsAdapter(Activity a) {

		activity = a;

		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		/*
		 * dbutil = new DbUtils(); notificationList =
		 * dbutil.getAllNotificationInfo(); //data=notificationList; data =
		 * reverseList(notificationList);
		 */
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		// return data.size();
		return 4;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder viewHolder;
		// final AllNotificationInfo array = data.get(position);
		/*
		 * if (convertView == null) { viewHolder = new ViewHolder(); convertView
		 * = inflater.inflate(R.layout.news_feeds_single_item, null);
		 * viewHolder.textInfo = (TextView) convertView
		 * .findViewById(R.id.message); viewHolder.headlineInfo = (TextView)
		 * convertView .findViewById(R.id.headline); viewHolder.dateInfo =
		 * (TextView) convertView .findViewById(R.id.dateInfo);
		 * viewHolder.timeInfo = (TextView) convertView
		 * .findViewById(R.id.timeInfo); viewHolder.imageView = (ImageView)
		 * convertView .findViewById(R.id.imgView);
		 * convertView.setTag(viewHolder);
		 * 
		 * } else { // prevent recycling of views viewHolder = (ViewHolder)
		 * convertView.getTag(); }
		 */
		/*
		 * Log.d("CRASH_MONITOR", "1"+array.gettext_info());
		 * viewHolder.textInfo.setText(array.gettext_info());
		 * Log.d("CRASH_MONITOR", "2"+array.getheadline_info());
		 * viewHolder.headlineInfo.setText(array.getheadline_info());
		 * Log.d("CRASH_MONITOR", "3"+array.getdate_info());
		 * viewHolder.dateInfo.setText(array.getdate_info());
		 * Log.d("CRASH_MONITOR", "4"+array.gettime_info());
		 * viewHolder.timeInfo.setText(array.gettime_info());
		 * Log.d("CRASH_MONITOR", "5"+array.getimageurl_info());
		 * if(array.gettype_info().equals("Video")){
		 * viewHolder.imageView.setImageResource(R.drawable.video_image);
		 * 
		 * } if(array.gettype_info().equals("Image")){ LocImageFile = new
		 * File(imgFolderPath + array.getimageurl_info() );
		 * viewHolder.imageView.setImageURI(Uri .fromFile(LocImageFile)); }
		 */
		/*
		 * convertView.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub if(array.gettype_info().equals("Video")){
		 * 
		 * Intent browserIntent = new Intent(Intent.ACTION_VIEW,
		 * Uri.parse(array.getvideourl_info()));
		 * activity.startActivity(browserIntent); }
		 * if(array.gettype_info().equals("Image")){ AlertDialog.Builder builder
		 * = new AlertDialog.Builder(activity); LayoutInflater inflater =
		 * (LayoutInflater) activity
		 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE); View view =
		 * inflater.inflate(R.layout.dialog_layout_listview, null); ImageView iv
		 * = (ImageView) view.findViewById(R.id.imageView1);
		 * 
		 * TextView headline = (TextView) view.findViewById(R.id.textView1);
		 * headline.setText(array.getheadline_info()); TextView message =
		 * (TextView) view.findViewById(R.id.textView2);
		 * message.setText(array.gettext_info());
		 * iv.setImageURI(Uri.parse(picsDownloadFolder
		 * +array.getimageurl_info()));
		 * 
		 * builder.setView(view);
		 * 
		 * builder.show(); } } });
		 */

		return convertView;
	}

	// view holder class
	public static class ViewHolder {

		// view hlder class
		public TextView textInfo;
		public TextView headlineInfo;
		public TextView dateInfo;
		public TextView timeInfo;
		public ImageView imageView;

	}
	/*
	 * public List<AllNotificationInfo> reverseList(List<AllNotificationInfo>
	 * myList) { List<AllNotificationInfo> invertedList = new
	 * ArrayList<AllNotificationInfo>(); for (int i = myList.size() - 1; i >= 0;
	 * i--) { invertedList.add(myList.get(i)); } return invertedList; }
	 */
}
