package com.digitize.wingleapp;

import java.util.ArrayList;
import java.util.Iterator;

import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.UpdateProfileService;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class AboutMeFragment extends Fragment implements ServiceCallback {

	EditText edtInterest;
	boolean likesEnable = false;
	ImageView imgEditInterest;
	UpdateProfileService updateProfileService;
	ProgressDialog progressDialog;
	WingleDB wingleDB;
	ArrayList<ProfileData> profileDataList;
	String editTextContent = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.about_me_fragment, null);
		imgEditInterest = (ImageView) view.findViewById(R.id.img_edit_interest);
		edtInterest = (EditText) view.findViewById(R.id.edt_interests);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		progressDialog.setMessage("Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		wingleDB = new WingleDB(getActivity());
		imgEditInterest.setOnClickListener(editInterestListener);
		edtInterest.setEnabled(false);
		edtInterest.setBackgroundColor(Color.TRANSPARENT);
		updateProfileService = new UpdateProfileService(AboutMeFragment.this,
				getActivity());
		try {
			wingleDB.open();
			profileDataList = wingleDB.getUserData(GlobalApp
					.getUidFromSharedPreference(getActivity()));
			wingleDB.close();
			Iterator<ProfileData> profileIterator = profileDataList.iterator();
			ProfileData profileData;
			while (profileIterator.hasNext()) {
				profileData = profileIterator.next();
				edtInterest.setText(profileData.getAboutMe());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public OnClickListener editInterestListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (likesEnable) {
				updateProfileService
						.setUpdateOption(UpdateProfileService.UPDATE_ABOUT_ME);
				updateProfileService.setAboutMe(edtInterest.getText()
						.toString().trim());
				updateProfileService.setUid(GlobalApp
						.getUidFromSharedPreference(getActivity()));
				if (!editTextContent.equalsIgnoreCase(edtInterest.getText()
						.toString().trim())) {
					new Thread(updateProfileService).start();
				} else {

				}
				edtInterest.setBackgroundColor(Color.TRANSPARENT);
				edtInterest.setEnabled(false);
				edtInterest.setTextColor(Color.parseColor("#ffffff"));
				likesEnable = false;
			} else {
				edtInterest.setEnabled(true);
				edtInterest.setBackgroundResource(R.drawable.input_field_style);
				edtInterest.setTextColor(Color.parseColor("#000000"));
				likesEnable = true;
				editTextContent = edtInterest.getText().toString().trim();
			}
		}
	};

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("updateprofile_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("update_success")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
				}
			});
		} else if (msg.equalsIgnoreCase("update_problem")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Problem updating profile",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase("params_missing")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Some params missing",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase("error")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Connection problem",
							Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}
}
