package com.digitize.wingleapp;

import java.util.ArrayList;
import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.pojo.NewsFeedData;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class NewsFeedFragment extends Fragment {

	ListView newsfeedList;
	ArrayList<NewsFeedData> newsFeedArrList;
	WingleDB wingleDB;
	NewsFeedAdapter newsFeedAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.news_feeds_list, null);
		newsfeedList = (ListView) view.findViewById(R.id.newsfeed_list);
		newsfeedList.setEmptyView(view.findViewById(R.id.empty));
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		wingleDB = new WingleDB(getActivity());
		try {
			wingleDB.open();
			wingleDB.deleteNewsFeed(1);
			wingleDB.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		newsFeedAdapter = new NewsFeedAdapter(getActivity());
		for (int i = 0; i < 5; i++) {
			NewsFeedData tempData = new NewsFeedData();
			tempData.setHeadline("Demo NewsFeed");
			tempData.setNewsType("Precheckin");
			tempData.setLatitude(70.12345);
			tempData.setLongitude(22.12345);
			tempData.setTime("18 June,2014");
			tempData.setWhoId(8);
			tempData.setWhomId(21);
			tempData.setMessage("This is demo message testing,This is demo message testing,This is demo message testing");
			tempData.setTransactionId(1);
			tempData.setIsRead(0);
			wingleDB.open();
			wingleDB.insertIntoNewsfeed(tempData);
			wingleDB.close();

		}
		wingleDB.open();
		newsFeedArrList = wingleDB.getNewsFeedData();
		wingleDB.close();
		newsFeedAdapter.setNewsData(newsFeedArrList);
		newsfeedList.setAdapter(newsFeedAdapter);
		newsFeedAdapter.notifyDataSetChanged();
	}
}