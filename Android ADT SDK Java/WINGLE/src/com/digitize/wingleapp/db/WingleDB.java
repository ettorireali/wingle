package com.digitize.wingleapp.db;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.digitize.wingleapp.places.PlaceItem;
import com.digitize.wingleapp.pojo.NewsFeedData;
import com.digitize.wingleapp.pojo.PlaceBookVO;
import com.digitize.wingleapp.pojo.ProfileData;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import at.vcity.androidim.types.MessageInfo;

public class WingleDB {
	// _id pid lat lon place_info google_search_id noted_date_time live_place_id
	SQLiteDatabase sdb = null;
	DBHelper dbh = null;
	Context con = null;
	private static final String DATABASE_NAME = "db_wingle";
	private static final int DATABASE_VERSION = 3;
	private static final String DATABASE_TABLE_NAME_APP_COMMON = "app_common";
	private static final String DATABASE_TABLE_NAME_PLACE_DATA = "place_book";
	private static final String DATABASE_TABLE_NAME_USER_DATA = "user_data";
	private static final String DATABASE_TABLE_NAME_NEWSFEED_DATA = "newsfeed_data";
	private static final String DATABASE_TABLE_NAME_CHAT_HISTORY = "chat_history";

	public static String appCommonTableQuery = "create table "
			+ DATABASE_TABLE_NAME_APP_COMMON
			+ "(key TEXT primary key,value TEXT)";
	public static String placeInfoTableQuery = "create table "
			+ DATABASE_TABLE_NAME_PLACE_DATA
			+ "(_id integer primary key autoincrement,person_id INTEGER,latitude TEXT,longitude TEXT,place_info TEXT,google_search_id TEXT,noted_time long,live_place_id TEXT)";
	public static String userDataTableQuery = "create table "
			+ DATABASE_TABLE_NAME_USER_DATA
			+ "(user_id INTEGER,firstname TEXT,lastname TEXT,username TEXT primary key,email TEXT,gender INTEGER,dob TEXT,likes TEXT,dislikes TEXT,fav_bars TEXT,date_created TEXT,device_id TEXT,status INTEGER,tagline TEXT,shown_others INTEGER,is_login INTEGER,image_path TEXT,about_me TEXT)";
	public static String newsfeedTableQuery = "create table "
			+ DATABASE_TABLE_NAME_NEWSFEED_DATA
			+ "(_id integer primary key autoincrement,news_type TEXT,lat TEXT,lon TEXT,headline TEXT,time_of_occurance TEXT,who_id integer,whom_id integer,message TEXT,transaction_id integer,is_read integer)";
	public static String chatHistoryTableQuery = "create table "
			+ DATABASE_TABLE_NAME_CHAT_HISTORY
			+ "(_id integer primary key autoincrement,sender TEXT,receiver TEXT,message TEXT,timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)";

	public WingleDB(Context ctx) {
		con = ctx;
		dbh = new DBHelper(ctx);
	}

	public void open() {
		sdb = dbh.getWritableDatabase();
	}

	public void close() {
		dbh.close();
	}

	private class DBHelper extends SQLiteOpenHelper {
		public DBHelper(Context ctx) {
			super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(placeInfoTableQuery);
			db.execSQL(userDataTableQuery);
			db.execSQL(newsfeedTableQuery);
			db.execSQL(chatHistoryTableQuery);
			db.execSQL(appCommonTableQuery);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("Drop table if exists " + DATABASE_TABLE_NAME_PLACE_DATA);
			db.execSQL("Drop table if exists " + DATABASE_TABLE_NAME_USER_DATA);
			db.execSQL("Drop table if exists "
					+ DATABASE_TABLE_NAME_NEWSFEED_DATA);
			db.execSQL("Drop table if exists "
					+ DATABASE_TABLE_NAME_CHAT_HISTORY);
			db.execSQL("Drop table if exists "
					+ DATABASE_TABLE_NAME_APP_COMMON);
			onCreate(db);
		}
	}

	public long addChat(MessageInfo pMsgInfo) {
		ContentValues values = new ContentValues();
		values.put("sender", pMsgInfo.getSenderId());
		values.put("receiver", pMsgInfo.getRecipientId());
		values.put("message", pMsgInfo.getMessage());
		return sdb.insert(DATABASE_TABLE_NAME_CHAT_HISTORY, null,values);
		// TODO Auto-generated method stub

	}
	public ArrayList<Object> getChatHistoryForSender(String user) {
		ArrayList<Object> chatHistoryForUser = new ArrayList<Object>();
		GregorianCalendar prev = null;
		MessageInfo object;
		String query = "SELECT * FROM "
				+ DATABASE_TABLE_NAME_CHAT_HISTORY + " where sender='" + user+"' OR receiver='"+user+"' order by datetime(timestamp) ASC";
		Cursor c = sdb.rawQuery(query, null);
		while (c.moveToNext()) {
			object = new MessageInfo();
			object.setMessage(c.getString(c.getColumnIndex("message")));
			object.setSenderId(c.getString(c.getColumnIndex("sender")));
			object.setRecipientId(c.getString(c.getColumnIndex("receiver")));
			object.setTimeinmillis(Timestamp.valueOf(c.getString(c.getColumnIndex("timestamp"))).getTime());
			GregorianCalendar curr = new GregorianCalendar();
			curr.setTimeInMillis(object.getTimeinmillis());
			curr.set(Calendar.HOUR_OF_DAY, 0);
			curr.set(Calendar.MINUTE, 0);
			curr.set(Calendar.SECOND, 0);
			curr.set(Calendar.MILLISECOND, 0);
			if(prev == null || curr.after(prev)){
				chatHistoryForUser.add(curr.getTime());
			}
			prev = new GregorianCalendar();
			prev.setTimeInMillis(object.getTimeinmillis());
			prev.set(Calendar.HOUR_OF_DAY, 0);
			prev.set(Calendar.MINUTE, 0);
			prev.set(Calendar.SECOND, 0);
			prev.set(Calendar.MILLISECOND, 0);
			chatHistoryForUser.add(object);
		}
		c.close();
		return chatHistoryForUser;
		// TODO Auto-generated method stub

	}
	public boolean exists(int livePlaceId) {
		boolean dataToReturn = false;
		String query = "SELECT * FROM " + DATABASE_TABLE_NAME_PLACE_DATA
				+ " WHERE live_place_id=" + livePlaceId;
		Cursor cursor = sdb.rawQuery(query, null);
		while (cursor.moveToNext()) {
			dataToReturn = true;
		}
		return dataToReturn;
	}

	/**
	 * @param : id userid
	 * @return : profile pic image based on uid
	 */
	
	public void putAppCommon(String key, String value){
		ContentValues values = new ContentValues();
		values.put("value", value);
		values.put("key", key);
		sdb.insertWithOnConflict(DATABASE_TABLE_NAME_APP_COMMON, null,values, SQLiteDatabase.CONFLICT_REPLACE);
	}
			
	public String getAppCommon(String key){
		String dataToReturn = null;
		String query = "SELECT value FROM "
				+ DATABASE_TABLE_NAME_APP_COMMON + " where key='" + key+"'";
		Cursor c = sdb.rawQuery(query, null);
		while (c.moveToNext()) {
			dataToReturn = c.getString(c.getColumnIndex("value"));
		}
		c.close();
		return dataToReturn;
	}


	public String getProfilePic(String uid) {
		String dataToReturn = null;
		String query = "SELECT image_path FROM "
				+ DATABASE_TABLE_NAME_USER_DATA + " where user_id=" + uid;
		Cursor c = sdb.rawQuery(query, null);
		while (c.moveToNext()) {
			dataToReturn = c.getString(c.getColumnIndex("image_path"));
		}
		c.close();
		return dataToReturn;
	}

	public long addPlaceToVisit(PlaceBookVO placeInstance) {
		ContentValues values = new ContentValues();
		values.put("person_id", placeInstance.getPersonId());
		Log.w("placebook_from_db", "person_id :" + placeInstance.getPersonId());
		values.put("latitude", placeInstance.getPos().latitude);
		values.put("longitude", placeInstance.getPos().longitude);
		values.put("place_info", placeInstance.getPlaceInfo());
		Log.w("placebook_from_db",
				"place info :" + placeInstance.getPlaceInfo());
		values.put("google_search_id", placeInstance.getGoogleSearchId());
		values.put("noted_time", placeInstance.getNotedTime());
		values.put("live_place_id", placeInstance.getLivePlaceId());
		return sdb.insert(DATABASE_TABLE_NAME_PLACE_DATA, null, values);
	}

	public long insertIntoNewsfeed(NewsFeedData newsFeedData) {
		ContentValues v = new ContentValues();
		v.put("news_type", newsFeedData.getNewsType());
		v.put("lat", newsFeedData.getLatitude());
		v.put("lon", newsFeedData.getLongitude());
		v.put("headline", newsFeedData.getHeadline());
		v.put("time_of_occurance", newsFeedData.getTime());
		v.put("who_id", newsFeedData.getWhoId());
		v.put("whom_id", newsFeedData.getWhomId());
		v.put("message", newsFeedData.getMessage());
		v.put("transaction_id", newsFeedData.getTransactionId());
		v.put("is_read", newsFeedData.getIsRead());

		return sdb.insert(DATABASE_TABLE_NAME_NEWSFEED_DATA, null, v);
	}

	public void deleteNewsFeed(long transactionId) {
		String query = "DELETE FROM " + DATABASE_TABLE_NAME_NEWSFEED_DATA
				+ " WHERE transaction_id=" + transactionId;
		sdb.execSQL(query);
	}

	public ArrayList<NewsFeedData> getNewsFeedData() {
		ArrayList<NewsFeedData> newsData = new ArrayList<NewsFeedData>();
		String query = "SELECT * FROM " + DATABASE_TABLE_NAME_NEWSFEED_DATA;
		Cursor c = sdb.rawQuery(query, null);
		while (c.moveToNext()) {
			NewsFeedData newsFeedData = new NewsFeedData();
			newsFeedData.setHeadline(c.getString(1));
			newsFeedData.setLatitude(Double.parseDouble(c.getString(2)));
			newsFeedData.setLongitude(Double.parseDouble(c.getString(3)));
			newsFeedData.setHeadline(c.getString(4));
			newsFeedData.setTime(c.getString(5));
			newsFeedData.setWhoId(c.getInt(6));
			newsFeedData.setWhomId(c.getInt(7));
			newsFeedData.setMessage(c.getString(8));
			newsFeedData.setTransactionId(c.getInt(9));
			newsFeedData.setIsRead(c.getInt(10));
			newsData.add(newsFeedData);
		}
		c.close();
		return newsData;
	}

	public long addUserData(ProfileData profileData) {
		ContentValues val = new ContentValues();
		val.put("user_id", profileData.getUid());
		val.put("firstname", profileData.getFirstName());
		val.put("lastname", profileData.getLastName());
		val.put("username", profileData.getUserName());
		val.put("email", profileData.getEmail());
		val.put("gender", profileData.getGender());
		val.put("dob", profileData.getDob());
		val.put("likes", profileData.getLikes());
		val.put("dislikes", profileData.getDislike());
		val.put("fav_bars", profileData.getFavBars());
		val.put("date_created", profileData.getDateCreated());
		val.put("device_id", profileData.getDeviceId());
		val.put("status", profileData.getStatus());
		val.put("tagline", profileData.getTagline());
		val.put("shown_others", profileData.getIsShownToOthers());
		val.put("is_login", profileData.getIsLogin());
		val.put("image_path", profileData.getImagePath());
		val.put("about_me", profileData.getAboutMe());

		return sdb.insertWithOnConflict(DATABASE_TABLE_NAME_USER_DATA, null, val,SQLiteDatabase.CONFLICT_REPLACE);
	}

	/**
	 * @param userId
	 * @return
	 */
	public ArrayList<ProfileData> getUserData(String userId) {
		ArrayList<ProfileData> userListData = new ArrayList<ProfileData>();
		String query = "SELECT * FROM " + DATABASE_TABLE_NAME_USER_DATA
				+ " where user_id='" + userId + "'";
		Cursor c = sdb.rawQuery(query, null);
		while (c.moveToNext()) {
			ProfileData userData = new ProfileData();
			userData.setUid(String.valueOf(c.getInt(0)));
			userData.setFirstName(c.getString(1));
			userData.setLastName(c.getString(2));
			userData.setUserName(c.getString(3));
			userData.setEmail(c.getString(4));
			userData.setGender(c.getInt(5));
			userData.setDob(c.getString(6));
			userData.setLikes(c.getString(7));
			userData.setDislike(c.getString(8));
			userData.setFavBars(c.getString(9));
			userData.setDateCreated(c.getString(10));
			userData.setDeviceId(c.getString(11));
			userData.setStatus(c.getString(12));
			userData.setTagline(c.getString(13));
			userData.setIsShownToOthers(c.getInt(14));
			userData.setIsLogin(c.getInt(15));
			userData.setImagePath(c.getString(16));
			userData.setAboutMe(c.getString(17));
			userListData.add(userData);
		}
		c.close();
		return userListData;
	}
	
	public ProfileData getUserDataForUserName(String username) {
		// TODO Auto-generated method stub
		ProfileData userData = null;
		ArrayList<ProfileData> userListData = new ArrayList<ProfileData>();
		String query = "SELECT * FROM " + DATABASE_TABLE_NAME_USER_DATA
				+ " where username='" + username + "'";
		Cursor c = sdb.rawQuery(query, null);
		while (c.moveToNext()) {
			userData = new ProfileData();
			userData.setUid(String.valueOf(c.getInt(0)));
			userData.setFirstName(c.getString(1));
			userData.setLastName(c.getString(2));
			userData.setUserName(c.getString(3));
			userData.setEmail(c.getString(4));
			userData.setGender(c.getInt(5));
			userData.setDob(c.getString(6));
			userData.setLikes(c.getString(7));
			userData.setDislike(c.getString(8));
			userData.setFavBars(c.getString(9));
			userData.setDateCreated(c.getString(10));
			userData.setDeviceId(c.getString(11));
			userData.setStatus(c.getString(12));
			userData.setTagline(c.getString(13));
			userData.setIsShownToOthers(c.getInt(14));
			userData.setIsLogin(c.getInt(15));
			userData.setImagePath(c.getString(16));
			userData.setAboutMe(c.getString(17));
			userListData.add(userData);
		}
		c.close();
		return userData;
		
	}

	public void associatePlaceData(PlaceItem placeInstance, String personID) {
		ArrayList<PlaceBookVO> placeData = getPlaceHistory(personID);
		Iterator<PlaceBookVO> placeIterator = placeData.iterator();
		PlaceBookVO placeBookInstance = null;
		double distance = 0;
		while (placeIterator.hasNext()) {
			placeBookInstance = placeIterator.next();
			distance = Math.abs(SphericalUtil.computeDistanceBetween(
					placeBookInstance.getPos(), placeInstance.getPosition()));
			if (distance < 2) {
				placeInstance.associateLocalPlaceData(placeBookInstance);
				placeInstance.addMe(personID);
				placeInstance.increaseCounter();
				Log.w("association", "distance " + distance);
			}
		}
	}

	public boolean associatePlaceData(ArrayList<PlaceItem> placeItemCollection,
			String personID) {
		ArrayList<PlaceBookVO> placeData = getPlaceHistory(personID);
		boolean dataToReturn = true;
		if (placeData.size() > 0) {
			dataToReturn = true;
			Iterator<PlaceBookVO> placeIterator = placeData.iterator();
			PlaceBookVO placeBookInstance = null;

			Iterator<PlaceItem> placeItemIterator = null;
			PlaceItem placeItem = null;

			double distance = 0;
			while (placeIterator.hasNext()) {
				placeBookInstance = placeIterator.next();
				placeItemIterator = placeItemCollection.iterator();
				while (placeItemIterator.hasNext()) {
					placeItem = placeItemIterator.next();
					distance = Math
							.abs(SphericalUtil.computeDistanceBetween(
									placeBookInstance.getPos(),
									placeItem.getPosition()));
					if (distance < 2) {
						placeItem.addMe(personID);
						placeItem.associateLocalPlaceData(placeBookInstance);
						placeItem.increaseCounter();
						Log.w("association", "distance " + distance);
					}
				}
			}
		}
		Log.w("association", "association done");
		return dataToReturn;
	}

	public boolean isEntryExists(String personId, LatLng latLng) {
		boolean dataToReturn = false;
		ArrayList<PlaceBookVO> placeData = getPlaceHistory(personId);
		Iterator<PlaceBookVO> placeIterator = placeData.iterator();
		PlaceBookVO placeBookInstance = null;
		double distance = 0;
		while (placeIterator.hasNext()) {
			placeBookInstance = placeIterator.next();
			distance = Math.abs(SphericalUtil.computeDistanceBetween(
					placeBookInstance.getPos(), latLng));
			if (distance < 2) {
				dataToReturn = true;
				break;
			}
		}
		return dataToReturn;
	}

	public ArrayList<PlaceBookVO> getPlaceHistory(String personId) {
		ArrayList<PlaceBookVO> orderData = new ArrayList<PlaceBookVO>();
		String q = "SELECT * FROM " + DATABASE_TABLE_NAME_PLACE_DATA
				+ " where person_id='" + personId + "'";
		Cursor cur = sdb.rawQuery(q, null);

		while (cur.moveToNext()) {
			PlaceBookVO instance = new PlaceBookVO();
			instance.setLocalPlaceId(cur.getInt(0));
			instance.setPersonId(cur.getInt(1));
			instance.setPos(new LatLng(Double.parseDouble(cur.getString(2)),
					Double.parseDouble(cur.getString(3))));
			instance.setPlaceInfo(cur.getString(4));
			instance.setGoogleSearchId(cur.getString(5));
			instance.setNotedTime(cur.getLong(6));
			instance.setLivePlaceId(cur.getLong(7));
			orderData.add(instance);
		}
		Log.w("place_list_from_db", "place list data : " + orderData.size());
		cur.close();
		return orderData;
	}

	public void updatePlaceLiveId(long localPlaceId, long livePlaceId) {
		String query = "UPDATE " + DATABASE_TABLE_NAME_PLACE_DATA
				+ " SET live_place_id=" + livePlaceId + " WHERE _id="
				+ localPlaceId;
		sdb.execSQL(query);
	}

	/**
	 * @description : deleting placeinfo
	 */
	public void deletePlaceInfo(long livePlaceId) {
		String query = "DELETE FROM " + DATABASE_TABLE_NAME_PLACE_DATA
				+ " WHERE live_place_id=" + livePlaceId;
		sdb.execSQL(query);
	}

	/**
	 * @description : updating image path based on key
	 */
	public void updateMediaPath(String uid, String imagePath) {
		String query = "UPDATE " + DATABASE_TABLE_NAME_USER_DATA
				+ " SET image_path='" + imagePath + "' WHERE user_id=" + uid;
		sdb.execSQL(query);
	}


}
