package com.digitize.wingleapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.pojo.LocationUtils;
import com.digitize.wingleapp.services.PlaceDecideService;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.UserActionService;
import com.digitize.wingleapp.services.UserService;
import com.digitize.wingleapp.services.Verifier;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks, LocationListener,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, ServiceCallback,
		LoaderCallbacks<Cursor> {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;
	NearbyFragment nearbyFragment = null;
	ProfileFragment profileFragment = null;
	RandomPlacesFragment randomPlacesFragment = null;
	MainPlacesFragment mainPlacesFragment = null;
	SettingsFragment settingFragment = null;
	MapFragment mapFragment = null;
	NearestFriendFragment friendsFragment = null;
	UserService userService;
	CustomDialog customAlertDialog;
	File f = null;
	boolean isPlacesSearch = true;

	ProgressDialog progressDialog;
	// A request to connect to Location Services
	private LocationRequest mLocationRequest;
	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;
	private LocationManager mlocationManager;
	private UserActionService userActionService = null;
	SearchManager searchManager = null;
	SearchView searchView = null;
	SearchView.SearchAutoComplete searchAutComp = null;
	private boolean firstTimeIconfied = true;
	LatLng myLatLon = null;
	Verifier verifier = null;
	NewsFeedFragment newsFeedFragment = null;
	WingleDB wingleDB;
	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	boolean call = false; // to make location call only once for now

	public void getScreenSize() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		Log.w("density", "" + displaymetrics.density);
		GlobalApp.screenHeight = displaymetrics.heightPixels;
		GlobalApp.screenWidth = displaymetrics.widthPixels;
	}

	// Broadcast receiver for getting message from GCM
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.w("wingle_gcm", "inreceive");
			String newMessage = intent.getExtras().getString(
					CommonUtilities.EXTRA_MESSAGE);
			String actionType = null;
			try {
				JSONObject parentObj = new JSONObject(newMessage);
				actionType = parentObj.getString("action_type");
				if (actionType.equals(UserActionService.ACTION_FOLLOW)) {
					isFollowAction(parentObj);
				} else if (actionType.equals(UserActionService.ACTION_UNFOLLOW)) {
					isUnfollowAction(parentObj);
				} else if (actionType.equals(UserActionService.ACTION_BLOCK)) {
					isBlockAction(parentObj);
				} else if (actionType.equals(UserActionService.ACTION_UNBLOCK)) {
					isUnblockAction(parentObj);
				} else if (actionType
						.equals(PlaceDecideService.PRECHECKIN_ACTION)) {
					isPrecheckin(parentObj);
				}

			} catch (JSONException jse) {
				Log.w("wingle_gcm", jse.toString());
			}
		}
	};

	public void isPrecheckinCancel(JSONObject obj) {
		try {
			String whoId = obj.getString("who");
			Log.w("action", "follow action " + whoId);
		} catch (JSONException jex) {

		}
	}

	/**
	 * @description : it can happen that if multi users login from the same
	 *              device , you need to check AM I Following that particular
	 *              user then only My Device should be updated..for that I will
	 *              ask to verifier object
	 */
	public void isPrecheckin(JSONObject obj) {
		try {

			String whoId = obj.getString("who");

			if (verifier.isFollowing(whoId)) {
				mapFragment.callProximityService(obj.getDouble("lat"),
						obj.getDouble("lon"));
			}
			String msg = obj.getString("message");
			Toast.makeText(getBaseContext(), whoId + " " + msg,
					Toast.LENGTH_LONG).show();
		} catch (JSONException jex) {

		}
	}

	public void isFollowAction(JSONObject obj) {
		try {
			String whoId = obj.getString("who");
			String whom = obj.getString("whom");
			Log.w("action", "follow action " + whoId);
			if (whom.equals(GlobalApp.getUidFromSharedPreference(this))) {
				Toast.makeText(getBaseContext(),
						whoId + " has started following you..",
						Toast.LENGTH_LONG).show();
			}
		} catch (JSONException jex) {

		}
	}

	public void isUnfollowAction(JSONObject obj) {
		try {
			String whoId = obj.getString("who");
			String whom = obj.getString("whom");
			if (whom.equals(GlobalApp.getUidFromSharedPreference(this))) {
				Toast.makeText(getBaseContext(),
						whoId + " has started unfollowed..", Toast.LENGTH_LONG)
						.show();
			}
		} catch (JSONException jex) {

		}
	}

	public void isBlockAction(JSONObject obj) {
		try {
			String whoId = obj.getString("who");
			String whom = obj.getString("whom");
			if (whom.equals(GlobalApp.getUidFromSharedPreference(this))) {
				Toast.makeText(getBaseContext(), whoId + " has blocked..",
						Toast.LENGTH_LONG).show();
			}
		} catch (JSONException jex) {

		}
	}

	public void isUnblockAction(JSONObject obj) {
		try {
			String whoId = obj.getString("who");
			String whom = obj.getString("whom");
			if (whom.equals(GlobalApp.getUidFromSharedPreference(this))) {
				Toast.makeText(getBaseContext(), whoId + " has blocked you",
						Toast.LENGTH_LONG).show();
			}
		} catch (JSONException jex) {

		}
	}

	private void handleIntent(Intent intent) {
		if (intent.getAction().equals(Intent.ACTION_SEARCH)) {
			doSearch(intent.getStringExtra(SearchManager.QUERY));
		} else if (intent.getAction().equals(Intent.ACTION_VIEW)) {
			getPlace(intent.getStringExtra(SearchManager.EXTRA_DATA_KEY));
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		handleIntent(intent);
	}

	private void doSearch(String query) {
		Bundle data = new Bundle();
		data.putString("query", query);
		getSupportLoaderManager().restartLoader(0, data, this);
	}

	private void getPlace(String query) {
		Bundle data = new Bundle();
		data.putString("query", query);
		getSupportLoaderManager().restartLoader(1, data, this);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		wingleDB = new WingleDB(getApplicationContext());
		getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
		setContentView(R.layout.activity_main);

		customAlertDialog = new CustomDialog(this);
		customAlertDialog
				.setCustomDialogOutsideListener(customDialogClickListener);
		progressDialog = new ProgressDialog(this, R.style.MyTheme);
		progressDialog.setMessage("Login Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		getScreenSize();

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
		nearbyFragment = new NearbyFragment();
		randomPlacesFragment = new RandomPlacesFragment();
		mainPlacesFragment = new MainPlacesFragment();
		settingFragment = new SettingsFragment();
		profileFragment = new ProfileFragment();
		newsFeedFragment = new NewsFeedFragment();
		mapFragment = new MapFragment();
		friendsFragment = new NearestFriendFragment(this, mapFragment);
		userService = new UserService(this, MainActivity.this);
		mLocationRequest = LocationRequest.create();
		mLocationRequest
				.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest
				.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
		// mLocationRequest.setSmallestDisplacement(5.0f);
		// mLocationRequest.setSmallestDisplacement(100);
		mLocationClient = new LocationClient(this, this, this);
		checkNotNull(CommonUtilities.SERVER_URL, "SERVER_URL");
		checkNotNull(CommonUtilities.SENDER_ID, "SENDER_ID");
		try {
			GCMRegistrar.checkDevice(this);
			// Make sure the manifest was properly set - comment out this line
			GCMRegistrar.checkManifest(this);
			registerReceiver(mHandleMessageReceiver, new IntentFilter(
					CommonUtilities.DISPLAY_MESSAGE_ACTION));
		} catch (Exception e) {
			e.printStackTrace();
		}
		GlobalApp.manageAppFolders();
		userActionService = new UserActionService(this);
		userActionService.setMyId(Long.parseLong(GlobalApp
				.getUidFromSharedPreference(this)));
		userActionService
				.setActionType(UserActionService.ACTION_GET_ALL_DETAIL);
		// hideKeyboard();
		new Thread(userActionService).start();
		verifier = new Verifier();

	}

	// verification process for GCM
	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException(getString(R.string.error_config,
					name));
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandleMessageReceiver);
	}

	public void jumpToPeopleListing(String targetUserIds) {
		try {
			Intent peopleFragIntent = new Intent(this,
					SeePeopleFragmentActivity.class);
			peopleFragIntent.putExtra("member_ids", targetUserIds);
			startActivity(peopleFragIntent);
			overridePendingTransition(android.R.anim.slide_in_left,
					android.R.anim.slide_out_right);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (GlobalApp.checkInternetConnection(MainActivity.this)) {
			mLocationClient.connect();
		} else {
			// Toast.makeText(MainActivity.this, "Internet not available",
			// Toast.LENGTH_SHORT).show();
			customAlertDialog.show();
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		try {
			this.unregisterReceiver(mHandleMessageReceiver);
		} catch (Exception ex) {
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mLocationClient.disconnect();
	}

	public OnClickListener customDialogClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			customAlertDialog.dismiss();
			finish();
		}
	};

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		// FragmentManager fragmentManager = getSupportFragmentManager();
		// fragmentManager.beginTransaction().replace(R.id.container,PlaceholderFragment.newInstance(position
		// + 1)).commit();

		changeFragment(position);
	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_section1);
			break;
		case 2:
			mTitle = getString(R.string.title_section2);
			break;
		case 3:
			mTitle = getString(R.string.title_section3);
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
		}

		getMenuInflater().inflate(R.menu.main, menu);
		restoreActionBar();
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int searchSrcTextId = getResources().getIdentifier(
				"android:id/search_src_text", null, null);
		if (item.getItemId() == R.id.action_search_people) {
			isPlacesSearch = false;
			PlaceProvider.currentMode = PlaceProvider.MODE_USER;
		} else if (item.getItemId() == R.id.action_search_places) {
			isPlacesSearch = true;
			PlaceProvider.currentMode = PlaceProvider.MODE_PLACE;
		}
		searchView.setIconifiedByDefault(false);
		firstTimeIconfied = false;
		hideKeyboard(searchAutComp);
		invalidateOptionsMenu();
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		if (isPlacesSearch) {
			Log.w("options", "inside place search");
			menu.clear();
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();

			MenuItem searchItem = menu.findItem(R.id.action_search);
			searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
			// searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
			// Assumes current activity is the searchable activity

			searchView.setIconified(firstTimeIconfied);

			SpannableStringBuilder ssb = new SpannableStringBuilder(
					"   Search places");
			ImageView collapsedSearchIcon = (ImageView) searchView
					.findViewById(android.support.v7.appcompat.R.id.search_button);
			collapsedSearchIcon.setImageResource(R.drawable.ic_search);
			View searchPlate = searchView
					.findViewById(android.support.v7.appcompat.R.id.search_plate);
			searchPlate.setBackgroundResource(R.drawable.tansparent_black);
			searchView.setSearchableInfo(searchManager
					.getSearchableInfo(getComponentName()));
			searchAutComp = (SearchView.SearchAutoComplete) searchView
					.findViewById(android.support.v7.appcompat.R.id.search_src_text);

			Drawable searchIcon = getResources().getDrawable(
					R.drawable.ic_search);
			int textSize = (int) (searchAutComp.getTextSize() * 1.25);
			searchIcon.setBounds(5, 0, textSize, textSize);
			ssb.setSpan(new ImageSpan(searchIcon), 1, 2,
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

			// searchAutComp.setBackgroundResource(R.drawable.tansparent_black);
			searchAutComp.setHintTextColor(Color.WHITE);
			searchAutComp.setTextColor(Color.WHITE);
			searchAutComp.setPadding(10, 5, 5, 5);
			searchAutComp.setHint(ssb);

			ImageView close = (ImageView) searchView
					.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
			close.setImageResource(R.drawable.ic_cancel);
			close.setAlpha(192);

		} else {
			Log.w("options", "inside people search");
			menu.clear();
			getMenuInflater().inflate(R.menu.search_people, menu);
			restoreActionBar();
			MenuItem searchItem = menu.findItem(R.id.action_people_search);
			searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
			// searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
			// Assumes current activity is the searchable activity

			searchView.setIconified(firstTimeIconfied);

			SpannableStringBuilder ssb = new SpannableStringBuilder(
					"   Search people");
			ImageView collapsedSearchIcon = (ImageView) searchView
					.findViewById(android.support.v7.appcompat.R.id.search_button);
			collapsedSearchIcon.setImageResource(R.drawable.ic_search);
			View searchPlate = searchView
					.findViewById(android.support.v7.appcompat.R.id.search_plate);
			searchPlate.setBackgroundResource(R.drawable.tansparent_black);
			searchView.setSearchableInfo(searchManager
					.getSearchableInfo(getComponentName()));
			searchAutComp = (SearchView.SearchAutoComplete) searchView
					.findViewById(android.support.v7.appcompat.R.id.search_src_text);

			Drawable searchIcon = getResources().getDrawable(
					R.drawable.ic_search);
			int textSize = (int) (searchAutComp.getTextSize() * 1.25);
			searchIcon.setBounds(5, 0, textSize, textSize);
			ssb.setSpan(new ImageSpan(searchIcon), 1, 2,
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

			// searchAutComp.setBackgroundResource(R.drawable.tansparent_black);
			searchAutComp.setHintTextColor(Color.WHITE);
			searchAutComp.setTextColor(Color.WHITE);
			searchAutComp.setPadding(10, 5, 5, 5);
			searchAutComp.setHint(ssb);

			ImageView close = (ImageView) searchView
					.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
			close.setImageResource(R.drawable.ic_cancel);
			close.setAlpha(192);
		}
		searchView.clearFocus();
		return super.onPrepareOptionsMenu(menu);
	}

	private void hideKeyboard(View view) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			TextView textView = (TextView) rootView
					.findViewById(R.id.section_label);
			textView.setText(Integer.toString(getArguments().getInt(
					ARG_SECTION_NUMBER)));
			return rootView;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);

		}
	}

	protected void changeFragment(int position) {
		if (position == 0) {
			startActivity(new Intent(MainActivity.this,
					ProfileFragmentActivity.class));
			overridePendingTransition(android.R.anim.slide_in_left,
					android.R.anim.slide_out_right);
		} else if (position == 1) {
			if (GlobalApp.checkInternetConnection(MainActivity.this)) {
				FragmentTransaction frgTransaction = getSupportFragmentManager()
						.beginTransaction();
				frgTransaction.replace(R.id.container, mapFragment);
				// frgTransaction.remove(removeFrag);
				frgTransaction.disallowAddToBackStack();
				frgTransaction.commit();
			} else {
				// Toast.makeText(MainActivity.this, "Internet not available",
				// Toast.LENGTH_SHORT).show();
				customAlertDialog.show();
			}
		} else if (position == 2) {
			if (GlobalApp.checkInternetConnection(MainActivity.this)) {
				friendsFragment.setUpMapIfNeeded();
			} else {
				// Toast.makeText(MainActivity.this, "Internet not available",
				// Toast.LENGTH_SHORT).show();
				customAlertDialog.show();
			}
		} else if (position == 3) {
			FragmentTransaction frgTransaction = getSupportFragmentManager()
					.beginTransaction();
			frgTransaction.replace(R.id.container, randomPlacesFragment);
			// frgTransaction.remove(removeFrag);
			frgTransaction.disallowAddToBackStack();
			frgTransaction.commit();

		} else if (position == 4) {
			FragmentTransaction frgTransaction = getSupportFragmentManager()
					.beginTransaction();
			frgTransaction.replace(R.id.container, mainPlacesFragment);
			// frgTransaction.remove(removeFrag);
			frgTransaction.disallowAddToBackStack();
			frgTransaction.commit();

		} else if (position == 5) {
			FragmentTransaction frgTransaction = getSupportFragmentManager()
					.beginTransaction();
			frgTransaction.replace(R.id.container, newsFeedFragment);
			// frgTransaction.remove(removeFrag);
			frgTransaction.disallowAddToBackStack();
			frgTransaction.commit();

		} else if (position == 6) {
			FragmentTransaction frgTransaction = getSupportFragmentManager()
					.beginTransaction();
			frgTransaction.replace(R.id.container, settingFragment);
			// frgTransaction.remove(removeFrag);
			frgTransaction.disallowAddToBackStack();
			frgTransaction.commit();

		} else if (position == 7) {
			String uid = GlobalApp
					.getUidFromSharedPreference(MainActivity.this);
			userService.setOption(UserService.OPTION_LOGOUT);
			userService.setUid(uid);
			new Thread(userService).start();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle connectionHint) {
		// TODO Auto-generated method stub
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.w("onresume", "on resume called");
	}

	public LatLng getMyLatLon() {
		return myLatLon;
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		myLatLon = new LatLng(arg0.getLatitude(), arg0.getLongitude());
		if (GlobalApp.checkInternetConnection(MainActivity.this)) {
			if (mapFragment.isVisible()) {
				if (!call) {
					mapFragment.setMyMarker(new LatLng(arg0.getLatitude(), arg0
							.getLongitude()));
					friendsFragment.setCurrLatLng(myLatLon);
					registerDevice();
					call = true;
				}
			}
		} else {
			Toast.makeText(MainActivity.this, "Internet not available",
					Toast.LENGTH_LONG).show();
		}
	}

	// Registering a device to GCM and retrive registration ID
	public void registerDevice() {
		final String regId = GCMRegistrar.getRegistrationId(this);
		if (regId.equals("")) {
			// Automatically registers application on startup.
			GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
		} else {
			// Device is already registered on GCM, check server.
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				// Skips registration.
				// mDisplay.append(getString(R.string.already_registered) +
				// "\n");
			} else {

			}
		}
	}

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("logout")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.setMessage("Logout Please wait...");
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("logout_success")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(MainActivity.this, "Logout Successfully",
							Toast.LENGTH_LONG).show();
					GlobalApp.clearUidFromSharedPreference(MainActivity.this);
					finish();
					startActivity(new Intent(MainActivity.this,
							LoginActivity.class));
					overridePendingTransition(android.R.anim.slide_in_left,
							android.R.anim.slide_out_right);
					deleteCachedFile(new File(GlobalApp.getCachedPathFull()));
				}
			});
		} else if (msg.equalsIgnoreCase("logout_problem")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(MainActivity.this, "Problem in Logout",
							Toast.LENGTH_LONG).show();
				}
			});
		} else if (msg.equalsIgnoreCase("error")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(MainActivity.this, "Connection problem",
							Toast.LENGTH_LONG).show();
				}
			});
		} else if (msg.equalsIgnoreCase(UserActionService.SUCCESS_MESSAGE)) {
			JSONArray jArr = userActionService.getMyData();
			cacheToFile(GlobalApp.CACHED_FILE_NAME, jArr.toString());
			verifier.prepareVerifier();

			try {
				mNavigationDrawerFragment.setFollowers(verifier.getFollowers());
				mNavigationDrawerFragment.setFollowing(verifier
						.getFollowingUsers());
				mNavigationDrawerFragment
						.setBlocked(verifier.getBlockedUsers());
				GlobalApp.setMemberIds(concatArray(verifier.getFollowers(),verifier.getFollowingUsers()).toString());
				wingleDB.open();
				wingleDB.putAppCommon(GlobalApp.memberIds, concatArray(verifier.getFollowers(),verifier.getFollowingUsers()).toString());
				Log.i("member_ids", wingleDB.getAppCommon(GlobalApp.memberIds));
				wingleDB.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("error")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(MainActivity.this, "Connection problem",
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle query) {
		// TODO Auto-generated method stub
		CursorLoader cLoader = null;
		if (arg0 == 0)
			cLoader = new CursorLoader(getBaseContext(),
					PlaceProvider.SEARCH_URI, null, null,
					new String[] { query.getString("query") }, null);
		else if (arg0 == 1)
			cLoader = new CursorLoader(getBaseContext(),
					PlaceProvider.DETAILS_URI, null, null,
					new String[] { query.getString("query") }, null);
		return cLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		// TODO Auto-generated method stub

		showLocations(arg1);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub

	}

	private void showLocations(Cursor c) {
		// operation
		if (PlaceProvider.currentMode == PlaceProvider.MODE_PLACE
				&& mapFragment.isVisible()) {
			LatLng position = null;
			while (c.moveToNext()) {
				position = new LatLng(Double.parseDouble(c.getString(1)),
						Double.parseDouble(c.getString(2)));
			}
			if (position != null) {
				mapFragment.jumpToLocation(position);
			}
		} else if (PlaceProvider.currentMode == PlaceProvider.MODE_USER) {

			StringBuffer sb = new StringBuffer();
			JSONArray jarrIds = new JSONArray();
			while (c.moveToNext()) {
				jarrIds.put(c.getInt(0));
			}
			if (jarrIds.length() > 0)
				jumpToPeopleListing(jarrIds.toString());
		}
	}

	public void cacheToFile(String fileName, String body) {
		FileOutputStream fos = null;
		try {
			// final File dir = new
			// File(Environment.getExternalStorageDirectory().getAbsolutePath()
			// + "/folderName/" );
			f = new File(GlobalApp.getCachedPathFull());

			final File myFile = new File(f, fileName);

			if (!myFile.exists()) {
				myFile.createNewFile();
				Log.w("cache", "file created");
			}
			fos = new FileOutputStream(myFile);

			fos.write(body.getBytes());
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean deleteCachedFile(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			if (files == null) {
				return true;
			}
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteCachedFile(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}
	
	private JSONArray concatArray(JSONArray... arrs)
	        throws JSONException {
	    JSONArray result = new JSONArray();
	    for (JSONArray arr : arrs) {
	        for (int i = 0; i < arr.length(); i++) {
	            result.put(arr.get(i));
	        }
	    }
	    return result;
	}

}