package com.digitize.wingleapp;

import java.util.ArrayList;
import java.util.Iterator;

import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.UpdateProfileService;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class BarsToGoFragment extends Fragment implements ServiceCallback {

	EditText edtBars;
	ImageView imgEditBars;
	boolean barsEnable = false;
	UpdateProfileService updateProfileService;
	ProgressDialog progressDialog;
	WingleDB wingleDB;
	ArrayList<ProfileData> profileDataList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.bars_fragment, null);
		imgEditBars = (ImageView) view.findViewById(R.id.img_edit_bars);
		edtBars = (EditText) view.findViewById(R.id.edt_bars_user_go);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		progressDialog.setMessage("Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		wingleDB = new WingleDB(getActivity());
		imgEditBars.setOnClickListener(editBarsListener);
		edtBars.setEnabled(false);
		edtBars.setBackgroundColor(Color.TRANSPARENT);
		updateProfileService = new UpdateProfileService(BarsToGoFragment.this,
				getActivity());
		try {
			wingleDB.open();
			profileDataList = wingleDB.getUserData(GlobalApp
					.getUidFromSharedPreference(getActivity()));
			wingleDB.close();
			Iterator<ProfileData> profileIterator = profileDataList.iterator();
			ProfileData profileData;
			while (profileIterator.hasNext()) {
				profileData = profileIterator.next();
				edtBars.setText(profileData.getFavBars());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public OnClickListener editBarsListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (barsEnable) {
				updateProfileService
						.setUpdateOption(UpdateProfileService.UPDATE_FAV_BARS);
				updateProfileService.setFavBars(edtBars.getText().toString()
						.trim());
				updateProfileService.setUid(GlobalApp
						.getUidFromSharedPreference(getActivity()));
				new Thread(updateProfileService).start();
				edtBars.setBackgroundColor(Color.TRANSPARENT);
				edtBars.setEnabled(false);
				edtBars.setTextColor(Color.parseColor("#ffffff"));
				barsEnable = false;
			} else {
				edtBars.setEnabled(true);
				edtBars.setBackgroundResource(R.drawable.input_field_style);
				edtBars.setTextColor(Color.parseColor("#000000"));
				barsEnable = true;
			}
		}
	};

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("updateprofile_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("update_success")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
				}
			});
		} else if (msg.equalsIgnoreCase("update_problem")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Problem updating profile",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase("params_missing")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Some params missing",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase("error")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Connection problem",
							Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}

}
