package com.digitize.wingleapp;

import java.util.ArrayList;

import com.digitize.wingleapp.imagepart.ImageLoader;
import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.UserActionService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SeePeopleAdapter extends BaseAdapter implements ServiceCallback {

	Context context;
	ArrayList<ProfileData> profileListData;
	LayoutInflater layoutinfalter;
	ImageLoader imageLoader;
	Activity activity;
	UserActionService userActionService = null;
	ProgressDialog progressDialog = null;
	String myId = null;

	public void setData(ArrayList<ProfileData> profileData) {
		this.profileListData = profileData;
	}

	public SeePeopleAdapter(Context context, Activity activity) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.activity = activity;
		this.layoutinfalter = LayoutInflater.from(context);
		imageLoader = new ImageLoader(activity);
		userActionService = new UserActionService(this);
		userActionService.setMyId(Long.parseLong(GlobalApp
				.getUidFromSharedPreference(context)));
		myId = GlobalApp.getUidFromSharedPreference(context);
		progressDialog = new ProgressDialog(context, R.style.MyTheme);
		progressDialog.setMessage("Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return profileListData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return profileListData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ProfileCell profileCell;
		ProfileData profile = profileListData.get(position);
		if (convertView == null) {
			convertView = layoutinfalter.inflate(R.layout.people_list_item,
					null);
			profileCell = new ProfileCell();
			profileCell.txtName = (TextView) convertView
					.findViewById(R.id.txt_people_name);
			profileCell.btnChat = (Button) convertView
					.findViewById(R.id.btn_people_chat);
			profileCell.txtDesc = (TextView) convertView
					.findViewById(R.id.txt_people_description);
			profileCell.imgProfile = (ImageView) convertView
					.findViewById(R.id.people_image);
			profileCell.btnFollow = (Button) convertView
					.findViewById(R.id.btn_people_follow);
			profileCell.btnFollow
					.setOnClickListener(profileCell.followClickListener);

			convertView.setTag(profileCell);
		} else {
			profileCell = (ProfileCell) convertView.getTag();
		}

		if (profile.isFollowing()) {
			profileCell.btnFollow.setText("Unfollow");
		} else {
			profileCell.btnFollow.setText("Follow");
		}
		profileCell.pos = position;
		if (profile.getUid().equals(myId)) {
			profileCell.btnChat.setVisibility(View.GONE);
			profileCell.btnFollow.setVisibility(View.GONE);
			profileCell.txtName.setText(profile.getFirstName() + " "
					+ profile.getLastName() + "  (YOU)");
		} else {
			profileCell.btnChat.setVisibility(View.VISIBLE);
			profileCell.btnFollow.setVisibility(View.VISIBLE);
			profileCell.txtName.setText(profile.getFirstName() + " "
					+ profile.getLastName());
		}

		profileCell.txtDesc.setText(profile.getEmail());
		imageLoader.DisplayImage(profile.getImagePath(), activity,
				profileCell.imgProfile);
		return convertView;
	}

	class ProfileCell {
		TextView txtName;
		TextView txtDesc;
		ImageView imgProfile;
		Button btnFollow;
		Button btnChat;
		int pos;

		public OnClickListener followClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (btnFollow.getText().toString().equals("Follow")) {
					ProfileData profileData = profileListData.get(pos);
					userActionService.setTargetId(Long.parseLong(profileData
							.getUid()));
					userActionService
							.setActionType(UserActionService.ACTION_FOLLOW);
					profileData.setFollowing(true);
					new Thread(userActionService).start();
					btnFollow.setText("Unfollow");
				} else if (btnFollow.getText().toString().equals("Unfollow")) {
					ProfileData profileData = profileListData.get(pos);
					userActionService.setTargetId(Long.parseLong(profileData
							.getUid()));
					userActionService
							.setActionType(UserActionService.ACTION_UNFOLLOW);
					new Thread(userActionService).start();
					btnFollow.setText("Follow");
				}
			}
		};

	}

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase(UserActionService.SERVICE_NAME)) {
			activity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase(UserActionService.SUCCESS_MESSAGE)) {
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					if (userActionService.getActionType().equals(
							UserActionService.ACTION_FOLLOW)) {
						Toast.makeText(context, "Followed", Toast.LENGTH_SHORT)
								.show();
					} else {
						Toast.makeText(context, "Unfollowed",
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		} else if (msg.equalsIgnoreCase(UserActionService.ERROR_MESSAGE)) {
			activity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}

}
