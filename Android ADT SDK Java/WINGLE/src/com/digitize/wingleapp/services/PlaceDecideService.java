package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class PlaceDecideService implements Runnable {

	ServiceCallback serviceCallback;
	Context context;
	private String option;
	private String uid;
	private double lat;
	private double lon;
	private String placeInfo;
	private String message;
	private long livePlaceId = 0;
	private long localPlaceId = 0;
	public static final String SUCCESS_MESSAGE = "PlaceDecide:success";
	public static final String ERROR_MESSAGE = "PlaceDecide:error";
	public static final String SERVICE_NAME = "PlaceDecide";
	public static final String MESSAGE_FORMAT = "{\"action_type\":\"%s\",\"who\":\"%s\",\"lat\":\"%s\",\"lon\":\"%s\",\"message\":\"%s\"}";
	public static final String PRECHECKIN_ACTION = "precheckin";

	public String generateMessage(String customMessage) {
		return String.format(MESSAGE_FORMAT,
				PlaceDecideService.PRECHECKIN_ACTION, uid,
				Double.toString(lat), Double.toString(lon), customMessage);
	}

	public long getLocalPlaceId() {
		return localPlaceId;
	}

	public void setLocalPlaceId(long localPlaceId) {
		this.localPlaceId = localPlaceId;
	}

	public long getLivePlaceId() {
		return livePlaceId;
	}

	public void setLivePlaceId(long livePlaceId) {
		this.livePlaceId = livePlaceId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getPlaceInfo() {
		return placeInfo;
	}

	public void setPlaceInfo(String placeInfo) {
		this.placeInfo = placeInfo;
	}

	public PlaceDecideService(ServiceCallback serviceCallback, Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		serviceCallback.serviceStarted(SERVICE_NAME);
		postDataForPlaceInfo();
	}

	Handler handler = new Handler() {

		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("placebook_response", response);
				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	public void postDataForPlaceInfo() {
		String textUrl = prepareUrlForRegistration();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("uid", uid));
		nameValuePairs.add(new BasicNameValuePair("lat", String.valueOf(lat)));
		nameValuePairs.add(new BasicNameValuePair("lon", String.valueOf(lon)));
		nameValuePairs.add(new BasicNameValuePair("place_info", placeInfo));
		nameValuePairs.add(new BasicNameValuePair("message",
				generateMessage("I m coming here")));
		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForRegistration() {
		return String.format(UrlCollection.PRECHECKING_URL);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractData(String res) {
		JSONObject jsonObj = null;
		String message = null;
		String code = null;

		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");
			setLivePlaceId(jsonObj.getInt("id"));
			serviceCallback.serviceEnd(SUCCESS_MESSAGE);

		} catch (JSONException e) {
			serviceCallback.serviceEnd(ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}
