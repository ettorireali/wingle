package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class RemovePrecheckin implements Runnable {

	ServiceCallback serviceCallback;
	Context context;
	String uid;

	public void setUid(String uid) {
		this.uid = uid;
	}

	public RemovePrecheckin(ServiceCallback serviceCallback, Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		serviceCallback.serviceStarted("removeplace");
		postDataForRemovePlaceInfo();
	}

	Handler handler = new Handler() {

		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("placebook_response", response);

				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	public void postDataForRemovePlaceInfo() {
		String textUrl = prepareUrlForRegistration();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("uid", uid));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForRegistration() {
		return String.format(UrlCollection.REMOVE_PRECHECKIN);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractData(String res) {
		JSONObject jsonObj = null;
		String message = null;
		String code = null;

		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");

			if (message.equalsIgnoreCase("Missing basic params")) {
				serviceCallback.serviceEnd("missing_params");
			} else if (message.equalsIgnoreCase("Book Info Updated")) {
				serviceCallback.serviceEnd("place_submit");
			} else {
				serviceCallback.serviceEnd("message");
			}
		} catch (JSONException e) {
			serviceCallback.serviceEnd("error");
			e.printStackTrace();
		}
	}

}
