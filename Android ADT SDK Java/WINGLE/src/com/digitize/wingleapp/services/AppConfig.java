package com.digitize.wingleapp.services;

public class AppConfig {

	/**
	 * default number of kilometers to perform
	 */
	public static int DEFAULT_KM_FOR_QUERY = 10;
	/**
	 * @description : it will search in google places api , type of place
	 */
	public static String DEFAULT_TYPE_TO_SEARCH = "bar|night_club";
}
