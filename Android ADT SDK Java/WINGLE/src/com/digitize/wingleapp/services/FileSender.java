package com.digitize.wingleapp.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.GlobalApp;
import com.digitize.wingleapp.db.WingleDB;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class FileSender {

	String parentDir = "WINGLE";
	UploadProgress uploadProgress = null;
	public static int COUNTER = 0;
	private boolean isContinue = false;
	private Object lockKey = new Object();
	Context context;
	public static final int MEDIA_TYPE_IMAGE = 0;
	WingleDB wingleDB;

	public static final String PHP_WRITE_MODE = "w"; // first time
	public static final String PHP_APPEND_MODE = "a"; // next time onwards
	private String currentMode = PHP_WRITE_MODE;
	private long bytesToSkip = 0;

	public long getBytesToSkip() {
		return bytesToSkip;
	}

	public void setBytesToSkip(long bytesToSkip) {
		Log.w("skip", String.valueOf(bytesToSkip));
		this.bytesToSkip = bytesToSkip;
	}

	public void setMode(String mMode) {
		this.currentMode = mMode;
	}

	public void setIsContinue(boolean isContinue) {
		synchronized (lockKey) {
			this.isContinue = isContinue;
		}
	}

	public FileSender(Context context) {
		this.context = context;
		isContinue = true;
		wingleDB = new WingleDB(context);
	}

	public void setUploadProgress(UploadProgress uploadProgress) {
		this.uploadProgress = uploadProgress;
	}

	public void sendFile(String filePath, String uId)
			throws MalformedURLException, IOException {
		URL url;
		File fileToUpload = new File(filePath);
		url = new URL(UrlCollection.image_upload_url);
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setRequestMethod("PUT");
		// httpCon.setChunkedStreamingMode(1024);
		// httpCon.setDefaultUseCaches(true);
		// httpCon.setAllowUserInteraction(true);
		httpCon.setRequestProperty("Connection", "Keep-Alive");
		httpCon.setRequestProperty("Content-Length",
				String.valueOf(fileToUpload.length()));
		httpCon.setRequestProperty("Content-Type", "application/octet-stream");
		httpCon.setConnectTimeout(0);
		// httpCon.setRequestProperty("Content-Type", "image/jpeg");eventId
		httpCon.setChunkedStreamingMode(0);
		httpCon.setDoOutput(true);
		httpCon.setDoInput(true);
		httpCon.setUseCaches(false);
		String fileName = fileToUpload.getName();
		Log.w("filename", fileName);
		Log.w("uid_from_sender", "uid : " + uId);

		httpCon.setRequestProperty("Imagename", fileName);
		httpCon.setRequestProperty("Mode", currentMode);
		httpCon.setRequestProperty("Uid", uId);
		httpCon.setRequestProperty("Media-type",
				String.valueOf(MEDIA_TYPE_IMAGE));

		// httpCon.setRequestProperty("partno", partno);
		httpCon.connect();

		if (fileToUpload.exists()) {
			Log.w("data", "file exists great..");
		} else {
			Log.w("data", "file doesn't exists at  ." + fileToUpload.getPath());
			return;
		}
		long fileLength = fileToUpload.length(); // total bytes to upload
		FileInputStream fin = new FileInputStream(new File(filePath));
		OutputStream outPipeline = httpCon.getOutputStream();
		byte[] chunks = new byte[1024];
		int uploaded = 0;
		long uploadedCounter = 0;

		/*
		 * skip bytes loop *
		 */
		if (bytesToSkip > 0) {
			fin.skip(bytesToSkip);
			uploadedCounter += bytesToSkip;
		}

		while ((uploaded = fin.read(chunks, 0, chunks.length)) != -1) {
			synchronized (lockKey) {
				if (!isContinue) {
					break;
				}
			}
			outPipeline.write(chunks, 0, uploaded);
			uploadedCounter += uploaded;
			uploadProgress.uploadPercent((uploadedCounter) * 100.0f
					/ fileLength, uploadedCounter);
			Log.w("response",
					String.valueOf((uploadedCounter) * 100.0f / fileLength));
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		InputStream inPipeline = httpCon.getInputStream();
		StringBuffer sb = new StringBuffer();
		int ch = -1;
		while ((ch = inPipeline.read()) != -1) {
			sb.append((char) ch);
		}
		inPipeline.close();

		// int status=httpCon.getResponseCode();
		fin.close();
		outPipeline.close();
		JSONObject jsonObj = null;
		String message = null;
		String imagePath = null;
		try {
			jsonObj = new JSONObject(sb.toString());
			message = jsonObj.getString("msg");
			imagePath = jsonObj.getString("media_path");
			if (imagePath != null) {
				wingleDB.open();
				wingleDB.updateMediaPath(
						GlobalApp.getUidFromSharedPreference(context),
						imagePath);
				wingleDB.close();
				Log.w("image_upload", "inside image path condition");
			}
			Log.w("image_upload", "media path is : " + imagePath);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.w("response", sb.toString());
		if (message.equalsIgnoreCase("Missing basic params")) {
			uploadProgress.uploadError("missing_param");
		} else if (message.equalsIgnoreCase("Some problem in image upload")) {
			uploadProgress.uploadError("upload_problem");
		} else if (message.equalsIgnoreCase("Image Upload Successfully")) {
			uploadProgress.uploadError("upload_success");
		}

		cacheToFile("responsefile", sb.toString());
		// Log.w("code", "response code : "+status);
		httpCon.disconnect();

	}

	public void cacheToFile(String fileName, String body) {
		FileOutputStream fos = null;
		File file;
		try {
			// final File dir = new
			// File(Environment.getExternalStorageDirectory().getAbsolutePath()
			// + "/folderName/" );
			file = new File(Environment.getExternalStorageDirectory() + "/"
					+ parentDir);
			if (!file.exists()) {
				file.mkdir();
			}
			final File myFile = new File(file, fileName + ".txt");

			if (!myFile.exists()) {
				myFile.createNewFile();
				Log.w("cache", "file created");
			}
			fos = new FileOutputStream(myFile);

			fos.write(body.getBytes());
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
