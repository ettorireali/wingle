package com.digitize.wingleapp.services;

import java.io.IOException;
import java.net.MalformedURLException;

import android.content.Context;

public class ProfilePictureUploadService implements Runnable, UploadProgress {

	ServiceCallback serviceCallback;
	Context context;
	String imagePath;
	FileSender fileSender;
	String uId;

	public ProfilePictureUploadService(ServiceCallback serviceCallback,
			Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
		fileSender = new FileSender(context);
		fileSender.setUploadProgress(this);
	}

	public void setImagePath(String path) {
		this.imagePath = path;
	}

	public void setUid(String uid) {
		this.uId = uid;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		uploadImagePutMethod();
	}

	public void uploadImagePutMethod() {
		try {
			serviceCallback.serviceStarted("upload_start");
			fileSender.sendFile(imagePath, uId);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serviceCallback.serviceEnd("error");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serviceCallback.serviceEnd("error");
		} catch (Exception e) {
			serviceCallback.serviceEnd("error");
			e.printStackTrace();
		}
	}

	@Override
	public void uploadPercent(float percent, long bytesUploaded) {
		// TODO Auto-generated method stub

	}

	@Override
	public void uploadStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void uploadEnded() {
		// TODO Auto-generated method stub

	}

	@Override
	public void uploadError(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("missing_param")) {
			serviceCallback.serviceEnd("missing_param");
		} else if (msg.equalsIgnoreCase("upload_problem")) {
			serviceCallback.serviceEnd("upload_problem");
		} else if (msg.equalsIgnoreCase("upload_success")) {
			serviceCallback.serviceEnd("upload_success");
		}
	}

}
