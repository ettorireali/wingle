package com.digitize.wingleapp.services;

public interface ServiceCallback {

	void serviceStarted(String msg);

	void serviceEnd(String msg);

	void serviceInProgress(String msg);

}
