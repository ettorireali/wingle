package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.GlobalApp;
import com.digitize.wingleapp.pojo.ProfileData;
import com.google.android.gms.internal.co;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class UserService implements Runnable {

	private ServiceCallback serviceCallback;
	Context context;
	ProfileData profileData;
	private String firstName;
	private String lastName;
	private String userName;
	private String dateOfBirth;
	private String email;
	private int gender;
	private String password;
	private String deviceId;
	private String uid;
	private String option; // possible values ( register | login | logout )

	public static final String OPTION_REGISTER = "register";
	public static final String OPTION_LOGIN = "login";
	public static final String OPTION_LOGOUT = "logout";
	private String ip = null;

	public ProfileData getUserData() {
		return profileData;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public ServiceCallback getServiceCallback() {
		return serviceCallback;
	}

	public void setServiceCallback(ServiceCallback serviceCallback) {
		this.serviceCallback = serviceCallback;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
		Log.w("uid", "uid from service : " + uid);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public UserService(Context context, ServiceCallback serviceCallback) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.serviceCallback = serviceCallback;
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;

				if (option.equalsIgnoreCase(OPTION_LOGIN)) {
					extractDataForLogin(response);
					Log.w("login_response", response);
				} else if (option.equalsIgnoreCase(OPTION_REGISTER)) {
					extractDataForRegistration(response);
					Log.w("register_response", response);
				} else if (option.equalsIgnoreCase(OPTION_LOGOUT)) {
					extractDataForLogout(response);
					Log.w("logout_response", response);
				}
				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {

					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	@Override
	public void run() {
		// TODO Auto-generated method stub

		if (option.equals(OPTION_REGISTER)) {
			serviceCallback.serviceStarted("register");
			postDataForRegistration();
		} else if (option.equalsIgnoreCase(OPTION_LOGIN)) {
			serviceCallback.serviceStarted("login");
			postDataForLogin();
		} else if (option.equalsIgnoreCase(OPTION_LOGOUT)) {
			serviceCallback.serviceStarted("logout");
			postDataForLogout();
		}
	}

	public void postDataForLogin() {
		String textUrl = prepareUrlForLogin();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("username", userName));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("port", String
				.valueOf(GlobalApp.generatePort())));
		if (ip != null) {
			nameValuePairs.add(new BasicNameValuePair("myip", ip));
		}
		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void postDataForLogout() {
		String textUrl = prepareUrlForLogout();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("uid", uid));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void postDataForRegistration() {
		String textUrl = prepareUrlForRegistration();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("firstname", firstName));
		nameValuePairs.add(new BasicNameValuePair("lastname", lastName));
		nameValuePairs.add(new BasicNameValuePair("username", userName));
		nameValuePairs.add(new BasicNameValuePair("dob", dateOfBirth));
		nameValuePairs.add(new BasicNameValuePair("email", email));
		nameValuePairs.add(new BasicNameValuePair("gender", String
				.valueOf(gender)));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("deviceid", deviceId));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForRegistration() {
		return String.format(UrlCollection.register_url);
	}

	public String prepareUrlForLogin() {
		return String.format(UrlCollection.login_url);
	}

	public String prepareUrlForLogout() {
		return String.format(UrlCollection.logout_url);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractDataForLogout(String res) {
		JSONObject jsonObj = null;
		String message = null;
		String code = null;

		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");
			if (message.equalsIgnoreCase("User Logout Successfully")
					&& code.equals("1")) {
				serviceCallback.serviceEnd("logout_success");
			} else {
				serviceCallback.serviceEnd("logout_problem");
			}

		} catch (JSONException jse) {
			serviceCallback.serviceEnd("error");
			jse.printStackTrace();
		}
	}

	public void extractDataForLogin(String res) {

		JSONObject jsonObj = null;
		String message = null;
		String code = null;

		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");
			profileData = new ProfileData();
			if (message.equalsIgnoreCase("User Login Successfully")
					&& code.equals("1")) {
				String uId = jsonObj.getString("uid");
				Log.w("uid", "uid from extractdata() : " + uId);
				GlobalApp.storeUidInSharedPreference(context, uId);
				profileData.setUid(uId);
				profileData.setFirstName(jsonObj.getString("firstname"));
				profileData.setLastName(jsonObj.getString("lastname"));
				profileData.setUserName(jsonObj.getString("username"));
				profileData.setEmail(jsonObj.getString("email"));
				profileData.setGender(jsonObj.getInt("gender"));
				profileData.setDob(jsonObj.getString("dob"));
				profileData.setLikes(jsonObj.getString("likes"));
				profileData.setDislike(jsonObj.getString("dislikes"));
				profileData.setFavBars(jsonObj.getString("fav_bars"));
				profileData.setDateCreated(jsonObj.getString("date_created"));
				profileData.setDeviceId(jsonObj.getString("deviceid"));
				profileData.setStatus(jsonObj.getString("status"));
				profileData.setTagline(jsonObj.getString("tagline"));
				profileData.setIsShownToOthers(jsonObj
						.getInt("is_shown_to_other"));
				profileData.setIsLogin(jsonObj.getInt("is_login"));
				profileData.setImagePath(jsonObj.getString("media_path"));
				profileData.setAboutMe(jsonObj.getString("about_me"));
				serviceCallback.serviceEnd("login_success");
			} else if (message.equalsIgnoreCase("User Login facing a problem")) {
				serviceCallback.serviceEnd("login_problem");
			} else {
				serviceCallback.serviceEnd("basic_param_missing");
			}

		} catch (JSONException jse) {
			serviceCallback.serviceEnd("error");
			jse.printStackTrace();
		}
	}

	public void extractDataForRegistration(String res) {
		JSONObject jsonObj = null;
		String message = null;
		String code = null;
		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");

			if (message.equalsIgnoreCase("User name and email already exist")) {
				serviceCallback.serviceEnd("username_email_exist");
			} else if (message.equalsIgnoreCase("User name is already exist")) {
				serviceCallback.serviceEnd("username_exist");
			} else if (message.equalsIgnoreCase("User email is already exist")) {
				serviceCallback.serviceEnd("email_exist");
			} else if (message
					.equalsIgnoreCase("User registeration successfuly")
					&& code.equals("1")) {
				serviceCallback.serviceEnd("register_success");
			} else {
				serviceCallback.serviceEnd("basic_param_missing");
			}

		} catch (JSONException jse) {
			serviceCallback.serviceEnd("dataerror");
			jse.printStackTrace();
		}
	}

}
