package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.GlobalApp;
import com.digitize.wingleapp.pojo.ProximityData;
import com.google.android.gms.maps.model.LatLng;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class ProximityService implements Runnable {

	ServiceCallback serviceCallback;
	Context context;
	double latitude;
	double longitude;
	ArrayList<ProximityData> proximityList;
	private String uid = null;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public ArrayList<ProximityData> getProximityData() {
		return proximityList;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public ProximityService(ServiceCallback serviceCallback, Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
		proximityList = new ArrayList<ProximityData>();
	}

	Handler handler = new Handler() {

		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("proximity_response", response);

				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	@Override
	public void run() {
		// TODO Auto-generated method stub
		serviceCallback.serviceStarted("proximity_started");
		postDataForProximity();
	}

	public void postDataForProximity() {
		String textUrl = prepareUrlForProximity();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("uid", uid));
		nameValuePairs.add(new BasicNameValuePair("lat", String
				.valueOf(latitude)));
		nameValuePairs.add(new BasicNameValuePair("lon", String
				.valueOf(longitude)));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForProximity() {
		return String.format(UrlCollection.proximity_url);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractData(String res) {
		JSONArray jArray;
		JSONObject jObj;
		proximityList.clear();
		ProximityData proximityData;
		try {
			jArray = new JSONArray(res);
			for (int i = 0; i < jArray.length(); i++) {
				jObj = jArray.getJSONObject(i);
				proximityData = new ProximityData();
				proximityData.setUid(jObj.getString("uid"));
				proximityData.setPosition(new LatLng(jObj.getDouble("lat"),
						jObj.getDouble("lon")));
				proximityData.setDistance(jObj.getDouble("distance"));
				proximityData.setPlaceId(jObj.getInt("place_id"));

				proximityList.add(proximityData);
			}
			serviceCallback.serviceEnd("proximity_data");
		} catch (JSONException j) {
			j.printStackTrace();
			serviceCallback.serviceEnd("proximity_error");
		}
	}

}
