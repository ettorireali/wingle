package com.digitize.wingleapp.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.GlobalApp;

/**
 * 
 * @author safy
 * @description : verifier will read json array from /sdcard/WingleApp and will
 *              extract data used to determine whether the target id is
 *              follower/following/blocked person
 */

public class Verifier {

	private String blockedUids[] = null;
	private String followerUids[] = null;
	private String followingUids[] = null;

	private final String TYPE_FOLLOWER = "followers";
	private final String TYPE_FOLLOWING = "following";
	private final String TYPE_BLOCKED = "blocked";

	public Verifier() {

	}

	public void prepareVerifier() {
		extractData(loadJSONFromSdcard());
	}

	public boolean isBlocked(String uid) {
		boolean dataToReturn = false;
		for (String temp : blockedUids) {
			if (uid.equalsIgnoreCase(temp)) {
				dataToReturn = true;
				break;
			}
		}
		return dataToReturn;
	}

	public boolean isFollower(String uid) {
		boolean dataToReturn = false;
		for (String temp : followerUids) {
			if (uid.equalsIgnoreCase(temp)) {
				dataToReturn = true;
				break;
			}
		}
		return dataToReturn;
	}

	public JSONArray getBlockedUsers() {
		JSONArray dataToReturn = new JSONArray();
		for (String uid : blockedUids) {
			dataToReturn.put(uid);
		}
		return dataToReturn;
	}

	public JSONArray getFollowingUsers() {
		JSONArray dataToReturn = new JSONArray();
		for (String uid : followingUids) {
			dataToReturn.put(uid);
		}
		return dataToReturn;
	}

	public JSONArray getFollowers() {
		JSONArray dataToReturn = new JSONArray();
		for (String uid : followerUids) {
			dataToReturn.put(uid);
		}
		return dataToReturn;
	}

	public boolean isFollowing(String uid) {
		boolean dataToReturn = false;
		for (String temp : followingUids) {
			if (uid.equalsIgnoreCase(temp)) {
				dataToReturn = true;
				break;
			}
		}
		return dataToReturn;
	}

	/**
	 * @param fileName
	 *            name of the file from where the response will be loaded
	 * @return data that is being stored in the sdcard file
	 */
	public String loadJSONFromSdcard() {
		String json = null;
		try {
			File f = new File(GlobalApp.getCachedPathFull());
			final File myFile = new File(f, GlobalApp.CACHED_FILE_NAME);
			FileInputStream is = new FileInputStream(myFile);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			json = new String(buffer, "UTF-8");
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;
	}

	public boolean extractData(String res) {
		boolean dataToReturn = false;
		try {
			JSONArray parentArr = new JSONArray(res);
			JSONObject tempObj = null;
			JSONArray innerArray = null;
			for (int i = 0; i < parentArr.length(); i++) {
				tempObj = parentArr.getJSONObject(i);
				if (tempObj.getString("type").equalsIgnoreCase(TYPE_FOLLOWER)) {
					innerArray = tempObj.getJSONArray("uid");
					followerUids = new String[tempObj.getJSONArray("uid")
							.length()];
					for (int j = 0; j < innerArray.length(); j++) {
						followerUids[j] = innerArray.getString(j);
					}
				} else if (tempObj.getString("type").equalsIgnoreCase(
						TYPE_FOLLOWING)) {
					innerArray = tempObj.getJSONArray("uid");
					followingUids = new String[tempObj.getJSONArray("uid")
							.length()];
					for (int j = 0; j < innerArray.length(); j++) {
						followingUids[j] = innerArray.getString(j);
					}
				} else if (tempObj.getString("type").equalsIgnoreCase(
						TYPE_BLOCKED)) {
					innerArray = tempObj.getJSONArray("uid");
					blockedUids = new String[tempObj.getJSONArray("uid")
							.length()];
					for (int j = 0; j < innerArray.length(); j++) {
						blockedUids[j] = innerArray.getString(j);
					}
				}
			}
			dataToReturn = true;
		} catch (JSONException json) {
			dataToReturn = true;
		}
		return dataToReturn;
	}
}
