package com.digitize.wingleapp.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.digitize.wingleapp.pojo.ProfileData;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class PeopleSearchService implements Runnable {

	ServiceCallback serviceCallback;
	Context context;
	String uId;
	String keyword;
	ArrayList<ProfileData> searchListData;

	public ArrayList<ProfileData> getSearchData() {
		return searchListData;
	}

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public PeopleSearchService(ServiceCallback serviceCallback, Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
		searchListData = new ArrayList<ProfileData>();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		serviceCallback.serviceStarted("search_started");
		// postDataForSearch();
		postData();
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("search_response", response);
				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	public void postDataForSearch() {
		String textUrl = prepareUrlForSearch();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("uid", uId));
		nameValuePairs.add(new BasicNameValuePair("keyword", keyword));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void postData() {
		String textUrl = prepareUrlForSearch();

		Map<String, String> params = new HashMap<String, String>();
		params.put("uid", uId);
		params.put("keyword", keyword);

		try {
			post(textUrl, params);
		} catch (IOException ex) {

		}
	}

	public String prepareUrlForSearch() {
		return String.format(UrlCollection.user_search_url);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractData(String res) {
		JSONArray jArray;
		JSONObject jObj;
		searchListData.clear();
		try {
			jArray = new JSONArray(res);
			ProfileData profileData;
			for (int i = 0; i < jArray.length(); i++) {
				jObj = jArray.getJSONObject(i);
				profileData = new ProfileData();
				profileData.setUid(jObj.getString("uid"));
				profileData.setFirstName(jObj.getString("firstname"));
				profileData.setLastName(jObj.getString("lastname"));
				profileData.setUserName(jObj.getString("username"));
				profileData.setEmail(jObj.getString("email"));
				profileData.setGender(jObj.getInt("gender"));
				profileData.setDob(jObj.getString("dob"));
				profileData.setLikes(jObj.getString("likes"));
				profileData.setDislike(jObj.getString("dislikes"));
				profileData.setFavBars(jObj.getString("fav_bars"));
				profileData.setDateCreated(jObj.getString("date_created"));
				profileData.setDateUpdated(jObj.getString("date_modified"));
				profileData.setStatus(jObj.getString("status"));
				profileData.setTagline(jObj.getString("tagline"));
				profileData
						.setIsShownToOthers(jObj.getInt("is_shown_to_other"));
				profileData.setAboutMe(jObj.getString("about_me"));
				profileData.setIsLogin(jObj.getInt("is_login"));
				profileData.setImagePath(jObj.getString("media"));
				searchListData.add(profileData);
			}
			serviceCallback.serviceEnd("search_data");

		} catch (JSONException j) {
			j.printStackTrace();
			serviceCallback.serviceEnd("search_error");
		}
	}

	/**
	 * Issue a POST request to the server.
	 * 
	 * @param endpoint
	 *            POST address.
	 * @param params
	 *            request parameters.
	 * @throws IOException
	 *             propagated from POST.
	 */
	private void post(String endpoint, Map<String, String> params)
			throws IOException {
		URL url;
		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=')
					.append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}
		String body = bodyBuilder.toString();
		byte[] bytes = body.getBytes();
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setFixedLengthStreamingMode(bytes.length);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");
			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();
			// handle the response
			int status = conn.getResponseCode();
			if (status != 200) {
				throw new IOException("Post failed with error code " + status);
			} else {
				StringBuffer sb = new StringBuffer();
				InputStream istream = conn.getInputStream();
				int ch = 0;
				while ((ch = istream.read()) != -1) {
					sb.append((char) ch);
				}
				istream.close();
				extractData(sb.toString());
			}
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

}
