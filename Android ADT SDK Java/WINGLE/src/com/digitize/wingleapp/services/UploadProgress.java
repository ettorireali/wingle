package com.digitize.wingleapp.services;

public interface UploadProgress {

	void uploadPercent(float percent, long bytesUploaded);

	void uploadStarted();

	void uploadEnded();

	void uploadError(String msg);
}
