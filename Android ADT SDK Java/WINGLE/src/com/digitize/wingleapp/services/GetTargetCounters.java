package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class GetTargetCounters implements Runnable {

	JSONArray dataToFeed = null;
	ServiceCallback serviceCallBack = null;
	public static final String SUCCESS_MESSAGE = "TargetSuccess";
	public static final String ERROR_MESSAGE = "TargetError";
	public static final int DEFAULT_USER_ID = 0;

	public GetTargetCounters(ServiceCallback serviceCallback) {
		this.serviceCallBack = serviceCallback;
		dataToFeed = new JSONArray();
	}

	public BasicNameValuePair getLocationParam() {
		return new BasicNameValuePair("loc_data", dataToFeed.toString());
	}

	public void addLocation(LatLng latLong) {
		JSONArray latlng = new JSONArray();
		try {
			latlng.put(latLong.latitude);
			latlng.put(latLong.longitude);
		} catch (JSONException jse) {

		}
		dataToFeed.put(latlng);
	}

	public void postDataForAction() {
		String textUrl = prepareUrlForAction();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(getLocationParam());

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public String prepareUrlForAction() {
		return String.format(UrlCollection.getTargetPlaces);
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallBack.serviceStarted(SUCCESS_MESSAGE);
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("user_action_response", response);

				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallBack.serviceEnd(ERROR_MESSAGE);
				} else {
					serviceCallBack.serviceEnd(ERROR_MESSAGE);
				}
				break;
			}
			}
		};
	};

	@Override
	public void run() {
		// TODO Auto-generated method stub
	}

	public void extractData(String res) {

	}
}
