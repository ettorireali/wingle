package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.pojo.ProximityData;
import com.google.android.gms.maps.model.LatLng;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class UpdateProfileService implements Runnable {

	ServiceCallback serviceCallback;
	Context context;
	String aboutMe;
	String dislikes;
	String favBars;
	String uid;
	String updateOption;
	String privacyPref;

	public String getPrivacyPref() {
		return privacyPref;
	}

	public void setPrivacyPref(String privacyPref) {
		this.privacyPref = privacyPref;
	}

	public String getUpdateOption() {
		return updateOption;
	}

	public void setUpdateOption(String updateOption) {
		this.updateOption = updateOption;
	}

	public static String UPDATE_ABOUT_ME = "update_about_me";
	public static String UPDATE_DISLIKES = "update_dislikes";
	public static String UPDATE_FAV_BARS = "update_fav_bars";
	public static String UPDATE_PRIVACY = "update_privacy";

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String about) {
		this.aboutMe = about;
	}

	public String getDislikes() {
		return dislikes;
	}

	public void setDislikes(String dislikes) {
		this.dislikes = dislikes;
	}

	public String getFavBars() {
		return favBars;
	}

	public void setFavBars(String favBars) {
		this.favBars = favBars;
	}

	public UpdateProfileService(ServiceCallback serviceCallback, Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
	}

	Handler handler = new Handler() {

		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("update_profile_response", response);

				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	@Override
	public void run() {
		// TODO Auto-generated method stub
		serviceCallback.serviceStarted("updateprofile_started");
		postDataForUpdateProfile();
	}

	public void postDataForUpdateProfile() {
		String textUrl = prepareUrlForUpdateProfile();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("uid", uid));
		if (updateOption.equals(UPDATE_ABOUT_ME)) {
			nameValuePairs.add(new BasicNameValuePair("about_me", aboutMe));
		} else if (updateOption.equals(UPDATE_DISLIKES)) {
			nameValuePairs.add(new BasicNameValuePair("dislikes", dislikes));
		} else if (updateOption.equals(UPDATE_FAV_BARS)) {
			nameValuePairs.add(new BasicNameValuePair("fav_bars", favBars));
		} else if (updateOption.equals(UPDATE_PRIVACY)) {
			nameValuePairs.add(new BasicNameValuePair("privacy_pref",
					privacyPref));
		}

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForUpdateProfile() {
		return String.format(UrlCollection.update_profile_url);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractData(String res) {
		JSONObject jsonObj = null;
		String message = null;
		String code = null;

		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");
			if (message.equalsIgnoreCase("User Updated Successfuly")) {
				serviceCallback.serviceEnd("update_success");
			} else if (message.equalsIgnoreCase("User Update facing a problem")) {
				serviceCallback.serviceEnd("update_problem");
			} else if (message
					.equalsIgnoreCase("Missing basic params for Update")) {
				serviceCallback.serviceEnd("params_missing");
			}

		} catch (JSONException j) {
			j.printStackTrace();
			serviceCallback.serviceEnd("update_error");
		}
	}
}
