package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.pojo.ProfileData;

import android.content.Context;
import android.graphics.Paint.Join;
import android.os.Handler;
import android.util.Log;

public class GetProfileService implements Runnable {

	ServiceCallback serviceCallback;
	Context context;
	JSONArray uId;
	ArrayList<ProfileData> profileDataList;
	Verifier verifier;

	public ArrayList<ProfileData> getProfileData() {
		return profileDataList;
	}

	public JSONArray getuId() {
		return uId;
	}

	public void setuId(JSONArray uId) {
		this.uId = uId;
	}

	public GetProfileService(ServiceCallback serviceCallback, Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
		profileDataList = new ArrayList<ProfileData>();
		verifier = new Verifier();
		verifier.prepareVerifier();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		serviceCallback.serviceStarted("profile_started");
		postDataForProfile();
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("profile_response", response);
				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	public void postDataForProfile() {
		String textUrl = prepareUrlForProfile();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("uid", uId.toString()));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForProfile() {
		return String.format(UrlCollection.profile_url);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractData(String res) {
		JSONArray jArray;
		JSONObject jObj;
		try {
			jArray = new JSONArray(res);
			ProfileData profileData;
			for (int i = 0; i < jArray.length(); i++) {
				jObj = jArray.getJSONObject(i);
				profileData = new ProfileData();
				profileData.setUid(jObj.getString("uid"));
				profileData.setFirstName(jObj.getString("firstname"));
				profileData.setLastName(jObj.getString("lastname"));
				profileData.setUserName(jObj.getString("username"));
				profileData.setEmail(jObj.getString("email"));
				profileData.setGender(jObj.getInt("gender"));
				profileData.setDob(jObj.getString("dob"));
				profileData.setLikes(jObj.getString("likes"));
				profileData.setDislike(jObj.getString("dislikes"));
				profileData.setFavBars(jObj.getString("fav_bars"));
				profileData.setDateCreated(jObj.getString("date_created"));
				profileData.setDateUpdated(jObj.getString("date_modified"));
				profileData.setStatus(jObj.getString("status"));
				profileData.setTagline(jObj.getString("tagline"));
				profileData
						.setIsShownToOthers(jObj.getInt("is_shown_to_other"));
				profileData.setAboutMe(jObj.getString("about_me"));
				profileData.setIsLogin(jObj.getInt("is_login"));
				profileData.setImagePath(jObj.getString("media"));
				profileData.setIp(jObj.getString("ip"));
				profileData.setPrivacy(jObj.getString("privacy_pref"));

				try {
					profileData
							.setPort(Integer.parseInt(jObj.getString("port")));
				} catch (Exception ex) {
					profileData.setPort(0);
				}
				profileData.setFollowing(verifier.isFollowing(profileData
						.getUid()));
				Log.w("follow", "follow : " + profileData.isFollowing());
				profileData
						.setBlocked(verifier.isBlocked(profileData.getUid()));
				Log.w("follow", "blocked : " + profileData.isBlocked());
				profileDataList.add(profileData);
			}
			serviceCallback.serviceEnd("profile_data");

		} catch (JSONException j) {
			j.printStackTrace();
			serviceCallback.serviceEnd("profile_error");
		}
	}

}
