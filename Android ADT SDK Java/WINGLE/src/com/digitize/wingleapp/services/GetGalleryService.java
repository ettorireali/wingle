package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.pojo.GalleryData;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class GetGalleryService implements Runnable {

	ServiceCallback serviceCallback;
	Context context;
	String uId;
	ArrayList<GalleryData> galleryList;

	public void setUidForGallery(String uid) {
		this.uId = uid;
	}

	public ArrayList<GalleryData> getGalleryList() {
		return galleryList;
	}

	public GetGalleryService(ServiceCallback serviceCallback, Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
		galleryList = new ArrayList<GalleryData>();

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		serviceCallback.serviceStarted("gallery_started");
		postDataForGallery();
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("gallery_response", response);
				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	public void postDataForGallery() {
		String textUrl = prepareUrlForGallery();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("uid", uId));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForGallery() {
		return String.format(UrlCollection.gallery_images_url);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractData(String res) {
		JSONArray jArray;
		JSONObject jObj;

		try {
			jArray = new JSONArray(res);
			GalleryData galleryData;
			for (int i = 0; i < jArray.length(); i++) {
				jObj = jArray.getJSONObject(i);
				galleryData = new GalleryData();
				galleryData.setImagePath(jObj.getString("media_path"));
				galleryData.setMid(jObj.getInt("mid"));
				galleryData.setMediaType(jObj.getInt("media_type"));
				galleryData.setUpdatedDate(jObj.getString("updated_date"));

				galleryList.add(galleryData);
				serviceCallback.serviceEnd("gallery_success");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			serviceCallback.serviceEnd("gallery_error");
		}

	}
}
