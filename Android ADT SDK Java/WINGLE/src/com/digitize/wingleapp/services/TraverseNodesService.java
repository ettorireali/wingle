package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

import android.os.Handler;
import android.util.Log;

public class TraverseNodesService implements Runnable {
	private String uid = null;
	private JSONArray jarr = null;
	private JSONArray result = null;
	public static final String SERVICE_NAME = "traverse_nodes";
	private final String PROGRESS_MESSAGE = "progress message";
	public static final String SUCCESS_MESSAGE = SERVICE_NAME + "success";
	public static final String ERROR_MESSAGE = SERVICE_NAME + "error";
	public static final int DEFAULT_USER_ID = 0;
	private ServiceCallback serviceCallback = null;

	public TraverseNodesService(ServiceCallback sCallback) {
		this.serviceCallback = sCallback;
		jarr = new JSONArray();
	}

	public JSONArray getResult() {
		return result;
	}

	public void feedNewLatLong(double lat, double lon) {
		JSONObject dataToFeed = new JSONObject();
		try {
			dataToFeed.put("lat", lat);
			dataToFeed.put("lon", lon);
		} catch (JSONException jse) {
		}
		jarr.put(dataToFeed);
	}

	public void feedNewLatLong(LatLng latLng) {
		JSONObject dataToFeed = new JSONObject();
		try {
			dataToFeed.put("lat", latLng.latitude);
			dataToFeed.put("lon", latLng.longitude);
		} catch (JSONException jse) {
		}
		jarr.put(dataToFeed);
	}

	public ServiceCallback getServiceCallback() {
		return serviceCallback;
	}

	public void setServiceCallback(ServiceCallback serviceCallback) {
		this.serviceCallback = serviceCallback;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void run() {
		postDataForAction();
	}

	public void postDataForAction() {
		String textUrl = prepareUrlForAction();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("uid", uid));
		nameValuePairs.add(new BasicNameValuePair("data", jarr.toString()));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForAction() {
		return String.format(UrlCollection.getTargetPlaces);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	Handler handler = new Handler() {

		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted(SUCCESS_MESSAGE);
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("user_action_response", response);

				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd(ERROR_MESSAGE);
				} else {
					serviceCallback.serviceEnd(ERROR_MESSAGE);
				}
				break;
			}
			}
		};
	};

	public void extractData(String res) {
		try {
			result = new JSONArray(res);
			serviceCallback.serviceEnd(SUCCESS_MESSAGE);
		} catch (JSONException jse) {
			serviceCallback.serviceEnd(ERROR_MESSAGE);
		}
	}
}
