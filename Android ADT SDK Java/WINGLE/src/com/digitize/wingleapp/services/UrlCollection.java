package com.digitize.wingleapp.services;

public class UrlCollection {

	// public static String
	// baseUrl="http://dev.digitize-info.com/php_projects/wingle/";
	public static String baseUrl = "http://50.87.153.14/~wingle/wingle/";

	public static String register_url = baseUrl + "register.php";
	public static String login_url = baseUrl + "login.php";
	public static String logout_url = baseUrl + "logout.php";
	public static String PRECHECKING_URL = baseUrl + "precheckin.php";
	public static String proximity_url = baseUrl + "proximity.php";
	public static String profile_url = baseUrl + "userlisting.php";
	public static String update_profile_url = baseUrl + "update_profile.php";
	public static String user_action_url = baseUrl + "follow_block.php";

	public static String user_search_url = baseUrl + "user_search.php";
	public static String add_places_url = baseUrl + "addnewplaces.php";
	public static String delete_place_url = baseUrl + "deleteplace.php";
	public static String getTargetPlaces = baseUrl + "traverse_nodes.php";
	public static String random_list_url = baseUrl + "getnewplaces.php";
	public static String REMOVE_PRECHECKIN = baseUrl + "delete_plcbook.php";

	// public static String
	// image_upload_url="http://192.168.1.16/tourapp/file_upload_service.php";
	/**
	 * used to upload the profile picture of the user on the server
	 * 
	 * @param Imagename
	 *            ,Mode,Uid,Media-type
	 */
	public static String image_upload_url = baseUrl + "add_media.php";

	/**
	 * used to get the gallery images of the user
	 * 
	 * @param uid
	 */
	public static String gallery_images_url = baseUrl + "getmedia.php";
	// public static String
	// placceook_url="http://dev.digitize-info.com/php_projects/wingle/services.php";
}
