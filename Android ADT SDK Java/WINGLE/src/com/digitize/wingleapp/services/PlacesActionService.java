package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import com.digitize.wingleapp.GlobalApp;
import com.digitize.wingleapp.pojo.ProfileData;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class PlacesActionService implements Runnable {

	ServiceCallback serviceCallback;
	Context context;
	String action;
	String uId;
	double latitude;
	double longitude;
	String placeTitle;
	String description;
	int placeId;
	ArrayList<ProfileData> randomPeopleList;

	public ArrayList<ProfileData> getRandomPeopleList() {
		return randomPeopleList;
	}

	public int getPlaceId() {
		return placeId;
	}

	public void setPlaceId(int placeId) {
		this.placeId = placeId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getPlaceTitle() {
		return placeTitle;
	}

	public void setPlaceTitle(String placeTitle) {
		this.placeTitle = placeTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static String ACTION_ADD_PLACES = "add";
	public static String ACTION_DELETE_PLACES = "delete";
	public static String ACTION_RANDOM_LISTING = "random_list";

	public PlacesActionService(ServiceCallback serviceCallback, Context context) {
		// TODO Auto-generated constructor stub
		this.serviceCallback = serviceCallback;
		this.context = context;
		this.randomPeopleList = new ArrayList<ProfileData>();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if (action.equals(ACTION_ADD_PLACES)) {
			postDataForAddPlaces();
			serviceCallback.serviceStarted("add_started");
		} else if (action.equals(ACTION_DELETE_PLACES)) {
			postDataForDeletePlaces();
			serviceCallback.serviceStarted("delete_started");
		} else if (action.equals(ACTION_RANDOM_LISTING)) {
			postDataForRandomList();
			serviceCallback.serviceStarted("random_started");
		}
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted("start:msg:");
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				if (action.equalsIgnoreCase(ACTION_ADD_PLACES)) {
					extractDataForAddPlaces(response);
					Log.w("addplace_response", response);
				} else if (action.equalsIgnoreCase(ACTION_DELETE_PLACES)) {
					extractDataForDeletePlaces(response);
					Log.w("deleteplace_response", response);
				} else if (action.equalsIgnoreCase(ACTION_RANDOM_LISTING)) {
					extractDataForRandomList(response);
					Log.w("randomlist_response", response);
				}
				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd("error");
				} else {
					serviceCallback.serviceInProgress("error");
				}
				break;
			}
			}
		};
	};

	public void postDataForAddPlaces() {
		String textUrl = prepareUrlForAddPlaces();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("uid", uId));
		nameValuePairs.add(new BasicNameValuePair("lat", String
				.valueOf(latitude)));
		nameValuePairs.add(new BasicNameValuePair("lon", String
				.valueOf(longitude)));
		nameValuePairs.add(new BasicNameValuePair("placetitle", placeTitle));
		nameValuePairs.add(new BasicNameValuePair("description", description));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void postDataForDeletePlaces() {
		String textUrl = prepareUrlForDeletePlaces();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs
				.add(new BasicNameValuePair("id", String.valueOf(placeId)));

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void postDataForRandomList() {
		String textUrl = prepareUrlForRandomList();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("uid", uId));
		nameValuePairs.add(new BasicNameValuePair("lat", String
				.valueOf(latitude)));
		nameValuePairs.add(new BasicNameValuePair("lon", String
				.valueOf(longitude)));
		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForRandomList() {
		return String.format(UrlCollection.random_list_url);
	}

	public String prepareUrlForAddPlaces() {
		return String.format(UrlCollection.add_places_url);
	}

	public String prepareUrlForDeletePlaces() {
		return String.format(UrlCollection.delete_place_url);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractDataForAddPlaces(String res) {
		JSONObject jsonObj = null;
		String message = null;
		String code = null;
		int random_place_id;

		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");
			random_place_id = jsonObj.getInt("id");
			GlobalApp.storePlaceIdInSharedPreference(context, random_place_id);
			serviceCallback.serviceEnd("added_place");
		} catch (JSONException jse) {
			serviceCallback.serviceEnd("add_error");
			jse.printStackTrace();
		}
	}

	public void extractDataForRandomList(String res) {
		JSONObject jsonObj = null;
		String message = null;
		String code = null;

		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");

			serviceCallback.serviceEnd("random_list");
		} catch (JSONException jse) {
			serviceCallback.serviceEnd("random_error");
			jse.printStackTrace();
		}
	}

	public void extractDataForDeletePlaces(String res) {

		JSONObject jsonObj = null;
		String message = null;
		String code = null;

		try {
			jsonObj = new JSONObject(res);
			message = jsonObj.getString("msg");
			code = jsonObj.getString("flg");
			serviceCallback.serviceEnd("deleted_place");
		} catch (JSONException jse) {
			serviceCallback.serviceEnd("delete_error");
			jse.printStackTrace();
		}
	}

}
