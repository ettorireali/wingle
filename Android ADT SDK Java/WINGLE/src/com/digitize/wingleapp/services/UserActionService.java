package com.digitize.wingleapp.services;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.util.Log;

public class UserActionService implements Runnable {

	public static final String SERVICE_NAME = "user_action";
	private final String PROGRESS_MESSAGE = "progress message";
	public static final String ACTION_FOLLOW = "follow";
	public static final String ACTION_UNFOLLOW = "unfollow";
	public static final String ACTION_BLOCK = "block";
	public static final String ACTION_UNBLOCK = "unblock";
	public static final String ACTION_GET_ALL_DETAIL = "all";
	public static final String SUCCESS_MESSAGE = "success";
	public static final String ERROR_MESSAGE = "error";
	public static final int DEFAULT_USER_ID = 0;
	private String actionType = null;
	private ServiceCallback serviceCallback = null;
	private long myId = DEFAULT_USER_ID;
	private long targetId = DEFAULT_USER_ID;
	private JSONArray myData = null;
	private String msg = null;
	public static final String MESSAGE_FORMAT = "{\"action_type\":\"%s\",\"who\":\"%s\",\"whom\":\"%s\",\"message\":\"%s\"}";

	public UserActionService(ServiceCallback serviceCallback) {
		this.serviceCallback = serviceCallback;
	}

	public String generateMessage(String customMessage) {
		return String.format(MESSAGE_FORMAT, actionType, myId,
				Long.toString(targetId), customMessage);
	}

	public ServiceCallback getServiceCallback() {
		return serviceCallback;
	}

	public void setServiceCallback(ServiceCallback serviceCallback) {
		this.serviceCallback = serviceCallback;
	}

	public long getMyId() {
		return myId;
	}

	public void setMyId(long myId) {
		this.myId = myId;
	}

	public long getTargetId() {
		return targetId;
	}

	public void setTargetId(long targetId) {
		this.targetId = targetId;
	}

	public JSONArray getMyData() {
		return myData;
	}

	public void setMyData(JSONArray myData) {
		this.myData = myData;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		serviceCallback.serviceStarted(SERVICE_NAME);
		postDataForAction();
	}

	Handler handler = new Handler() {

		public void handleMessage(android.os.Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				serviceCallback.serviceStarted(SUCCESS_MESSAGE);
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				String response = (String) message.obj;
				extractData(response);
				Log.w("user_action_response", response);

				break;
			}
			case HttpConnection.DID_ERROR: {
				Exception e = (Exception) message.obj;
				e.printStackTrace();

				if (e instanceof UnknownHostException) {
					serviceCallback.serviceEnd(ERROR_MESSAGE);
				} else {
					serviceCallback.serviceEnd(ERROR_MESSAGE);
				}
				break;
			}
			}
		};
	};

	public void postDataForAction() {
		String textUrl = prepareUrlForAction();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs
				.add(new BasicNameValuePair("by_id", String.valueOf(myId)));
		nameValuePairs.add(new BasicNameValuePair("to_id", String
				.valueOf(targetId)));
		nameValuePairs.add(new BasicNameValuePair("action", actionType));

		if (!actionType.equals(ACTION_GET_ALL_DETAIL)) {
			String msg = generateMessage("hello");
			nameValuePairs.add(new BasicNameValuePair("message", msg));
		}

		try {
			getAsyncdata(textUrl, new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String prepareUrlForAction() {
		return String.format(UrlCollection.user_action_url);
	}

	public void getAsyncdata(String txturl, UrlEncodedFormEntity urlEncoded) {
		new HttpConnection(handler).post(txturl, urlEncoded);
	}

	public void extractData(String res) {

		if (!actionType.equalsIgnoreCase(ACTION_GET_ALL_DETAIL)) {
			try {
				JSONObject jsonInstance = new JSONObject(res);
				int success = jsonInstance.getInt("success");
				if (success == 1) {
					serviceCallback.serviceEnd(SUCCESS_MESSAGE);
				} else {
					serviceCallback.serviceEnd(ERROR_MESSAGE);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				myData = new JSONArray(res);
				serviceCallback.serviceEnd(SUCCESS_MESSAGE);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				serviceCallback.serviceEnd(ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}
}
