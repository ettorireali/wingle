package com.digitize.wingleapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.http.impl.cookie.DateUtils;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import at.vcity.androidim.types.MessageInfo;

public class MessageHistoryAdapter extends BaseAdapter {

	ArrayList<Object> msgCollection = null;
	LayoutInflater layoutinfalter = null;
	String meId = null;
	private MessageInfo lastMsg;

	public MessageHistoryAdapter(Activity activity) {
		msgCollection = new ArrayList<Object>();
		this.layoutinfalter = LayoutInflater.from(activity);
	}

	public void setData(ArrayList<Object> data) {
		this.msgCollection = data;
	}

	public String getMeId() {
		return meId;
	}

	public void setMeId(String meId) {
		this.meId = meId;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return msgCollection.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return msgCollection.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public MessageInfo getLastMsg() {
		// TODO Auto-generated method stub
		return lastMsg;
	}
	
	public void setLastMsg(MessageInfo msg) {
		// TODO Auto-generated method stub
		lastMsg=msg;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View chatRightView = null, chatLeftView = null;
		CellItem rightMsgCell = null, leftMsgCell = null, msgCell = null,dateCell;
		Object obj = msgCollection.get(position);
		if(obj instanceof Date){
			Date dt = (Date) obj;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			dateCell = new CellItem();
			View dateView = layoutinfalter.inflate(R.layout.chat_date_item,
					null);
			dateCell.txtMsg = (TextView) dateView
					.findViewById(R.id.txt_chat_date_text);
			convertView = dateView;
			msgCell = dateCell;
			msgCell.txtMsg.setText(sdf.format(dt));
		}else{
		MessageInfo msgInfo = (MessageInfo) msgCollection.get(position);
		if (msgInfo.getSenderId().equals(meId)) {
			rightMsgCell = new CellItem();
			Log.i("receipient ============>", meId);
			chatRightView = layoutinfalter.inflate(R.layout.chat_right_item,
					null);
			rightMsgCell.txtMsg = (TextView) chatRightView
					.findViewById(R.id.txt_right_chat_text);
			rightMsgCell.imgSender = (ImageView) chatRightView
					.findViewById(R.id.chat_right_user_image);
			convertView = chatRightView;
			msgCell = rightMsgCell;
			msgCell.txtMsg.setText(msgInfo.getMessage());
		} else {
			leftMsgCell = new CellItem();
			Log.i("receipient ============>", msgInfo.getSenderId());
			chatLeftView = layoutinfalter
					.inflate(R.layout.chat_left_item, null);
			leftMsgCell.txtMsg = (TextView) chatLeftView
					.findViewById(R.id.txt_left_chat_text);
			leftMsgCell.imgSender = (ImageView) chatLeftView
					.findViewById(R.id.chat_left_user_image);
			convertView = chatLeftView;
			msgCell = leftMsgCell;
			msgCell.txtMsg.setText(msgInfo.getMessage());
			
		}
		convertView.setTag(msgCell);
		}
		return convertView;
	}

	private CellItem getDateCell(MessageInfo msgInfo,CellItem dateCell) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date curr = new Date(msgInfo.getTimeinmillis());
		dateCell.txtMsg.setText(sdf.format(curr));
		dateCell.txtMsg.setVisibility(View.VISIBLE);
		return dateCell;
	}

	private boolean isDateChanged(MessageInfo msgInfo) {
		// TODO Auto-generated method stub
		if(getLastMsg()==null){
			setLastMsg(msgInfo);
			return true;
		}	
		setLastMsg(msgInfo);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar curr = new GregorianCalendar();
		GregorianCalendar prev = new GregorianCalendar();
		curr.setTimeInMillis(msgInfo.getTimeinmillis());
		curr.set(Calendar.HOUR_OF_DAY, 0);
		curr.set(Calendar.MINUTE, 0);
		curr.set(Calendar.SECOND, 0);
		curr.set(Calendar.MILLISECOND, 1);
		prev.setTimeInMillis(getLastMsg().getTimeinmillis());
		prev.set(Calendar.HOUR_OF_DAY, 0);
		prev.set(Calendar.MINUTE, 0);
		prev.set(Calendar.SECOND, 0);
		prev.set(Calendar.MILLISECOND, 1);
		if(curr.after(prev)){
			
			return true;
		}
		
		return false;
		
		
	}

	class CellItem {
		TextView txtMsg = null;
		ImageView imgSender = null;
		TextView txtDate = null;
	}

}
