package com.digitize.wingleapp;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.GetProfileService;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.UserActionService;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import at.vcity.androidim.interfaces.IAppManager;
import at.vcity.androidim.services.IMService;
import at.vcity.androidim.types.MessageInfo;

public class SeePeopleFragmentActivity extends ActionBarActivity implements
		ServiceCallback {

	WingleDB wingleDB = null;
	UserActionService userActionService = null;
	SeePeopleFragment seePeopleFragment;
	ProgressDialog progressDialog = null;
	private IAppManager imService;
	private ChatFragment chatFragment = null;
	private MessageReceiver messageReceiver = new MessageReceiver();
	ProfileData profileData = null;
	SeePeopleDetailsFragment seePeopleDetailFragment = null;
	GetProfileService getProfileService;
	boolean notify = true;
	private NotificationManager mNM;
	private ArrayList<Object> messageCollection;
	private String chatHistoryUser;
	AQuery aQuery = null;

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			imService = ((IMService.IMBinder) service).getService();
		}

		public void onServiceDisconnected(ComponentName className) {
			imService = null;
			Toast.makeText(SeePeopleFragmentActivity.this,
					R.string.local_service_stopped, Toast.LENGTH_SHORT).show();
		}
	};
	protected ArrayList<ProfileData> profileList;
	private Menu optionsMenu;
	private ImageView imgUserProfile;
	private TextView mTitleTextView;

	@Override
	protected void onResume() {
		super.onResume();
		bindService(
				new Intent(SeePeopleFragmentActivity.this, IMService.class),
				mConnection, Context.BIND_AUTO_CREATE);
		IntentFilter intent = new IntentFilter();
		intent.addAction(IMService.TAKE_MESSAGE);
		registerReceiver(messageReceiver, intent);
		GlobalApp.CHAT_NOTIFY = false;
		if(imService!=null)
		imService.setNotifyFlag(false);
	}

	public void sendMessage(MessageInfo msgInfo) {
		imService.sendMessage(msgInfo);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unbindService(mConnection);
		GlobalApp.CHAT_NOTIFY = true;
		if(imService!=null)
		imService.setNotifyFlag(true);
	}

	public class MessageReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle extra = intent.getExtras();
			String username = extra.getString(MessageInfo.USERID);
			String message = extra.getString(MessageInfo.MESSAGETEXT);
			Log.w("data", message);
			if (username != null && message != null) {
				// message arrival and hand over to chat fragment
				MessageInfo mInfo = new MessageInfo();
				mInfo.setMessage(message);
				mInfo.setSenderId(username);
				mInfo.setTimeinmillis(System.currentTimeMillis());
				mInfo.setRecipientId(GlobalApp.getUserNameFromSharedPreference(context));
				if(getChatHistoryUser() != null && !getChatHistoryUser().equalsIgnoreCase(username)){
					ProfileData senderProfileData = getProfileForUsername(username);
					showNotification(username,message,senderProfileData);//can be dialog
					ChatCache.incrementUnreadMessages(username);
				}else{
					chatFragment.msgReceived(mInfo);
				}
				
			}
		}

		private ProfileData getProfileForUsername(String username) {
			// TODO Auto-generated method stub
			ProfileData profileData = GlobalApp.getProfileMap().get(username);
			if(profileData == null){
				profileData = wingleDB.getUserDataForUserName(username);
				GlobalApp.getProfileMap().put(username, profileData);
			}
			
			return profileData;
		}

	};

	/**
	 * Show a notification while this service is running.
	 * 
	 * @param msg
	 * @param senderProfileData 
	 **/
	private void showNotification(String username, String msg, ProfileData senderProfileData) {
		// Set the icon, scrolling text and timestamp
		int icon = R.drawable.ic_launcher;//
		Context context = getApplicationContext();
		Log.w("gennoti", "notification");
		long when = System.currentTimeMillis();
		Notification notification = new Notification(icon, msg, when);
		String title = context.getString(R.string.app_name);
		Intent notificationIntent = new Intent(context,
				SeePeopleFragmentActivity.class);
		notificationIntent.putExtra("from_random_places", senderProfileData);
		// set intent so it does not start a new activity
/*		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);*/
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, msg, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		mNM.notify(0, notification);

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		aQuery = new AQuery(this);
		wingleDB = new WingleDB(this);
		getProfileService = new GetProfileService(this, this);
		try {
			String memberIds;
			if((memberIds=GlobalApp.getMemberIds()) == null){
				wingleDB.open();
				memberIds = wingleDB.getAppCommon(memberIds);
				wingleDB.close();
			}
			getProfileService.setuId(new JSONArray(memberIds));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (GlobalApp.checkInternetConnection(this)) 
			new Thread(getProfileService).start();
		setContentView(R.layout.see_people_fragment_activity);
		seePeopleFragment = new SeePeopleFragment();
		userActionService = new UserActionService(this);
		progressDialog = new ProgressDialog(this);
		userActionService.setMyId(Long.parseLong(GlobalApp
				.getUidFromSharedPreference(this)));
		chatFragment = new ChatFragment();
		messageCollection = new ArrayList<Object>();
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		seePeopleDetailFragment = new SeePeopleDetailsFragment();
		Intent i = getIntent();
		profileData = (ProfileData) i
				.getSerializableExtra("from_random_places");
		if (profileData != null) {
			FragmentTransaction frgTransaction = getSupportFragmentManager()
					.beginTransaction();
			frgTransaction.add(R.id.frm_layout, seePeopleDetailFragment,
					"SeePeopleDetailFragment");
			seePeopleDetailFragment.setPeopleData(profileData);
			// frgTransaction.addToBackStack(null);
			frgTransaction.commit();
		} else {
			String memberIdsCollection = getIntent().getStringExtra(
					"member_ids");
			try {
				seePeopleFragment.setUidCollection(new JSONArray(
						memberIdsCollection));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			FragmentTransaction frgTransaction = getSupportFragmentManager()
					.beginTransaction();
			frgTransaction.add(R.id.frm_layout, seePeopleFragment,
					"SeePeopleFragment");
			// frgTransaction.addToBackStack(null);
			frgTransaction.commit();
		}

		userActionService = new UserActionService(this);
		progressDialog = new ProgressDialog(this);
		userActionService.setMyId(Long.parseLong(GlobalApp
				.getUidFromSharedPreference(this)));
		chatFragment = new ChatFragment();
		GlobalApp.CHAT_NOTIFY = false;
		if(imService!=null)
			imService.setNotifyFlag(false);
		

	}

	public void callUserAction(String actionType, long targetUserId) {
		userActionService.setTargetId(targetUserId);
		userActionService.setActionType(actionType);
		new Thread(userActionService).start();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		GlobalApp.CHAT_NOTIFY = true;
		if(imService!=null)
		imService.setNotifyFlag(true);
		try {
			unregisterReceiver(messageReceiver);
		} catch (Exception e) {

		}
		super.onBackPressed();
		
		getSupportFragmentManager().popBackStack();

	}

	@Override
	public void serviceStarted(final String msg) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				progressDialog.setMessage(msg);
				progressDialog.show();
			}
		});
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase(UserActionService.SUCCESS_MESSAGE)) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(SeePeopleFragmentActivity.this, "Followed",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase(UserActionService.ERROR_MESSAGE)) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(SeePeopleFragmentActivity.this, "Error",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase("profile_data")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					profileList = getProfileService.getProfileData();
					wingleDB.open();
					for (Iterator iterator = profileList.iterator(); iterator
							.hasNext();) {
						ProfileData type = (ProfileData) iterator.next();
						wingleDB.addUserData(type);
						GlobalApp.getProfileMap().put(type.getUserName(), type);
					}
					wingleDB.close();
				}
			});
		} 
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}

	public void switchChatFragment(ProfileData profData) {
		setProfileImg(profData);
		setChatHistoryUser(profData.getUserName());
		chatFragment.setProfileData(profData);
		Log.w("listview", "inside switchFragment");
		FragmentTransaction frgTransaction = getSupportFragmentManager()
				.beginTransaction();
		frgTransaction.add(R.id.frm_layout, chatFragment,
				"SeePeopleDetailsFragment");
		frgTransaction.addToBackStack("SeePeopleFragment");
		frgTransaction.commit();
	}
	

	public ArrayList<Object> getMessageCollection() {
		// TODO Auto-generated method stub
		return messageCollection;
	}
	
	public void setMessageCollection(ArrayList<Object> msgColl) {
		// TODO Auto-generated method stub
		messageCollection=msgColl;
	}

	public String getChatHistoryUser() {
		return chatHistoryUser;
	}

	public void setChatHistoryUser(String chatHistoryUser) {
		this.chatHistoryUser = chatHistoryUser;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		ActionBar mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.actionbar_chat, null);
		mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
		mTitleTextView.setText("WINGLE");
		imgUserProfile =  (ImageView)mCustomView
			.findViewById(R.id.imageView1);
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		return super.onCreateOptionsMenu(menu);
	}

	private void setProfileImg(ProfileData profile) {
		if(profile != null){
			ActionBar actionBar = getSupportActionBar();
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
			actionBar.setDisplayShowTitleEnabled(true);
			mTitleTextView.setText(profile.getFirstName());
			displayImage(profile.getImagePath());
		}
	}
	
	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("title");
	}
	
	public void displayImage(String url) {
		aQuery.id(imgUserProfile)
				.progress(R.id.icon_progress)
				.image(url, true, true, (int) (200), R.drawable.noimage,
						new BitmapAjaxCallback() {
							@Override
							protected void callback(String url, ImageView iv,
									Bitmap bm, AjaxStatus status) {
								// TODO Auto-generated method stub
								super.callback(url, iv, bm, status);
								Log.w("fired",
										url + " status : " + status.getCode());
							}
						});
	}
	



}
