package com.digitize.wingleapp;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.GetProfileService;
import com.digitize.wingleapp.services.ServiceCallback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class SeePeopleFragment extends Fragment implements ServiceCallback {
	ListView peopleList;
	ProgressDialog progressDialog;
	GetProfileService getProfileService;
	JSONArray jsonArr;
	SeePeopleAdapter peopleAdapter;
	ArrayList<ProfileData> profileArrayList;
	SeePeopleDetailsFragment peopleDetailsFragment;
	String tempUid[] = { "1", "8", "10" };
	SeePeopleFragmentActivity seePeopleFragmentActivity = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.see_people_fragment, null);
		peopleList = (ListView) view.findViewById(R.id.people_list);
		peopleList.setEmptyView(view.findViewById(R.id.empty));
		return view;
	}

	public void setUidCollection(JSONArray uidCollection) {
		this.jsonArr = uidCollection;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		peopleAdapter = new SeePeopleAdapter(getActivity(), getActivity());
		peopleDetailsFragment = new SeePeopleDetailsFragment();
		progressDialog = new ProgressDialog(getActivity());
		progressDialog.setTitle("WINGLE");
		progressDialog.setMessage("Getting data please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		peopleList.setOnItemClickListener(peopleListListener);
		getProfileService = new GetProfileService(SeePeopleFragment.this,
				getActivity());
		getProfileService.setuId(jsonArr);
		if (jsonArr != null && jsonArr.length() >= 1) {
			new Thread(getProfileService).start();
		} else {

		}
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		seePeopleFragmentActivity = (SeePeopleFragmentActivity) activity;
	}

	public OnItemClickListener peopleListListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view,
				int position, long arg3) {
			// TODO Auto-generated method stub
			ProfileData profile = (ProfileData) adapter
					.getItemAtPosition(position);

			if (!profile.getUid().equals(
					GlobalApp.getUidFromSharedPreference(getActivity()))) {
				peopleDetailsFragment.setPeopleData(profile);
				Log.w("listview", "inside onItemClickListener");
				if (!peopleDetailsFragment.isAdded()) {
					switchFragment();
				}
			}
		}
	};

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("profile_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("profile_data")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					profileArrayList = getProfileService.getProfileData();
					peopleAdapter.setData(profileArrayList);
					peopleList.setAdapter(peopleAdapter);
					peopleAdapter.notifyDataSetChanged();
				}
			});
		} else if (msg.equalsIgnoreCase("profile_error")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Problem parsing data",
							Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}

	public void switchFragment() {
		Log.w("listview", "inside switchFragment");
		FragmentTransaction frgTransaction = getFragmentManager()
				.beginTransaction();
		frgTransaction.add(R.id.frm_layout, peopleDetailsFragment,
				"SeePeopleDetailsFragment");
		frgTransaction.addToBackStack("SeePeopleFragment");

		frgTransaction.commit();
	}

}
