package com.digitize.wingleapp;

import java.util.ArrayList;
import java.util.HashMap;

import roboguice.fragment.RoboFragment;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.pojo.ProfileData;
import com.google.inject.Inject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import at.vcity.androidim.types.MessageInfo;

public class ChatFragment extends Fragment {

	WingleDB wingleDB;
	SeePeopleFragmentActivity parentFragment = null;
	ListView peopleList;
	MessageHistoryAdapter msgHistoryAdapter = null;
	private Button btnSend = null;
	ArrayList<Object> msgCollection = null;
	private EditText edtMsgToSend = null;
	private String myId = null;
	private String myUserName = null;
	private String myEmail = null;
	private String myPwd = null;
	private ProfileData profData = null;
	AQuery aQuery = null;

	public void setProfileData(ProfileData profData) {
		this.profData = profData;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		wingleDB = new WingleDB(getActivity());
		aQuery = new AQuery(getActivity());
		resetChatHistory(profData.getUserName());
		msgCollection = parentFragment.getMessageCollection();
		msgHistoryAdapter = new MessageHistoryAdapter(getActivity());
		myEmail = GlobalApp.getUserNameFromSharedPreference(getActivity());
		myUserName = GlobalApp.getUserNameFromSharedPreference(getActivity());
		myPwd = GlobalApp.getPasswordFromSharedPreference(getActivity());
		myId = myUserName;
		msgHistoryAdapter.setMeId(myId);
		msgHistoryAdapter.setData(msgCollection);
		peopleList.setAdapter(msgHistoryAdapter);
		peopleList.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		btnSend.setOnClickListener(sendMsg);
		msgHistoryAdapter.notifyDataSetChanged();
		
	}

	public void msgReceived(MessageInfo msgInfo) {
		//msgCollection.add(msgInfo);
		msgHistoryAdapter.notifyDataSetChanged();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		final View view = inflater.inflate(R.layout.chat_screen_main, null);
		peopleList = (ListView) view.findViewById(R.id.chat_list);
		peopleList.setEmptyView(view.findViewById(R.id.empty));
		btnSend = (Button) view.findViewById(R.id.btn_send);
		edtMsgToSend = (EditText) view.findViewById(R.id.edt_message);
		return view;
	}

	public MessageInfo prepareMsgInfo() {
		MessageInfo dataToReturn = null;
		String msgToSend = edtMsgToSend.getText().toString().trim();
		if (!msgToSend.equals("")) {
			dataToReturn = new MessageInfo();
			dataToReturn.setMessage(msgToSend);
			dataToReturn.setSenderId(myId);
		}
		return dataToReturn;
	}

	private OnClickListener sendMsg = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			MessageInfo preparedMsg = prepareMsgInfo();
			if (preparedMsg != null) {
				preparedMsg.setRecipientEmail(profData.getEmail());
				preparedMsg.setRecipientId(profData.getUserName());
				wingleDB.open();
				preparedMsg.setTimeinmillis(System.currentTimeMillis());
				wingleDB.addChat(preparedMsg);
				wingleDB.close();
				resetChatHistory(preparedMsg.getRecipientId());
				msgCollection.add(preparedMsg);
				msgHistoryAdapter.notifyDataSetChanged();
				parentFragment.sendMessage(preparedMsg);
			}
		}
	};

	private void resetChatHistory(String user) {
		ArrayList<Object> userChatHist;
		if((userChatHist = ChatCache.getChatHistoryForUsername(user))!=null){
			parentFragment.setMessageCollection(userChatHist);
		}else{
			wingleDB.open();
			parentFragment.setMessageCollection(wingleDB.getChatHistoryForSender(user));
			wingleDB.close();
			ChatCache.putChatHistoryForUsername(user, parentFragment.getMessageCollection());
		}
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		parentFragment = (SeePeopleFragmentActivity) activity;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	


}
