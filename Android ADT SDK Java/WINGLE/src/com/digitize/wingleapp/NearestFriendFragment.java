package com.digitize.wingleapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitize.wingleapp.pojo.Person;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

/**
 * Demonstrates heavy customisation of the look of rendered clusters.
 */
public class NearestFriendFragment implements
		ClusterManager.OnClusterClickListener<Person>,
		ClusterManager.OnClusterInfoWindowClickListener<Person>,
		ClusterManager.OnClusterItemClickListener<Person>,
		ClusterManager.OnClusterItemInfoWindowClickListener<Person> {
	private ClusterManager<Person> mClusterManager;
	private GoogleMap mMap;
	private Random mRandom = new Random(1984);
	private Activity parentActivity = null;
	private Fragment mapFragment;
	private LatLng currLatLng;
	private double searchDistanceInKms = 1000;

	/**
	 * Draws profile photos inside markers (using IconGenerator). When there are
	 * multiple people in the cluster, draw multiple photos (using
	 * MultiDrawable).
	 */

	class CustomInfoWindowAdapter implements InfoWindowAdapter {

		// These a both viewgroups containing an ImageView with id "badge" and
		// two TextViews with id
		// "title" and "snippet".
		private final View mWindow;
		private final View mContents;

		CustomInfoWindowAdapter() {
			mWindow = parentActivity.getLayoutInflater().inflate(
					R.layout.custom_info_window, null);
			mContents = parentActivity.getLayoutInflater().inflate(
					R.layout.custom_info_contents, null);

		}

		@Override
		public View getInfoWindow(Marker marker) {
			render(marker, mWindow);
			return mWindow;
		}

		@Override
		public View getInfoContents(Marker marker) {
			render(marker, mContents);
			return mContents;
		}

		private void render(Marker marker, View view) {
			int badge;
			// Use the equals() method on a Marker to check for equals. Do not
			// use ==.

			badge = 0;

			((ImageView) view.findViewById(R.id.badge)).setImageResource(badge);

			String title = marker.getTitle();
			TextView titleUi = ((TextView) view.findViewById(R.id.title));
			if (title != null) {
				// Spannable string allows us to edit the formatting of the
				// text.
				SpannableString titleText = new SpannableString(title);
				titleText.setSpan(new ForegroundColorSpan(Color.RED), 0,
						titleText.length(), 0);
				titleUi.setText(titleText);
			} else {
				titleUi.setText("");
			}

			String snippet = marker.getSnippet();
			TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
			if ((snippet != null) && (snippet.length() > 12)) {
				SpannableString snippetText = new SpannableString(snippet);
				snippetText.setSpan(new ForegroundColorSpan(Color.MAGENTA), 0,
						10, 0);
				snippetText.setSpan(new ForegroundColorSpan(Color.BLUE), 12,
						snippet.length(), 0);
				snippetUi.setText(snippetText);
			} else {
				snippetUi.setText("");
			}
		}
	}

	public NearestFriendFragment(Activity parent, Fragment mapFragment) {
		this.parentActivity = parent;
		this.mapFragment = mapFragment;

	}

	private class PersonRenderer extends DefaultClusterRenderer<Person> {
		private final IconGenerator mIconGenerator = new IconGenerator(
				parentActivity.getApplicationContext());
		private final IconGenerator mClusterIconGenerator = new IconGenerator(
				parentActivity.getApplicationContext());
		private final ImageView mImageView;
		private final ImageView mClusterImageView;
		private final int mDimension;

		public PersonRenderer() {
			super(parentActivity.getApplicationContext(), mMap, mClusterManager);

			View multiProfile = parentActivity.getLayoutInflater().inflate(
					R.layout.marker_with_counter, null);
			mClusterIconGenerator.setContentView(multiProfile);
			mClusterImageView = (ImageView) multiProfile
					.findViewById(R.id.personimage);
			mImageView = new ImageView(parentActivity.getApplicationContext());
			mDimension = (int) parentActivity.getResources().getDimension(
					R.dimen.custom_profile_image);
			mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension,
					mDimension));
			int padding = (int) parentActivity.getResources().getDimension(
					R.dimen.custom_profile_padding);
			mImageView.setPadding(padding, padding, padding, padding);
			mIconGenerator.setContentView(mImageView);

		}

		@Override
		protected void onBeforeClusterItemRendered(Person person,
				MarkerOptions markerOptions) {
			// Draw a single person.
			// Set the info window to show their name.
			double dist = SphericalUtil.computeDistanceBetween(getCurrLatLng(),
					person.getPosition());
			mImageView.setImageResource(person.profilePhoto);
			Bitmap icon = mIconGenerator.makeIcon();
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(
					person.name);
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
					.snippet(String.format("Distance : %s", dist));
		}

		@Override
		protected void onBeforeClusterRendered(Cluster<Person> cluster,
				MarkerOptions markerOptions) {
			// Draw multiple people.
			// Note: this method runs on the UI thread. Don't spend too much
			// time in here (like in this example).
			List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4,
					cluster.getSize()));
			int width = mDimension;
			int height = mDimension;

			for (Person p : cluster.getItems()) {
				// Draw 4 at most.
				if (profilePhotos.size() == 4)
					break;
				Drawable drawable = parentActivity.getResources().getDrawable(
						p.profilePhoto);
				drawable.setBounds(0, 0, width, height);
				profilePhotos.add(drawable);
			}
			MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
			multiDrawable.setBounds(0, 0, width, height);

			mClusterImageView.setImageDrawable(multiDrawable);
			Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster
					.getSize()));
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
		}

		@Override
		protected boolean shouldRenderAsCluster(Cluster cluster) {
			// Always render clusters.
			return cluster.getSize() > 1;
		}
	}

	@Override
	public boolean onClusterClick(Cluster<Person> cluster) {
		// Show a toast with some info when the cluster is clicked.
		String firstName = cluster.getItems().iterator().next().name;
		Toast.makeText(parentActivity.getApplicationContext(),
				cluster.getSize() + " (including " + firstName + ")",
				Toast.LENGTH_SHORT).show();
		return true;
	}

	@Override
	public void onClusterInfoWindowClick(Cluster<Person> cluster) {
		// Does nothing, but you could go to a list of the users.
	}

	@Override
	public boolean onClusterItemClick(Person item) {
		// Does nothing, but you could go into the user's profile page, for
		// example.
		return false;
	}

	@Override
	public void onClusterItemInfoWindowClick(Person item) {
		// Does nothing, but you could go into the user's profile page, for
		// example.
	}

	protected void startDemo() {
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(getCurrLatLng(),
				10.5f));
		mClusterManager = new ClusterManager<Person>(
				parentActivity.getApplicationContext(), mMap);
		mClusterManager.setRenderer(new PersonRenderer());
		mMap.setOnCameraChangeListener(mClusterManager);
		mMap.setOnMarkerClickListener(mClusterManager);
		mMap.setOnInfoWindowClickListener(mClusterManager);
		mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
		mClusterManager.setOnClusterClickListener(this);
		mClusterManager.setOnClusterInfoWindowClickListener(this);
		mClusterManager.setOnClusterItemClickListener(this);
		mClusterManager.setOnClusterItemInfoWindowClickListener(this);

		addItems();
		mClusterManager.cluster();
	}

	private void addItems() {
		// http://www.flickr.com/photos/sdasmarchives/5036248203/
		Person walter = new Person(position(), "Walter", R.drawable.walter);

		// if(isPersonWithinRadious(walter))
		mClusterManager.addItem(walter);

		// http://www.flickr.com/photos/usnationalarchives/4726917149/
		mClusterManager.addItem(new Person(position(), "Gran",
				R.drawable.walter));

		// http://www.flickr.com/photos/nypl/3111525394/
		mClusterManager.addItem(new Person(position(), "Ruth",
				R.drawable.walter));

		// http://www.flickr.com/photos/smithsonian/2887433330/
		mClusterManager.addItem(new Person(position(), "Stefan",
				R.drawable.walter));

		// http://www.flickr.com/photos/library_of_congress/2179915182/
		mClusterManager.addItem(new Person(position(), "Mechanic",
				R.drawable.walter));

		// http://www.flickr.com/photos/nationalmediamuseum/7893552556/
		mClusterManager.addItem(new Person(position(), "Yeats",
				R.drawable.walter));

		// http://www.flickr.com/photos/sdasmarchives/5036231225/
		mClusterManager.addItem(new Person(position(), "John",
				R.drawable.walter));

		// http://www.flickr.com/photos/anmm_thecommons/7694202096/
		mClusterManager.addItem(new Person(position(), "Trevor the Turtle",
				R.drawable.walter));

		// http://www.flickr.com/photos/usnationalarchives/4726892651/
		mClusterManager.addItem(new Person(position(), "Teach",
				R.drawable.walter));
	}

	private LatLng position() {
		return new LatLng(random(getCurrLatLng().latitude,
				getCurrLatLng().latitude - 0.38494009999999), random(
				getCurrLatLng().longitude,
				getCurrLatLng().longitude - 0.3514683));
	}

	private double random(double min, double max) {
		return (mRandom.nextDouble() * (max - min)) + min;
	}

	public void setUpMapIfNeeded() {
		mMap = ((SupportMapFragment) mapFragment.getFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		if (mMap != null) {
			mMap.clear();
			startDemo();
		}
	}

	public boolean isPersonWithinRadious(Person person) {
		double dist = SphericalUtil.computeDistanceBetween(getCurrLatLng(),
				person.getPosition());
		if (dist < (getSearchDistanceInKms() * 1000)) {
			return true;
		}
		return false;
	}

	public double getSearchDistanceInKms() {
		return searchDistanceInKms;
	}

	public void setSearchDistanceInKms(double searchDistanceInKms) {
		this.searchDistanceInKms = searchDistanceInKms;
	}

	public LatLng getCurrLatLng() {
		return currLatLng;
	}

	public void setCurrLatLng(LatLng currLatLng) {
		this.currLatLng = currLatLng;
	}

}
