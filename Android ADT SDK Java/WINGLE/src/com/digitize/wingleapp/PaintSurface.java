package com.digitize.wingleapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

public class PaintSurface extends SurfaceView implements Runnable,
		OnTouchListener {

	// Surface holder allows to control and monitor the surface
	private SurfaceHolder mHolder;
	// A thread where the painting activities are taking place
	private Thread mThread;
	// A flag which controls the start and stop of the repainting of the
	// SurfaceView
	private boolean mFlag = false;

	// X coordinate of the touched position
	private float mX;

	// Y Coordinate of the touched position
	private float mY;

	public float getmX() {
		return mX;
	}

	public void setmX(float mX) {
		this.mX = mX;
	}

	public float getmY() {
		return mY;
	}

	public void setmY(float mY) {
		this.mY = mY;
	}

	public void setPoint(Point point) {
		this.mX = point.x;
		this.mY = point.y;
		invalidate();

	}

	// Paint
	private Paint mPaint;

	public PaintSurface(Context context, AttributeSet attrs) {
		super(context, attrs);
		// Getting the holder
		mHolder = getHolder();
	}

	private Bitmap punchAHoleInABitmap(Bitmap foreground) {
		Bitmap bitmap = Bitmap.createBitmap(foreground.getWidth(),
				foreground.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		Paint paint = new Paint();
		canvas.drawBitmap(foreground, 0, 0, paint);
		paint.setAntiAlias(true);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		float radius = (float) (GlobalApp.screenWidth * .15);

		canvas.drawCircle(mX, mY, radius, paint);
		return bitmap;
	}

	public void resume() {
		// Instantiating the thread
		mThread = new Thread(this);
		// setting the mFlag to true for start repainting
		mFlag = true;
		// Start repaint the SurfaceView
		// mThread.start();
	}

	public void pause() {
		mFlag = false;
	}

	Bitmap foreG = null;

	public void setForeG(Bitmap bitmap) {
		this.foreG = bitmap;
	}

	@Override
	public void run() {

		synchronized (mHolder) {

			if (!mHolder.getSurface().isValid()) {
				Log.w("surface", "not valid surface");
			} else {
				Log.w("surface", "yes valid surface");
				for (int i = 1; i <= 2; i++) {
					Canvas canvas = mHolder.lockCanvas();
					onDraw(canvas);

					mHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
		// Start editing the surface
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
}
