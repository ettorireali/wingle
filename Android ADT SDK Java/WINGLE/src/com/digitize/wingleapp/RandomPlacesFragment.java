package com.digitize.wingleapp;

import java.util.ArrayList;

import org.json.JSONArray;

import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.GetProfileService;
import com.digitize.wingleapp.services.PlacesActionService;
import com.digitize.wingleapp.services.ServiceCallback;
import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class RandomPlacesFragment extends Fragment implements ServiceCallback {

	Button btnAdd;
	Button btnDelete;
	Button btnRandomList;
	MainActivity mainActivity = null;
	PlacesActionService placesActionService;
	GetProfileService getProfileService;
	ProgressDialog progressDialog;
	ArrayList<ProfileData> profileList;
	JSONArray jArray;
	SeePeopleAdapter peopleAdapter;
	ListView randomList;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mainActivity = (MainActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.random_places_fragment, null);
		btnAdd = (Button) view.findViewById(R.id.btn_add_place);
		randomList = (ListView) view.findViewById(R.id.random_list);
		randomList.setEmptyView(view.findViewById(R.id.empty));
		// btnDelete=(Button)view.findViewById(R.id.btn_delete_places);
		// btnRandomList=(Button)view.findViewById(R.id.btn_random_list);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		randomList.setOnItemClickListener(randomItemListener);
		getProfileService = new GetProfileService(this, getActivity());
		if (GlobalApp.getPlaceIdFromSharedPreference(getActivity()) != -1) {
			btnAdd.setText("Cancel");
		} else {
			btnAdd.setText("Go Random");
		}
		btnAdd.setOnClickListener(addPlaceListener);
		// btnDelete.setOnClickListener(deletePlaceListener);
		// btnRandomList.setOnClickListener(randomListListener);
		placesActionService = new PlacesActionService(this, getActivity());
		peopleAdapter = new SeePeopleAdapter(getActivity(), getActivity());
		jArray = new JSONArray();
		jArray.put("1");
		jArray.put("21");
		getProfileService.setuId(jArray);
		if (GlobalApp.checkInternetConnection(getActivity())) {
			new Thread(getProfileService).start();
		} else {
			Toast.makeText(getActivity(), "Check your internet connection",
					Toast.LENGTH_SHORT).show();
		}

	}

	public OnItemClickListener randomItemListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view,
				int position, long arg3) {
			// TODO Auto-generated method stub
			ProfileData profData = (ProfileData) adapter
					.getItemAtPosition(position);
			if (profData != null) {
				Intent inte = new Intent(getActivity(),
						SeePeopleFragmentActivity.class);
				inte.putExtra("from_random_places", profData);
				startActivity(inte);
			}
		}
	};
	public OnClickListener addPlaceListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			if (GlobalApp.checkInternetConnection(getActivity())) {
				if (GlobalApp.getPlaceIdFromSharedPreference(getActivity()) != -1) {
					placesActionService
							.setAction(PlacesActionService.ACTION_DELETE_PLACES);
					placesActionService.setPlaceId(GlobalApp
							.getPlaceIdFromSharedPreference(getActivity()));
					new Thread(placesActionService).start();

				} else {
					LatLng myLatLon = mainActivity.getMyLatLon();
					if (myLatLon != null) {
						placesActionService
								.setAction(PlacesActionService.ACTION_ADD_PLACES);
						placesActionService.setuId(GlobalApp
								.getUidFromSharedPreference(getActivity()));
						placesActionService.setLatitude(myLatLon.latitude);
						placesActionService.setLongitude(myLatLon.longitude);
						placesActionService.setPlaceTitle("Untitled Place");
						placesActionService
								.setDescription("Dont know about this place");
						new Thread(placesActionService).start();
					}
				}
			} else {
				Toast.makeText(getActivity(), "Check your internet connection",
						Toast.LENGTH_SHORT).show();
			}
		}
	};
	public OnClickListener deletePlaceListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

		}
	};
	public OnClickListener randomListListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (GlobalApp.checkInternetConnection(getActivity())) {
				LatLng myLatLon = mainActivity.getMyLatLon();
				placesActionService
						.setAction(PlacesActionService.ACTION_RANDOM_LISTING);
				placesActionService.setuId(GlobalApp
						.getUidFromSharedPreference(getActivity()));
				placesActionService.setLatitude(myLatLon.latitude);
				placesActionService.setLongitude(myLatLon.longitude);
				new Thread(placesActionService).start();
			} else {
				Toast.makeText(getActivity(), "Check your internet connection",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("add_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.setMessage("Adding Place...");
					progressDialog.show();
				}
			});
		} else if (msg.equalsIgnoreCase("delete_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.setMessage("Deleting Place...");
					progressDialog.show();
				}
			});
		} else if (msg.equalsIgnoreCase("random_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.setMessage("Getting listing...");
					progressDialog.show();
				}
			});
		} else if (msg.equalsIgnoreCase("profile_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("added_place")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Place Added",
							Toast.LENGTH_SHORT).show();
					btnAdd.setText("Cancel");
				}
			});
		} else if (msg.equalsIgnoreCase("deleted_place")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Places Deleted",
							Toast.LENGTH_SHORT).show();
					btnAdd.setText("Go Random");
					GlobalApp.clearPlaceIdFromSharedPreference(getActivity());
				}
			});
		} else if (msg.equalsIgnoreCase("add_error")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Error in add place",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase("random_list")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
				}
			});
		} else if (msg.equalsIgnoreCase("random_error")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Problem in json data",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase("profile_data")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					profileList = getProfileService.getProfileData();
					peopleAdapter.setData(profileList);
					randomList.setAdapter(peopleAdapter);
					peopleAdapter.notifyDataSetChanged();
				}
			});
		} else if (msg.equalsIgnoreCase("profile_error")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Problem parsing data",
							Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}
}
