package com.digitize.wingleapp;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import roboguice.RoboGuice;

import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.places.PlaceItem;
import com.digitize.wingleapp.pojo.PreferenceSet;
import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.pojo.ProximityData;
import com.digitize.wingleapp.services.AppConfig;
import com.google.android.gms.internal.ed;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.google.maps.android.SphericalUtil;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.EditText;

public class GlobalApp{

	public static String android_Id;
	public static String appDirectoryName = "WINGLE";
	public static String imageDirName = "ProfilePicture";
	public static int DIR_TYPE_IMAGE = 103;
	public static final long HOUR = 3600 * 1000;
	public static final String CACHED_FILE_NAME = "user_action.txt";
	private static final String KEY_MEMBER_IDS = "member_ids";
	public static int PORT_TO_LISTEN = 9035;
	public static String cacheDirName = "cached";
	public static int screenWidth = 1276;
	public static int screenHeight = 800;
	public static boolean CHAT_NOTIFY = false;
	public static HashMap<String,ProfileData> PROFILE_MAP = new HashMap<String,ProfileData>();
	public static String memberIds;

	public static int generatePort() {
		Random random = new Random();
		PORT_TO_LISTEN = 9000 + random.nextInt(1000);
		return PORT_TO_LISTEN;
	}

	public static String createImageFolder(String uid) {
		File temp = null;
		temp = new File(getImagePathFull() + File.separator + uid);
		if (!temp.exists())
			temp.mkdirs();
		return temp.getAbsolutePath();
	}

	public static String getImagePathFull() {
		return getAppDirPathFull() + File.separator + imageDirName;
	}

	public static String getAppDirPathFull() {
		return Environment.getExternalStorageDirectory() + File.separator
				+ appDirectoryName;
	}

	public static String getCachedPathFull() {
		return getAppDirPathFull() + File.separator + cacheDirName;
	}

	public static void manageAppFolders() {
		File parentDir = new File(GlobalApp.getAppDirPathFull());
		if (!parentDir.exists()) {
			parentDir.mkdir();
		}
		File imageFolder = new File(GlobalApp.getImagePathFull());
		if (!imageFolder.exists()) {
			imageFolder.mkdir();
		}
		File cachedFolder = new File(GlobalApp.getCachedPathFull());
		if (!cachedFolder.exists()) {
			cachedFolder.mkdir();
		}
	}

	public static boolean isBlank(EditText edt) {
		if (edt.getText().toString().trim().equals(""))
			return true;
		return false;
	}

	/**
	 * @param edt
	 *            edit text reference
	 * @param context
	 *            activity reference
	 * @param text
	 *            string that user want to display
	 */
	public static void takeDefaultAction(EditText edt, Context context,
			String text) {
		edt.setTextColor(Color.RED);
		edt.setError(text);
		edt.requestFocus();

	}

	public static boolean checkInternetConnection(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		// test for connection
		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			Log.w("internet", "Internet Connection Not Present");
			return false;
		}
	}

	public static String getDeviceId(Context c) {
		android_Id = Secure
				.getString(c.getContentResolver(), Secure.ANDROID_ID);
		return android_Id;
	}

	public static double distanceFromCurrentLocation(double lat1, double lng1,
			double lat2, double lng2) {
		double earthRadius = 3958.75;
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
				* Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;
		int meterConversion = 1609;
		// return new Float(dist * meterConversion).floatValue();
		return (dist * meterConversion) / 1000.0;
	}

	public synchronized static PlaceItem getTargetPlaceItem(
			ArrayList<PlaceItem> placeCollection, ProximityData proxData) {
		PlaceItem dataToReturn = null;
		Iterator<PlaceItem> placeDataIterator = placeCollection.iterator();
		PlaceItem tempInstance = null;
		double currentDistance = 0;
		while (placeDataIterator.hasNext()) {
			tempInstance = placeDataIterator.next();
			currentDistance = Math.abs(SphericalUtil.computeDistanceBetween(
					proxData.getPosition(), tempInstance.getPosition()));
			if (currentDistance < 20) {
				dataToReturn = tempInstance;
			}
		}
		return dataToReturn;
	}

	public synchronized static PlaceItem getTargetPlaceItem(
			ArrayList<PlaceItem> placeCollection, LatLng proxData) {
		PlaceItem dataToReturn = null;
		Iterator<PlaceItem> placeDataIterator = placeCollection.iterator();
		PlaceItem tempInstance = null;
		double currentDistance = 0;
		while (placeDataIterator.hasNext()) {
			tempInstance = placeDataIterator.next();
			currentDistance = Math.abs(SphericalUtil.computeDistanceBetween(
					proxData, tempInstance.getPosition()));
			if (currentDistance < 20) {
				dataToReturn = tempInstance;
			}
		}
		return dataToReturn;
	}

	public static synchronized PlaceItem associateCountersWithMarkers(
			ArrayList<PlaceItem> placeItemCollection,
			ArrayList<ProximityData> proxDataCollection, String personId,
			WingleDB wingleDB) {
		PlaceItem targetPlaceItem = getTargetPlaceItem(placeItemCollection,
				proxDataCollection.get(0));
		Iterator<ProximityData> proxDataIterator = proxDataCollection
				.iterator();
		ProximityData tempProxInstance = null;
		if (targetPlaceItem != null) {
			targetPlaceItem.resetFields();
			while (proxDataIterator.hasNext()) {
				tempProxInstance = proxDataIterator.next();
				targetPlaceItem.associateProx(tempProxInstance);
			}
			wingleDB.open();
			wingleDB.associatePlaceData(targetPlaceItem, personId);
			wingleDB.close();

		}
		return targetPlaceItem;
	}

	public static double distanceFromCurrentLocation(LatLng first, LatLng second) {
		double earthRadius = 3958.75;
		double dLat = Math.toRadians(second.latitude - first.latitude);
		double dLng = Math.toRadians(second.longitude - first.longitude);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(first.latitude))
				* Math.cos(Math.toRadians(second.latitude))
				* Math.sin(dLng / 2) * Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;
		int meterConversion = 1609;
		// return new Float(dist * meterConversion).floatValue();
		return (dist * meterConversion) / 1000.0;
	}

	public static double round(double unrounded, int precision, int roundingMode) {
		BigDecimal bd = new BigDecimal(unrounded);
		BigDecimal rounded = bd.setScale(precision, roundingMode);
		return rounded.doubleValue();
	}

	public static boolean isLoginUserFromSharedPreference(Context context) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		return sp.getBoolean("isLogin", false);
	}

	public static void clearUserSharedPreference(Context context) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.clear();
		editor.commit();
	}

	public static void storeUserNameInSharedPreference(Context context,
			String userName) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("user_name", userName);
		// editor.putBoolean("isLogin", true);
		editor.commit();
	}

	public static String getUserNameFromSharedPreference(Context context) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		return sp.getString("user_name", "unavailable");
	}

	public static void storePasswordInSharedPreference(Context context,
			String pass) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("user_password", pass);
		// editor.putBoolean("isLogin", true);
		editor.commit();
	}

	public static String getPasswordFromSharedPreference(Context context) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		return sp.getString("user_password", "unavailable");
	}

	public static void storeUidInSharedPreference(Context context, String uid) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("uid", uid);
		// editor.putBoolean("isLogin", true);
		editor.commit();
	}

	public static String getUidFromSharedPreference(Context context) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		return sp.getString("uid", "unavailable");
	}

	public static void clearUidFromSharedPreference(Context context) {
		SharedPreferences share = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor edt = share.edit();
		edt.putString("uid", "unavailable");
		edt.commit();
	}

	public static void storeProfilePicUrlInSharedPreference(Context context,
			String url) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("profile_pic", url);

		// editor.putBoolean("isLogin", true);
		editor.commit();
	}

	public static String getProfilePicUrlFromSharedPreference(Context context) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		return sp.getString("profile_pic", "unavailable");
	}

	/**
	 * 
	 * @param context
	 * @param pass
	 * @description it will set default proximity in KM (kilometers)
	 */

	public static void setDefaultProximity(Context context, int km) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt("proximity", km);
		// editor.putBoolean("isLogin", true);
		editor.commit();
	}

	public static void storePlaceIdInSharedPreference(Context context,
			int randomPlaceId) {
		SharedPreferences shared = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = shared.edit();
		editor.putInt(formKeyForRandomPlace(context), randomPlaceId);
		editor.commit();
	}

	public static int getPlaceIdFromSharedPreference(Context context) {
		SharedPreferences shared = context.getSharedPreferences("wingleApp", 0);
		return shared.getInt(formKeyForRandomPlace(context), -1);
	}

	public static void clearPlaceIdFromSharedPreference(Context context) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt("random_place_id", -1);
		editor.commit();
	}

	/**
	 * 
	 * @param context
	 * @return no of kilometers if not set it will
	 *         AppConfig.DEFAULT_KM_FOR_QUERY
	 */
	public static int getDefaultProximity(Context context) {
		SharedPreferences sp = context.getSharedPreferences("wingleApp", 0);
		return sp.getInt("proximity", AppConfig.DEFAULT_KM_FOR_QUERY);
	}

	public static String formKeyPrivacy(Context context) {
		return "privacy:" + getUidFromSharedPreference(context);
	}

	public static String formKeyForRandomPlace(Context context) {
		return "random_place_id:" + getUidFromSharedPreference(context);
	}

	public static void storePrivacyToPref(Context context, String prefSettings) {
		SharedPreferences shared = context.getSharedPreferences("wingleApp", 0);
		SharedPreferences.Editor editor = shared.edit();
		editor.putString(formKeyPrivacy(context), prefSettings);
		editor.commit();
	}

	public static String getPrivacyFromSharedPreference(Context context) {
		SharedPreferences shared = context.getSharedPreferences("wingleApp", 0);
		return shared.getString(formKeyPrivacy(context),
				PreferenceSet.prefSettings);
	}
	public static Object getDefultIfNull(Object obj,Object defaultObj){
		try {
			return Preconditions.checkNotNull(obj);
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			return defaultObj;
		}
	}

	public static void setProfileMap(HashMap<String,ProfileData> profileList) {
		// TODO Auto-generated method stub
		PROFILE_MAP.putAll(profileList);
		
	}
	
	public static HashMap<String,ProfileData>  getProfileMap() {
		// TODO Auto-generated method stub
		return PROFILE_MAP;
		
	}

	public static void setMemberIds(String targetUserIds) {
		// TODO Auto-generated method stub
		memberIds=new String(targetUserIds);
		
	}
	
	public static String getMemberIds() {
		// TODO Auto-generated method stub
		return memberIds;
		
	}
}
