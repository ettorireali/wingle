package com.digitize.wingleapp.pojo;

import com.google.android.gms.maps.model.LatLng;

public class ProximityData {

	String uid;
	private LatLng position = null;
	double distance;
	int placeId;

	public LatLng getPosition() {

		return position;
	}

	public void setPosition(LatLng position) {
		this.position = position;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public int getPlaceId() {
		return placeId;
	}

	public void setPlaceId(int placeId) {
		this.placeId = placeId;
	}

}
