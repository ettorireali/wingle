package com.digitize.wingleapp.pojo;

import org.json.JSONException;
import org.json.JSONObject;

public class PreferenceSet {
	public static final String prefSettings = "{\"email\":0,\"dob\":0,\"contact\":0}";
	public static final int DEFAULT = 0;
	public static final int PRIVATE = 1;
	public static final int FOLLOWERS_ONLY = 2;
	public static final int PUBLIC = 3;
	private JSONObject currentSettings = null;

	public PreferenceSet() {
		try {
			currentSettings = new JSONObject(prefSettings);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setPrivacy(String privacy) {
		try {
			currentSettings = new JSONObject(privacy);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getPrivacyStatus(String fieldName) {
		String dataToReturn = "default";
		try {
			int privacyStatus = currentSettings.getInt(fieldName);
			if (privacyStatus == DEFAULT) {
				dataToReturn = "Default";
			} else if (privacyStatus == PRIVATE) {
				dataToReturn = "Private";
			} else if (privacyStatus == PUBLIC) {
				dataToReturn = "Public";
			} else if (privacyStatus == FOLLOWERS_ONLY) {
				dataToReturn = "Followers Only";
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			dataToReturn = "Default";
		}
		return dataToReturn;
	}

	public String getPrivacy() {
		return currentSettings.toString();
	}

	public void setFieldVisibility(String fieldName, int visibility) {
		try {
			currentSettings.put(fieldName, visibility);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
