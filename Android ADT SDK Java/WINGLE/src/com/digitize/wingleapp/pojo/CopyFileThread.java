package com.digitize.wingleapp.pojo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.digitize.wingleapp.services.ServiceCallback;

public class CopyFileThread implements Runnable {

	private String selectedPath;
	private String destinationPath;
	private ServiceCallback serviceCallback;

	public CopyFileThread(ServiceCallback serviceCallback) {
		this.serviceCallback = serviceCallback;
	}

	public String getSelectedPath() {
		return selectedPath;
	}

	public void setSelectedPath(String selectedPath) {
		this.selectedPath = selectedPath;
	}

	public String getDestinationPath() {
		return destinationPath;
	}

	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			serviceCallback.serviceStarted("started");
			copyFile(selectedPath, destinationPath);
			serviceCallback.serviceEnd("copysuccess," + destinationPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void copyFile(String selectedPath, String destinationPath)
			throws IOException {

		File f = new File(destinationPath);
		InputStream in = new FileInputStream(selectedPath);
		OutputStream out = new FileOutputStream(f);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}

		in.close();
		out.close();
		f.setLastModified(System.currentTimeMillis());
	}

}
