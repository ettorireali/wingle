package com.digitize.wingleapp.pojo;

import com.digitize.wingleapp.GlobalApp;
import com.digitize.wingleapp.places.Place;
import com.google.android.gms.maps.model.LatLng;

public class PlaceBookVO {
	private long localPlaceId = 0L;
	private long personId = 0L;
	private LatLng pos = null;
	private String placeInfo = null;
	private String googleSearchId = null;
	private long notedTime = 0L;
	private long livePlaceId = 0L;
	public static final int DEFAULT_PLACE_ID = -1;

	public long isCandidateForRemoval() {
		if (notedTime >= (System.currentTimeMillis() - (GlobalApp.HOUR * 12)))
			return livePlaceId;
		else
			return DEFAULT_PLACE_ID;
	}

	public PlaceBookVO() {
		placeInfo = "N/A";
	}

	public PlaceBookVO(Place placeInstance) {
		placeInfo = placeInstance.name;
		googleSearchId = placeInstance.id;
		pos = new LatLng(placeInstance.geometry.location.lat,
				placeInstance.geometry.location.lng);
		notedTime = System.currentTimeMillis();
	}

	public long getLocalPlaceId() {
		return localPlaceId;
	}

	public void setLocalPlaceId(long localPlaceId) {
		this.localPlaceId = localPlaceId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public LatLng getPos() {
		return pos;
	}

	public void setPos(LatLng pos) {
		this.pos = pos;
	}

	public String getPlaceInfo() {
		return placeInfo;
	}

	public void setPlaceInfo(String placeInfo) {
		this.placeInfo = placeInfo;
	}

	public String getGoogleSearchId() {
		return googleSearchId;
	}

	public void setGoogleSearchId(String googleSearchId) {
		this.googleSearchId = googleSearchId;
	}

	public long getNotedTime() {
		return notedTime;
	}

	public void setNotedTime(long notedTime) {
		this.notedTime = notedTime;
	}

	public long getLivePlaceId() {
		return livePlaceId;
	}

	public void setLivePlaceId(long livePlaceId) {
		this.livePlaceId = livePlaceId;
	}
}
