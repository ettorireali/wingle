package com.digitize.wingleapp;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.common.base.Preconditions;

import android.graphics.Bitmap;
import at.vcity.androidim.types.MessageInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class ImageMemoryCache.
 */
public class ChatCache {

    /** The cache. */
    private static HashMap<String, ArrayList<Object>> cache = new HashMap<String,  ArrayList<Object>>();
    private static HashMap<String, Integer> unreadCount = new HashMap<String,  Integer>();

    /**
     * Gets the image.
     * 
     * @param key
     *            the key
     * @return the image
     */
    public static ArrayList<Object> getChatHistoryForUsername(String key) {
        if (cache.containsKey(key)) {
            return cache.get(key);
        }
        return null;
    }

    /**
     * Sets the image.
     * 
     * @param key
     *            the key
     * @param image
     *            the image
     */
    public static void putChatHistoryForUsername(String key, ArrayList<Object> msgs) {
        cache.put(key, msgs);
    }
    
    public static Integer getUnReadMessaageForUsr(String userName) {
		return (Integer) GlobalApp.getDefultIfNull(unreadCount.get(userName), new Integer(0));
		// TODO Auto-generated method stub

	}
    
    public static void incrementUnreadMessages(String userName){
    	Integer cnt =  (Integer) GlobalApp.getDefultIfNull(unreadCount.get(userName), new Integer(0));
		cnt++;
		unreadCount.put(userName, cnt);
    	
    }
    
}