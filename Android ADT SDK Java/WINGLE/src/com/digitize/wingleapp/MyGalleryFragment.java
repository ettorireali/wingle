package com.digitize.wingleapp;

import java.util.ArrayList;

import com.digitize.wingleapp.imagezoom.zoomexample;
import com.digitize.wingleapp.pojo.GalleryData;
import com.digitize.wingleapp.services.GetGalleryService;
import com.digitize.wingleapp.services.ServiceCallback;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.Toast;

public class MyGalleryFragment extends Fragment implements ServiceCallback {
	GridView galleryGrid;
	ImageAdapter imgAdapter;
	GetGalleryService getGalleryService;
	ProgressDialog progressDialog;
	ArrayList<GalleryData> galleryListData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.gallery_fragment, null);
		galleryGrid = (GridView) view.findViewById(R.id.gallery_grid);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		progressDialog.setMessage("Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		imgAdapter = new ImageAdapter(getActivity());
		getGalleryService = new GetGalleryService(this, getActivity());
		getGalleryService.setUidForGallery(GlobalApp
				.getUidFromSharedPreference(getActivity()));
		if (GlobalApp.checkInternetConnection(getActivity())) {
			new Thread(getGalleryService).start();
		} else {
			Toast.makeText(getActivity(),
					"Check internet connection and Try again.",
					Toast.LENGTH_SHORT).show();
		}

		galleryGrid.setOnItemClickListener(galleryItemClickListener);
	}

	public OnItemClickListener galleryItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view,
				int position, long arg3) {
			// TODO Auto-generated method stub
			Intent inte = new Intent(getActivity(), zoomexample.class);
			inte.putExtra("currentposition", position);
			inte.putExtra("cached_url", galleryListData.get(position)
					.getImagePath());
			// inte.putExtra("imagename", imagePathList);
			startActivity(inte);
		}
	};

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("gallery_started")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("gallery_success")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					galleryListData = getGalleryService.getGalleryList();
					imgAdapter.setGalleryList(galleryListData);
					galleryGrid.setAdapter(imgAdapter);
					imgAdapter.notifyDataSetChanged();
				}
			});
		} else if (msg.equalsIgnoreCase("gallery_error")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Some problem found",
							Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}
}
