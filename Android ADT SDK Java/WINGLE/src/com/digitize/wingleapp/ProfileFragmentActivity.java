package com.digitize.wingleapp;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.json.JSONArray;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.imagepart.RoundedImageView;
import com.digitize.wingleapp.pojo.CopyFileThread;
import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.GetProfileService;
import com.digitize.wingleapp.services.ProfilePictureUploadService;
import com.digitize.wingleapp.services.ServiceCallback;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

public class ProfileFragmentActivity extends FragmentActivity implements
		TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener,
		ServiceCallback {

	private TabHost mTabHost;
	private ViewPager mViewPager;
	AboutMeFragment aboutFragment;
	MyGalleryFragment mygalleryFragment;
	BarsToGoFragment barsFragment;
	MyPlacesFragment myPlacesFragment;
	TextView txtUserName, txtEmail;
	String storedImagePath;
	String imageDirectoryFromPath;
	ArrayList<ProfileData> profileArrayList;
	boolean result = false;
	private static final int IMAGE_CAPTURE = 11;
	private static final int IMAGE_FROM_MEDIA = 12;
	CopyFileThread copyFile;
	ProgressDialog progressDialog;
	RoundedImageView profileImage;
	ProfilePictureUploadService pictureUploadService;
	private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, ProfileFragmentActivity.TabInfo>();
	private PagerAdapter mPagerAdapter;
	String galleryImageUri = null;
	WingleDB wingleDB;

	AQuery aQuery = null;
	public int stub_id = R.drawable.noimage;
	private int screenHeight = 0, screenWidth = 0;

	public void getScreenParams() {
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
	}

	private class TabInfo {
		private String tag;
		private Class<?> clss;
		private Bundle args;
		private Fragment fragment;

		TabInfo(String tag, Class<?> clazz, Bundle args) {
			this.tag = tag;
			this.clss = clazz;
			this.args = args;
		}

	}

	class TabFactory implements TabContentFactory {
		private final Context mContext;

		public TabFactory(Context context) {
			mContext = context;
		}

		/*
		 * public View createTabContent(String tag) { View v = new
		 * View(mContext); v.setMinimumWidth(0); v.setMinimumHeight(0); return
		 * v; }
		 */
		public View createTabContent(String tag) {
			View v = LayoutInflater.from(mContext).inflate(R.layout.tabview_bg,
					null);
			TextView txtView = (TextView) v.findViewById(R.id.tabsText);
			txtView.setText(tag);
			return v;
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabs_viewpager_layout);
		wingleDB = new WingleDB(this);
		aQuery = new AQuery(this);
		copyFile = new CopyFileThread(this);
		pictureUploadService = new ProfilePictureUploadService(this, this);
		profileImage = (RoundedImageView) findViewById(R.id.profile_imageView);
		txtUserName = (TextView) findViewById(R.id.txt_profile_user_name);
		txtEmail = (TextView) findViewById(R.id.txt_profile_email);
		/*
		 * if(!GlobalApp.getProfilePicUrlFromSharedPreference(this).equalsIgnoreCase
		 * ("unavailable")){
		 * galleryImageUri=GlobalApp.getProfilePicUrlFromSharedPreference
		 * (ProfileFragmentActivity.this); displayImage(galleryImageUri,
		 * ProfileFragmentActivity.this); }else{
		 * profileImage.setImageResource(R.drawable.company_3_img); }
		 */
		
	//	Intent i= getIntent();
	//	Bundle b= i.getExtras();
	//	int pid= b.getInt("pid");
	//	profileImage.setId(pid);
		
		
		
		
		wingleDB.open();
		profileArrayList = wingleDB.getUserData(GlobalApp
				.getUidFromSharedPreference(this));
		wingleDB.close();
		Iterator<ProfileData> profileIterator = profileArrayList.iterator();
		ProfileData profileData;
		while (profileIterator.hasNext()) {
			profileData = profileIterator.next();
			txtUserName.setText(profileData.getFirstName() + " "
					+ profileData.getLastName());
			txtEmail.setText(profileData.getEmail());
			Log.w("image_path",
					"profile image path is : " + profileData.getImagePath());
			displayImage(profileData.getImagePath());
		}
		progressDialog = new ProgressDialog(this, R.style.MyTheme);
		progressDialog.setMessage("Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);

		imageDirectoryFromPath = GlobalApp.createImageFolder(GlobalApp
				.getUidFromSharedPreference(this));
		this.initialiseTabHost(savedInstanceState);
		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); // set
																				// the
																				// tab
																				// as
																				// per
																				// the
																				// saved
																				// state
		}
		this.intialiseViewPager();
		getScreenParams();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// new Thread(getProfileService).start();
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("WINGLE");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.photo_option, menu);
		restoreActionBar();

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if (id == R.id.action_change_photo) {
			final CharSequence[] items = { "Capture Image", "Get From Gallery" };

			AlertDialog.Builder builder = new AlertDialog.Builder(
					ProfileFragmentActivity.this);
			FrameLayout frameView = new FrameLayout(
					ProfileFragmentActivity.this);
			builder.setView(frameView);
			builder.setTitle("Select Image Source");
			builder.setItems(items, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int pos) {
					// Do something with the selection
					if (pos == 0) {
						storedImagePath = imageDirectoryFromPath + "/"
								+ System.currentTimeMillis() + ".jpg";
						File f = new File(storedImagePath);
						Uri imageFile = Uri.fromFile(f);

						Intent intent = new Intent(
								android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFile);
						Log.w("from_intent", "imageFile : " + imageFile);
						startActivityForResult(intent, IMAGE_CAPTURE);
					} else {
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(
								Intent.createChooser(intent, "Select Image"),
								IMAGE_FROM_MEDIA);
					}
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == IMAGE_CAPTURE && resultCode == RESULT_OK) {
			result = true;
			pictureUploadService.setImagePath(storedImagePath);
			pictureUploadService.setUid(GlobalApp
					.getUidFromSharedPreference(ProfileFragmentActivity.this));
			new Thread(pictureUploadService).start();

		} else if (requestCode == IMAGE_FROM_MEDIA) {
			result = true;
			if (data != null) {
				try {
					storedImagePath = imageDirectoryFromPath + File.separator
							+ System.currentTimeMillis() + ".jpg";
					Uri selectedimageuri = data.getData();
					// profileImage.setImageURI(selectedimageuri);
					String selectedImagePath = getPath(selectedimageuri);
					pictureUploadService.setImagePath(selectedImagePath);
					pictureUploadService
							.setUid(GlobalApp
									.getUidFromSharedPreference(ProfileFragmentActivity.this));
					new Thread(pictureUploadService).start();
					Log.w("path", "path = " + selectedImagePath);
					copyFile.setSelectedPath(selectedImagePath);
					copyFile.setDestinationPath(storedImagePath);
					// new Thread(copyFile).start();
				} catch (Exception ex) {

				}
			}
		}
	}

	private void intialiseViewPager() {
		List<Fragment> fragments = new Vector<Fragment>();
		aboutFragment = (AboutMeFragment) Fragment.instantiate(this,
				AboutMeFragment.class.getName());
		fragments.add(aboutFragment);
		barsFragment = (BarsToGoFragment) Fragment.instantiate(this,
				BarsToGoFragment.class.getName());
		fragments.add(barsFragment);
		myPlacesFragment = (MyPlacesFragment) Fragment.instantiate(this,
				MyPlacesFragment.class.getName());
		fragments.add(myPlacesFragment);
		mygalleryFragment = (MyGalleryFragment) Fragment.instantiate(this,
				MyGalleryFragment.class.getName());
		fragments.add(mygalleryFragment);

		this.mPagerAdapter = new PagerAdapter(super.getSupportFragmentManager());
		mPagerAdapter.setFragments(fragments);
		this.mViewPager = (ViewPager) super.findViewById(R.id.viewpager);
		this.mViewPager.setOffscreenPageLimit(2);
		this.mViewPager.setAdapter(this.mPagerAdapter);
		this.mViewPager.setOnPageChangeListener(this);
	}

	private void initialiseTabHost(Bundle args) {
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();
		TabInfo tabInfo = null;
		TabSpec tabSpec;

		View tabview = createTabView(this, "About Me");
		tabSpec = this.mTabHost.newTabSpec("About Me").setIndicator(tabview)
				.setContent(this.new TabFactory(this));
		mTabHost.addTab(tabSpec);
		tabInfo = new TabInfo("About Me", AboutMeFragment.class, args);
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		tabview = createTabView(this, "Favorite Bars");
		tabSpec = this.mTabHost.newTabSpec("Favorite Bars")
				.setIndicator(tabview).setContent(this.new TabFactory(this));
		mTabHost.addTab(tabSpec);
		tabInfo = new TabInfo("Favorite Bars", BarsToGoFragment.class, args);
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		tabview = createTabView(this, "My Places");
		tabSpec = this.mTabHost.newTabSpec("My Places").setIndicator(tabview)
				.setContent(this.new TabFactory(this));
		mTabHost.addTab(tabSpec);
		tabInfo = new TabInfo("My Places", MyPlacesFragment.class, args);
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		tabview = createTabView(this, "My Gallery");
		tabSpec = this.mTabHost.newTabSpec("My Gallery").setIndicator(tabview)
				.setContent(this.new TabFactory(this));
		mTabHost.addTab(tabSpec);
		tabInfo = new TabInfo("My Gallery", MyGalleryFragment.class, args);
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		mTabHost.setOnTabChangedListener(this);
	}

	private static void AddTab(ProfileFragmentActivity activity,
			TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
		// Attach a Tab view factory to the spec
		tabSpec.setContent(activity.new TabFactory(activity));
		tabHost.addTab(tabSpec);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int position) {
		// TODO Auto-generated method stub
		this.mTabHost.setCurrentTab(position);
	}

	@Override
	public void onTabChanged(String arg0) {
		// TODO Auto-generated method stub
		int pos = this.mTabHost.getCurrentTab();
		this.mViewPager.setCurrentItem(pos);
	}

	private static View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(R.layout.tabview_bg,
				null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);

		return view;
	}

	@SuppressWarnings("deprecation")
	public String getPath(Uri uri) {
		Cursor cursor = null;
		int column_index = 0;
		if (result) {
			String[] projection = { MediaStore.Images.Media.DATA };
			cursor = managedQuery(uri, projection, null, null, null);
			// cursor=CursorLoader(getActivity(),uri,projection,null,null,null);
			column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
			cursor.moveToFirst();
			result = false;
		}
		return cursor.getString(column_index);
	}

	@Override
	public void serviceStarted(final String msg) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (msg.equalsIgnoreCase("started")) {
					progressDialog.show();
				} else if (msg.equalsIgnoreCase("upload_start")) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							progressDialog.show();
						}
					});

				} else if (msg.equalsIgnoreCase("profile_started")) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							progressDialog.show();
						}
					});
				}
			}
		});
	}

	@Override
	public void serviceEnd(final String msg) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (msg.startsWith("copysuccess")) {
					String[] fileNames = msg.split(",");
					// addNewImages(fileNames[1]);
					progressDialog.dismiss();
					/*
					 * pictureUploadService.setImagePath(storedImagePath);
					 * pictureUploadService
					 * .setUid(GlobalApp.getUidFromSharedPreference
					 * (ProfileFragmentActivity.this)); new
					 * Thread(pictureUploadService).start();
					 */
				} else if (msg.equalsIgnoreCase("error")) {
					progressDialog.dismiss();
				} else if (msg.equalsIgnoreCase("missing_param")) {
					progressDialog.dismiss();
					Toast.makeText(ProfileFragmentActivity.this,
							"Some params are missing", Toast.LENGTH_SHORT)
							.show();
				} else if (msg.equalsIgnoreCase("upload_problem")) {
					progressDialog.dismiss();
					Toast.makeText(ProfileFragmentActivity.this,
							"Problem uploading file.Try again.",
							Toast.LENGTH_SHORT).show();
				} else if (msg.equalsIgnoreCase("upload_success")) {
					progressDialog.dismiss();
					String profilePic = null;
					wingleDB.open();
					profilePic = wingleDB
							.getProfilePic(GlobalApp
									.getUidFromSharedPreference(ProfileFragmentActivity.this));
					wingleDB.close();
					displayImage(profilePic);
					Toast.makeText(ProfileFragmentActivity.this,
							"Image Uploaded", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}

	public void displayImage(String url) {
		aQuery.id(profileImage)
				.progress(R.id.icon_progress)
				.image(url, true, true, (int) (200), R.drawable.noimage,
						new BitmapAjaxCallback() {
							@Override
							protected void callback(String url, ImageView iv,
									Bitmap bm, AjaxStatus status) {
								// TODO Auto-generated method stub
								super.callback(url, iv, bm, status);
								Log.w("fired",
										url + " status : " + status.getCode());
							}
						});
	}
}
