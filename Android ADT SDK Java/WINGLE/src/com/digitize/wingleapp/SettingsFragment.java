package com.digitize.wingleapp;

import com.digitize.wingleapp.pojo.CustomAutoComplete;
import com.digitize.wingleapp.pojo.PreferenceSet;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.UpdateProfileService;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class SettingsFragment extends Fragment implements ServiceCallback {

	CustomAutoComplete placesAutoCompEdt;
	SeekBar proximitySeekbar;
	TextView txtPercent;
	CheckBox chkNotifyMe;
	Button btnReset, btnSave;
	Typeface typefaceSmall;
	TextView txtSettingEmail, txtSettingDob, txtEmailAttr, txtDobAttr;
	PreferenceSet prefSet;
	UpdateProfileService updateProfile = null;
	TextView txtPrivacySetting, txtMapSetting;
	LinearLayout linearMapSetting, linearPrivacySetting;
	boolean mapClicked = false;
	boolean privacyClicked = false;
	ProgressDialog progressDialog = null;
	Drawable arrowRight, arrowDown, mapDrawable, privacyDrawable;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.settings_fragment, null);
		placesAutoCompEdt = (CustomAutoComplete) view
				.findViewById(R.id.places_automcomp_edittext);
		proximitySeekbar = (SeekBar) view.findViewById(R.id.proximity_seekbar);
		txtPercent = (TextView) view.findViewById(R.id.txt_percent);
		chkNotifyMe = (CheckBox) view.findViewById(R.id.chk_notify_me);
		btnReset = (Button) view.findViewById(R.id.btn_reset);
		btnSave = (Button) view.findViewById(R.id.btn_setting_save);
		txtPrivacySetting = (TextView) view
				.findViewById(R.id.txt_privacy_setting);
		txtMapSetting = (TextView) view.findViewById(R.id.txt_map_setting);
		txtSettingEmail = (TextView) view.findViewById(R.id.txt_setting_email);
		txtSettingDob = (TextView) view.findViewById(R.id.txt_setting_dob);
		txtEmailAttr = (TextView) view
				.findViewById(R.id.txt_setting_email_attribute);
		txtDobAttr = (TextView) view
				.findViewById(R.id.txt_setting_dob_attribute);
		linearMapSetting = (LinearLayout) view
				.findViewById(R.id.linear_map_setting);
		linearPrivacySetting = (LinearLayout) view
				.findViewById(R.id.linear_privacy_setting);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		linearMapSetting.setVisibility(View.GONE);
		linearPrivacySetting.setVisibility(View.GONE);
		progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		progressDialog.setMessage("Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		updateProfile = new UpdateProfileService(this, getActivity());
		updateProfile.setUid(GlobalApp
				.getUidFromSharedPreference(getActivity()));
		updateProfile.setUpdateOption(UpdateProfileService.UPDATE_PRIVACY);
		arrowRight = getResources().getDrawable(R.drawable.ic_arrow_right);
		arrowDown = getResources().getDrawable(R.drawable.ic_arrow_down);
		mapDrawable = getResources().getDrawable(R.drawable.ic_map_setting);
		privacyDrawable = getResources().getDrawable(R.drawable.ic_privacy);
		String[] places = getResources().getStringArray(R.array.places);
		typefaceSmall = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/AliquamREG.ttf");
		placesAutoCompEdt.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line, places));
		placesAutoCompEdt
				.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
		proximitySeekbar.setOnSeekBarChangeListener(proximitySeekbarListener);
		proximitySeekbar.setProgress(GlobalApp
				.getDefaultProximity(getActivity()));
		btnReset.setTypeface(typefaceSmall);
		btnSave.setTypeface(typefaceSmall);
		btnReset.setOnClickListener(resetClickListener);
		btnSave.setOnClickListener(saveClickListener);
		txtMapSetting.setOnClickListener(mapSettingListener);
		txtPrivacySetting.setOnClickListener(privacySettingListener);
		txtSettingEmail.setOnClickListener(emailClickListener);
		txtSettingDob.setOnClickListener(dobClickListener);
		prefSet = new PreferenceSet();
		String privacy = GlobalApp
				.getPrivacyFromSharedPreference(getActivity());
		prefSet.setPrivacy(privacy);
		txtEmailAttr.setText(prefSet.getPrivacyStatus("email"));
		txtDobAttr.setText(prefSet.getPrivacyStatus("dob"));
	}

	public OnClickListener mapSettingListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (!mapClicked) {
				expand(linearMapSetting);
				txtMapSetting.setCompoundDrawablesWithIntrinsicBounds(
						mapDrawable, null, arrowDown, null);
				mapClicked = true;
			} else {
				collapse(linearMapSetting);
				txtMapSetting.setCompoundDrawablesWithIntrinsicBounds(
						mapDrawable, null, arrowRight, null);
				mapClicked = false;
			}
		}
	};
	public OnClickListener privacySettingListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (!privacyClicked) {
				expand(linearPrivacySetting);
				txtPrivacySetting.setCompoundDrawablesWithIntrinsicBounds(
						privacyDrawable, null, arrowDown, null);
				privacyClicked = true;
			} else {
				collapse(linearPrivacySetting);
				txtPrivacySetting.setCompoundDrawablesWithIntrinsicBounds(
						privacyDrawable, null, arrowRight, null);
				privacyClicked = false;
			}
		}
	};
	public OnClickListener emailClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			showAttributeDialog(true);

		}
	};
	public OnClickListener dobClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			showAttributeDialog(false);
		}
	};
	public OnClickListener resetClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

		}
	};
	public OnClickListener saveClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			GlobalApp.setDefaultProximity(getActivity(),
					proximitySeekbar.getProgress());
			if (GlobalApp.checkInternetConnection(getActivity())) {
				String privacy = prefSet.getPrivacy().toString();
				Log.w("privacy", "privacy is : " + privacy);
				GlobalApp.storePrivacyToPref(getActivity(), privacy);
				updateProfile.setPrivacyPref(privacy);
				new Thread(updateProfile).start();
			}
		}
	};
	public OnSeekBarChangeListener proximitySeekbarListener = new OnSeekBarChangeListener() {

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			int value = seekBar.getProgress();
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// TODO Auto-generated method stub
			txtPercent.setText(progress + " km");
		}
	};

	public static void expand(final View v) {
		v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		final int targtetHeight = v.getMeasuredHeight();

		v.getLayoutParams().height = 0;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT
						: (int) (targtetHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (targtetHeight / v.getContext().getResources()
				.getDisplayMetrics().density) * 2);
		v.startAnimation(a);
	}

	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				if (interpolatedTime == 1) {
					v.setVisibility(View.GONE);
				} else {
					v.getLayoutParams().height = initialHeight
							- (int) (initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (initialHeight / v.getContext().getResources()
				.getDisplayMetrics().density) * 2);
		v.startAnimation(a);
	}

	public void showAttributeDialog(final boolean isEmail) {
		final CharSequence[] items = { "Default", "Public", "Private",
				"Followers Only" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select Attribute");
		builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				String attrResponse = prefSet.getPrivacy();
				Log.w("preferences", "Preferences are : " + attrResponse);
			}
		});

		builder.setSingleChoiceItems(items, -1,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						if ("Default".equals(items[which])) {
							if (isEmail) {
								txtEmailAttr.setText("Default");
								prefSet.setFieldVisibility("email",
										PreferenceSet.DEFAULT);
							} else {
								txtDobAttr.setText("Default");
								prefSet.setFieldVisibility("dob",
										PreferenceSet.DEFAULT);
							}
						} else if ("Public".equals(items[which])) {
							if (isEmail) {
								txtEmailAttr.setText("Public");
								prefSet.setFieldVisibility("email",
										PreferenceSet.PUBLIC);
							} else {
								txtDobAttr.setText("Public");
								prefSet.setFieldVisibility("dob",
										PreferenceSet.PUBLIC);
							}
						} else if ("Private".equals(items[which])) {
							if (isEmail) {
								txtEmailAttr.setText("Private");
								prefSet.setFieldVisibility("email",
										PreferenceSet.PRIVATE);
							} else {
								txtDobAttr.setText("Private");
								prefSet.setFieldVisibility("dob",
										PreferenceSet.PRIVATE);
							}
						} else if ("Followers Only".equals(items[which])) {
							if (isEmail) {
								txtEmailAttr.setText("Followers Only");
								prefSet.setFieldVisibility("email",
										PreferenceSet.FOLLOWERS_ONLY);
							} else {
								txtDobAttr.setText("Followers Only");
								prefSet.setFieldVisibility("dob",
										PreferenceSet.FOLLOWERS_ONLY);
							}
						}

					}
				});
		builder.show();

	}

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("updateprofile_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("update_success")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
				}
			});
		} else if (msg.equalsIgnoreCase("update_problem")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(),
							"Problem saving data.Try again", Toast.LENGTH_SHORT)
							.show();
				}
			});
		} else if (msg.equalsIgnoreCase("params_missing")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(),
							"Some params missing.Try again", Toast.LENGTH_SHORT)
							.show();
				}
			});
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}

}
