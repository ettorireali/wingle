package com.digitize.wingleapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;

public class Feeds_list_activity extends Activity {

	ImageView Goto_map, edit_profile_bar, setting_icon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setFullscreen();
		setContentView(R.layout.news_feeds_list);
		// setting_icon=(ImageView)findViewById(R.id.setting_icon);

		setting_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		// Goto_map=(ImageView)findViewById(R.id.goto_map);
		// edit_profile_bar=(ImageView)findViewById(R.id.edit_profile);
		edit_profile_bar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		Goto_map.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		// ListView lv = (ListView)findViewById(R.id.lv);
		MainNewsAdapter adapter = new MainNewsAdapter(this);
		// lv.setAdapter(adapter);
	}

	public void setFullscreen() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
}
