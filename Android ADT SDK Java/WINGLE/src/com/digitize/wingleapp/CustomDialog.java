package com.digitize.wingleapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;

public class CustomDialog extends Dialog {

	private Activity activity;
	private LayoutParams outerParams;
	private int screenHeight = 0, screenWidth = 0;
	Button btnOk;

	public void getScreenParams() {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
	}

	public void setCustomDialogOutsideListener(
			android.view.View.OnClickListener clickListener) {
		btnOk.setOnClickListener(clickListener);
	}

	public CustomDialog(Activity activity) {
		super(activity);
		this.activity = activity;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		setContentView(R.layout.custom_dialog);
		btnOk = (Button) findViewById(R.id.btn_dialog_ok);
		getScreenParams();
		outerParams = getWindow().getAttributes();
		outerParams.height = (int) (screenHeight * 0.50f);
		outerParams.width = (int) (screenWidth * 0.55f);
		getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) outerParams);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		super.show();
	}

}
