package com.digitize.wingleapp;

import java.util.ArrayList;
import java.util.zip.Inflater;

import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.pojo.PlaceBookVO;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class MyPlacesFragment extends Fragment {

	ListView placesList;
	MyPlaceAdapter myPlaceAdapter;
	ArrayList<PlaceBookVO> placesArrayList;
	WingleDB wingleDB;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.myplaces_fragment, null);
		placesList = (ListView) view.findViewById(R.id.places_list);
		placesList.setEmptyView(view.findViewById(R.id.empty));
		return view;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		wingleDB = new WingleDB(getActivity());
		myPlaceAdapter = new MyPlaceAdapter(getActivity());
		wingleDB.open();
		placesArrayList = wingleDB.getPlaceHistory(GlobalApp
				.getUidFromSharedPreference(getActivity()));
		wingleDB.close();
		Log.w("places_list",
				"PlaceList from fragment : " + placesArrayList.size());
		myPlaceAdapter.setPlaceData(placesArrayList);
		placesList.setAdapter(myPlaceAdapter);
		myPlaceAdapter.notifyDataSetChanged();
	}

}
