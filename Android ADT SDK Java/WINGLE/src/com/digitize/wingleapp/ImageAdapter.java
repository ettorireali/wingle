package com.digitize.wingleapp;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.digitize.wingleapp.pojo.GalleryData;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {

	Context context;
	LayoutInflater layoutInflater;
	ArrayList<GalleryData> galleryArrList;

	AQuery aQuery = null;

	public void setGalleryList(ArrayList<GalleryData> galleryList) {
		this.galleryArrList = galleryList;
	}

	public ImageAdapter(Context c) {
		// TODO Auto-generated constructor stub
		this.context = c;
		this.layoutInflater = LayoutInflater.from(c);
		aQuery = new AQuery(c);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return galleryArrList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		int actualPosition = position % galleryArrList.size();
		return galleryArrList.get(actualPosition);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		CellItem cellItem = null;
		int itemPos = (position % galleryArrList.size());
		GalleryData galleryData = galleryArrList.get(itemPos);
		try {
			if (convertView == null) {
				cellItem = new CellItem();
				convertView = layoutInflater.inflate(R.layout.gallery_item,
						null);
				cellItem.cellImageView = (ImageView) convertView
						.findViewById(R.id.gallery_imageview);
				convertView.setTag(cellItem);
			} else {
				cellItem = (CellItem) convertView.getTag();
			}

			aQuery.id(cellItem.cellImageView)
					.progress(R.id.gallery_progress)
					.image(galleryData.getImagePath(), true, true, (int) (200),
							R.drawable.grey_overall_background,
							new BitmapAjaxCallback() {
								@Override
								protected void callback(String url,
										ImageView iv, Bitmap bm,
										AjaxStatus status) {
									// TODO Auto-generated method stub
									super.callback(url, iv, bm, status);
									Log.w("fired",
											url + " status : "
													+ status.getCode());
								}
							});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	class CellItem {
		ImageView cellImageView;
	}
}
