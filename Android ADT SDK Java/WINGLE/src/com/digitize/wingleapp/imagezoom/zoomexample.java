package com.digitize.wingleapp.imagezoom;

import java.io.File;
import java.util.ArrayList;

import com.androidquery.AQuery;
import com.digitize.wingleapp.R;
import com.digitize.wingleapp.imagezoom.SimpleZoomListener.ControlType;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.Toast;

/**
 * Activity for zoom tutorial 1
 */
public class zoomexample extends Activity implements OnTouchListener {

	/** Constant used as menu item id for setting zoom control type */
	private static final int MENU_ID_ZOOM = 0;

	/** Constant used as menu item id for setting pan control type */
	private static final int MENU_ID_PAN = 1;

	/** Constant used as menu item id for resetting zoom state */
	private static final int MENU_ID_RESET = 2;

	/** Image zoom view */
	private ImageZoomView mZoomView;

	/** Zoom state */
	private ZoomState mZoomState;

	/** Decoded bitmap image */
	private Bitmap mBitmap;

	View prev, next;
	private ArrayList<String> propertyImageGalleryURLs = null;
	public static Activity activity;
	int totalimages = 0;
	int position = 0;
	AQuery aQuery = null;
	String cachedUrl = null;
	/** On touch listener for zoom view */
	private SimpleZoomListener mZoomListener;

	Handler handler = new Handler();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.imagetransition);
		mZoomState = new ZoomState();
		activity = this;
		Intent inte = getIntent();
		// ArrayList<String> temp=inte.getStringArrayListExtra("imagename");
		// propertyImageGalleryURLs=temp;
		// mBitmap = BitmapFactory.decodeResource(getResources(),
		// R.drawable.home2);
		mZoomListener = new SimpleZoomListener(showHide);
		mZoomListener.setZoomState(mZoomState);
		mZoomView = (ImageZoomView) findViewById(R.id.zoomview);
		mZoomView.setZoomState(mZoomState);
		position = getIntent().getExtras().getInt("currentposition");
		cachedUrl = getIntent().getExtras().getString("cached_url");

		mZoomView.setOnTouchListener(mZoomListener);
		prev = findViewById(R.id.prevImageBtn);
		next = findViewById(R.id.nextImageBtn);
		/*
		 * next.setOnClickListener(new View.OnClickListener() { public void
		 * onClick(View view) { if(position!=totalimages-1) { position++; try{
		 * mZoomView.setImage(BitmapFactory.decodeResource(getResources(),
		 * images[position])); }catch(OutOfMemoryError o){ o.printStackTrace();
		 * } } } } );
		 */
		/*
		 * prev.setOnClickListener(new View.OnClickListener() { public void
		 * onClick(View view) { if(position!=0) { position--; try{
		 * mZoomView.setImage(BitmapFactory.decodeResource(getResources(),
		 * images[position])); }catch(OutOfMemoryError m){ m.printStackTrace();
		 * } } } });
		 */
		resetZoomState();
		scheduleHideButtons();
		aQuery = new AQuery(this);
		mZoomView.setImage(BitmapFactory.decodeResource(getResources(),
				R.drawable.noimage));
		try {
			mZoomView.setImage(aQuery.getCachedImage(cachedUrl));
		} catch (OutOfMemoryError out) {
			out.printStackTrace();
		}
	}

	private void scheduleHideButtons() {
		handler.removeCallbacks(hideButtonsRunnable);
		handler.postDelayed(hideButtonsRunnable, 2000);
	}

	private Runnable hideButtonsRunnable = new Runnable() {

		public void run() {
			fadeButtons(false);
		}
	};

	private void fadeButtons(final boolean fadeIn) {
		if (fadeIn) {
			scheduleHideButtons();
		}
		Animation anim = AnimationUtils.loadAnimation(this,
				fadeIn ? android.R.anim.fade_in : android.R.anim.fade_out);
		prev.startAnimation(anim);
		next.startAnimation(anim);

		anim.setAnimationListener(new AnimationListener() {

			public void onAnimationEnd(Animation animation) {
				prev.setVisibility(fadeIn ? View.GONE : View.GONE);
				next.setVisibility(fadeIn ? View.GONE : View.GONE);

			}

			public void onAnimationRepeat(Animation animation) {
			}

			public void onAnimationStart(Animation animation) {
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		mZoomView.setOnTouchListener(null);
		mZoomState.deleteObservers();
	}

	Handler showHide = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				showHideButtons();
				break;
			}
		}
	};

	void showHideButtons() {
		if (prev.getVisibility() == View.GONE) {
			fadeButtons(true);
		} else {
			scheduleHideButtons();
		}
	}

	protected void onPause() {
		mZoomView.setImage(null);
		super.onPause();
	}

	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (prev.getVisibility() == View.GONE) {
				fadeButtons(true);
			} else {
				scheduleHideButtons();
			}
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(Menu.NONE, MENU_ID_ZOOM, 0, "Zoom");
		menu.add(Menu.NONE, MENU_ID_PAN, 1, "Move");
		menu.add(Menu.NONE, MENU_ID_RESET, 2, "Reset");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_ID_ZOOM:
			mZoomListener.setControlType(ControlType.ZOOM);
			break;

		case MENU_ID_PAN:
			mZoomListener.setControlType(ControlType.PAN);
			break;

		case MENU_ID_RESET:
			resetZoomState();
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Reset zoom state and notify observers
	 */
	private void resetZoomState() {
		mZoomState.setPanX(0.5f);
		mZoomState.setPanY(0.5f);
		mZoomState.setZoom(1f);
		mZoomState.notifyObservers();
	}

}
