package com.digitize.wingleapp.places;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.digitize.wingleapp.pojo.PlaceBookVO;
import com.digitize.wingleapp.pojo.ProximityData;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.maps.android.clustering.ClusterItem;

public class PlaceItem implements ClusterItem {

	Place placeToRender = null;
	LatLng latLng = null;
	int counter = 0;
	JSONArray uidCollection = null;
	private int placeId = 0;
	public static final int DEFAULT_PLACE_ID = 0;
	private long localPlaceId = -1;
	private long livePlaceId = -1;

	public JSONArray getUidCollection() {
		return uidCollection;
	}

	public void addMe(String uid) {
		uidCollection.put(uid);
	}

	public void removeMySelf(String uid) {
		JSONArray revisedArr = new JSONArray();
		for (int i = 0; i < uidCollection.length(); i++) {
			try {
				if (!uid.equalsIgnoreCase(uidCollection.getString(i))) {
					revisedArr.put(uidCollection.getString(i));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		uidCollection = null;
		uidCollection = revisedArr;
	}

	public void associatePlaceInstance(JSONArray arr) {
		if (arr != null && arr.length() > 0) {
			try {
				for (int i = 0; i < arr.length(); i++) {
					uidCollection.put(arr.get(i));
				}
				counter = uidCollection.length();
			} catch (JSONException jse) {

			}
		}
	}

	public boolean areYouHere(String uid) {
		boolean hasElement = false;
		for (int i = 0; i < uidCollection.length(); i++) {
			try {
				if (uid.equalsIgnoreCase(uidCollection.getString(i))) {
					hasElement = true;
					break;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return hasElement;
	}

	public long getLivePlaceId() {
		return livePlaceId;
	}

	public void setLivePlaceId(long livePlaceId) {
		this.livePlaceId = livePlaceId;
	}

	public void setLocalPlaceId(long localPlaceId) {
		this.localPlaceId = localPlaceId;
	}

	public long getLocalPlaceId() {
		return localPlaceId;
	}

	public int getPlaceId() {
		return placeId;
	}

	public void setPlaceId(int placeId) {
		this.placeId = placeId;
	}

	public PlaceItem(Place placeObj) {
		// TODO Auto-generated constructor stub
		this.placeToRender = placeObj;
		this.latLng = new LatLng(placeToRender.geometry.location.lat,
				placeToRender.geometry.location.lng);
		uidCollection = new JSONArray();
		localPlaceId = DEFAULT_PLACE_ID;
		livePlaceId = DEFAULT_PLACE_ID;
	}

	public void setCounter(int c) {
		this.counter = c;
	}

	public int getCounter() {
		return counter;
	}

	public void resetFields() {
		counter = 0;
		uidCollection = new JSONArray();
	}

	public void associateProx(ProximityData proxInstance) {
		boolean hasElement = false;
		/*
		 * for(int i=0; i<uidCollection.length(); i++){ try {
		 * if(proxInstance.getUid
		 * ().equalsIgnoreCase(uidCollection.getString(i))){ hasElement=true;
		 * break; } } catch (JSONException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } }
		 */
		if (!hasElement) {
			uidCollection.put(proxInstance.getUid());
			setPlaceId(proxInstance.getPlaceId());
			counter++;
		}
	}

	public void associateLocalPlaceData(PlaceBookVO placeBookInstance) {
		setLocalPlaceId(placeBookInstance.getLocalPlaceId());
		setLivePlaceId(placeBookInstance.getLivePlaceId());
	}

	public void increaseCounter() {
		counter++;
	}

	public void decreaseCounter() {
		counter--;
	}

	public Place getPlaceInstance() {
		return placeToRender;
	}

	@Override
	public LatLng getPosition() {
		// TODO Auto-generated method stub
		return latLng;
	}

}
