package com.digitize.wingleapp;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.util.AQUtility;
import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.imagepart.FileCache;
import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.UserService;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class LoginActivity extends ActionBarActivity implements ServiceCallback {

	Button btnLogin;
	Button btnRegister;
	TextView txtHeading, txtRegister;
	EditText edtLoginUsername, edtLoginPassword;
	EditText edtUserName, edtFirstName, edtLastName, edtEmail, edtPassword,
			edtDateOfBirth, edtConfirmPass;
	RadioGroup genderGroup;
	ViewSwitcher switcher;
	UserService userService;
	DatePickerDialog datePickerDialog;
	ProgressDialog progressDialog;
	boolean isChanged = false;
	String deviceId = null;
	int gender = -1;

	Calendar cal = Calendar.getInstance();
	int verCode = android.os.Build.VERSION.SDK_INT;
	WingleDB wingleDB;
	ProfileData userData;
	Typeface typefaceSmall;
	private ProfilePictureView profilepicture;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	public String getIpAddress() {
		WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
		return String.format("%d.%d.%d.%d", (ipAddress & 0xff),
				(ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
				(ipAddress >> 24 & 0xff));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
			"com.digitize.wingleapp",
			PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
			MessageDigest md = MessageDigest.getInstance("SHA");
			md.update(signature.toByteArray());
			Log.d("TAG_KEY_HASH", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
			} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			}
		
		profilepicture=(ProfilePictureView)findViewById(R.id.profilePicture);
		profilepicture.setVisibility(View.INVISIBLE);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("WINGLE");
		progressDialog.setMessage("Login Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);
		typefaceSmall = Typeface.createFromAsset(getAssets(),
				"fonts/AliquamREG.ttf");
		wingleDB = new WingleDB(this);
		txtHeading = (TextView) findViewById(R.id.txt_wingle_head);
		txtRegister = (TextView) findViewById(R.id.txt_wingle_register_head);
		txtHeading.setTypeface(typefaceSmall);
		txtRegister.setTypeface(typefaceSmall);
		edtLoginUsername = (EditText) findViewById(R.id.edt_login_username);
		edtLoginPassword = (EditText) findViewById(R.id.edt_login_password);

		genderGroup = (RadioGroup) findViewById(R.id.gender_group);
		edtFirstName = (EditText) findViewById(R.id.edt_first_name);
		edtLastName = (EditText) findViewById(R.id.edt_last_name);
		edtUserName = (EditText) findViewById(R.id.edt_user_name);
		edtEmail = (EditText) findViewById(R.id.edt_email);
		edtDateOfBirth = (EditText) findViewById(R.id.edt_dob);
		edtPassword = (EditText) findViewById(R.id.edt_password);
		edtConfirmPass = (EditText) findViewById(R.id.edt_confirm_password);
		genderGroup.setOnCheckedChangeListener(genderCheckedChangeListener);
		btnRegister = (Button) findViewById(R.id.btn_register);
		btnRegister.setOnClickListener(registerClickListener);
		edtDateOfBirth.setOnClickListener(dobClickListener);
		datePickerDialog = new DatePickerDialog(this, dobDatePickerListener,
				cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
				cal.get(Calendar.DAY_OF_MONTH));

		btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setTypeface(typefaceSmall);
		btnRegister.setTypeface(typefaceSmall);
		switcher = (ViewSwitcher) findViewById(R.id.switcher);
		btnLogin.setOnClickListener(loginClickListener);
		deviceId = GlobalApp.getDeviceId(this);
		userService = new UserService(LoginActivity.this, LoginActivity.this);
		String uid = GlobalApp.getUidFromSharedPreference(this);
		Log.w("uid", "uid : " + uid);
		if (uid.equals("unavailable")) {

		} else {
			startActivity(new Intent(LoginActivity.this, MainActivity.class));

			finish();
		}
		if (verCode >= 11) {
			try {
				datePickerDialog
						.getDatePicker()
						.setMaxDate(
								new GregorianCalendar().getTimeInMillis() - 1000 * 3600 * 24);
			} catch (Exception ex) {

			}
		}
		try {
			userService.setIp(getIpAddress());
			Log.w("myip", getIpAddress());
		} catch (Exception ex) {
			Log.w("myip", ex.toString());
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		AQUtility.setCacheDir(FileCache.getCacheTarget(this));
		AQUtility.setContext(getApplication());
		// set the max number of icons (image width <= 50) to be cached in
		// memory, default is 20
		BitmapAjaxCallback.setIconCacheLimit(50);
		// set the max number of images (image width > 50) to be cached in
		// memory, default is 20
		BitmapAjaxCallback.setCacheLimit(50);
	}

	public OnClickListener dobClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			datePickerDialog.show();
		}
	};
	public DatePickerDialog.OnDateSetListener dobDatePickerListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			Calendar max = Calendar.getInstance();
			Calendar calendar = Calendar.getInstance();
			calendar.set(year, monthOfYear, dayOfMonth);
			max.add(Calendar.DATE, 0);
			max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH),
					max.get(Calendar.DAY_OF_MONTH));
			String formattedDate = null;
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			formattedDate = formatter.format(calendar.getTime());
			edtDateOfBirth.setText(formattedDate);
		}
	};
	public OnCheckedChangeListener genderCheckedChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if (checkedId == R.id.radio_male) {
				gender = 1;
			} else if (checkedId == R.id.radio_female) {
				gender = 0;
			}
		}
	};
	public OnClickListener loginClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (passValidationForLogin()) {

				userService.setOption(UserService.OPTION_LOGIN);
				userService.setUserName(edtLoginUsername.getText().toString()
						.trim());
				userService.setPassword(edtLoginPassword.getText().toString()
						.trim());
				new Thread(userService).start();
			}
		}
	};
	public OnClickListener registerClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (GlobalApp.checkInternetConnection(LoginActivity.this)) {
				if (passValidationForRegister()) {
					if (validEmail(edtEmail.getText().toString().trim())) {
						if (edtPassword
								.getText()
								.toString()
								.trim()
								.equals(edtConfirmPass.getText().toString()
										.trim())) {
							userService.setOption(UserService.OPTION_REGISTER);
							userService.setFirstName(edtFirstName.getText()
									.toString().trim());
							userService.setLastName(edtLastName.getText()
									.toString().trim());
							userService.setUserName(edtUserName.getText()
									.toString().trim());
							userService.setDateOfBirth(edtDateOfBirth.getText()
									.toString().trim());
							userService.setEmail(edtEmail.getText().toString()
									.trim());
							userService.setPassword(edtPassword.getText()
									.toString().trim());
							userService.setGender(gender);
							userService.setDeviceId(deviceId);

							new Thread(userService).start();
						} else {
							GlobalApp.takeDefaultAction(edtConfirmPass,
									LoginActivity.this,
									"Confirm password does not match");
						}
					} else {
						GlobalApp.takeDefaultAction(edtEmail,
								LoginActivity.this, "Invalid Email");
					}
				}
			} else {
				Toast.makeText(LoginActivity.this,
						"Internet connection not available", Toast.LENGTH_LONG)
						.show();
			}
		}
	};

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("WINGLE");
	}

	public boolean passValidationForLogin() {
		if (GlobalApp.isBlank(edtLoginUsername)) {
			GlobalApp.takeDefaultAction(edtLoginUsername, LoginActivity.this,
					"Username cannot be blank");
			return false;
		} else if (GlobalApp.isBlank(edtLoginPassword)) {
			GlobalApp.takeDefaultAction(edtLoginPassword, LoginActivity.this,
					"Password cannot be blank");
			return false;
		}
		return true;
	}

	public boolean passValidationForRegister() {
		if (GlobalApp.isBlank(edtFirstName)) {
			GlobalApp.takeDefaultAction(edtFirstName, LoginActivity.this,
					"Firstname cannot be blank");
			return false;
		} else if (GlobalApp.isBlank(edtLastName)) {
			GlobalApp.takeDefaultAction(edtLastName, LoginActivity.this,
					"Lastname cannot be blank");
			return false;
		} else if (GlobalApp.isBlank(edtUserName)) {
			GlobalApp.takeDefaultAction(edtUserName, LoginActivity.this,
					"Username cannot be blank");
			return false;
		} else if (GlobalApp.isBlank(edtEmail)) {
			GlobalApp.takeDefaultAction(edtEmail, LoginActivity.this,
					"Email cannot be blank");
			return false;
		} else if (GlobalApp.isBlank(edtDateOfBirth)) {
			GlobalApp.takeDefaultAction(edtDateOfBirth, LoginActivity.this,
					"Date of birth cannot be blank");
			return false;
		} else if (GlobalApp.isBlank(edtPassword)) {
			GlobalApp.takeDefaultAction(edtPassword, LoginActivity.this,
					"Password cannot be blank");
			return false;
		} else if (GlobalApp.isBlank(edtConfirmPass)) {
			GlobalApp.takeDefaultAction(edtConfirmPass, LoginActivity.this,
					"Confirm password cannot be blank");
			return false;
		}
		return true;
	}

	/**
	 * @param reg_email
	 *            validates the email address
	 * @return true if email is valid
	 */
	public boolean validEmail(String reg_email) {
		boolean isvalid = false;
		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = reg_email;
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isvalid = true;
		}
		return isvalid;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.global, menu);
		restoreActionBar();
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if (id == R.id.action_reg) {
			switcher.showNext();
			return true;
		} else {
			switcher.showPrevious();
		}

		if (verCode >= 11)
			invalidateOptionsMenu();
		else
			supportInvalidateOptionsMenu();

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (isChanged) {
			menu.getItem(0).setTitle(R.string.action_login);
			isChanged = false;
		} else {
			menu.getItem(0).setTitle(R.string.action_register);
			isChanged = true;
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("register")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.setMessage("Registering Please wait...");
					progressDialog.show();
				}
			});
		} else if (msg.equalsIgnoreCase("login")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.setMessage("Login Please wait...");
					progressDialog.show();
				}
			});
		} else if (msg.equalsIgnoreCase("logout")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.setMessage("Logout Please wait...");
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("username_email_exist")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					GlobalApp.takeDefaultAction(edtEmail, LoginActivity.this,
							"Email already exist");
					GlobalApp.takeDefaultAction(edtUserName,
							LoginActivity.this, "Username already exist");
				}
			});
		} else if (msg.equalsIgnoreCase("username_exist")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					GlobalApp.takeDefaultAction(edtUserName,
							LoginActivity.this, "Username already exist");
				}
			});
		} else if (msg.equalsIgnoreCase("email_exist")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					GlobalApp.takeDefaultAction(edtEmail, LoginActivity.this,
							"Email already exist");
				}
			});
		} else if (msg.equalsIgnoreCase("register_success")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(LoginActivity.this,
							"Registered Successfully", Toast.LENGTH_LONG)
							.show();
					switcher.showNext();
					invalidateOptionsMenu();
				}
			});
		} else if (msg.equalsIgnoreCase("basic_param_missing")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(LoginActivity.this, "Basic params missing",
							Toast.LENGTH_LONG).show();
				}
			});
		} else if (msg.equalsIgnoreCase("login_success")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					GlobalApp.storeUserNameInSharedPreference(
							LoginActivity.this, edtLoginUsername.getText()
									.toString().trim());
					GlobalApp.storePasswordInSharedPreference(
							LoginActivity.this, edtLoginPassword.getText()
									.toString().trim());
					userData = userService.getUserData();
					wingleDB.open();
					wingleDB.addUserData(userData);
					wingleDB.close();
					startActivity(new Intent(LoginActivity.this,
							MainActivity.class));
					overridePendingTransition(android.R.anim.slide_in_left,
							android.R.anim.slide_out_right);
					finish();
				}
			});
		} else if (msg.equalsIgnoreCase("login_problem")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(LoginActivity.this,
							"Username or Password not valid", Toast.LENGTH_LONG)
							.show();
				}
			});
		} else if (msg.equalsIgnoreCase("logout_success")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(LoginActivity.this, "Logout Successfully",
							Toast.LENGTH_LONG).show();

				}
			});
		} else if (msg.equalsIgnoreCase("logout_problem")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(LoginActivity.this, "Problem in Logout",
							Toast.LENGTH_LONG).show();
				}
			});
		} else if (msg.equalsIgnoreCase("error")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(LoginActivity.this, "Connection problem",
							Toast.LENGTH_SHORT).show();
				}
			});

		} else if (msg.equalsIgnoreCase("dataerror")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(LoginActivity.this, "Problem parsing data",
							Toast.LENGTH_SHORT).show();
				}
			});

		}
	}

	public void facebookLogin(View v){
		Session.openActiveSession(this, true, new Session.StatusCallback() {

		      // callback when session changes state
		      @Override
		      public void call(Session session, SessionState state, Exception exception) {
		        if (session.isOpened()) {

		          // make request to the /me API
		          Request.newMeRequest(session, new Request.GraphUserCallback() {

		            // callback after Graph API response with user object
		            @Override
		            public void onCompleted(GraphUser user, Response response) {
		              if (user != null) {
		                TextView welcome = (TextView) findViewById(R.id.welcomename);
		                profilepicture.setVisibility(View.VISIBLE);
		                welcome.setText("Hello " + user.getName() + "!");
		                profilepicture.setProfileId(user.getId());
		                	
		                Intent i= new Intent(getApplicationContext(),ProfileFragmentActivity.class);
		                i.putExtra("pid", user.getId());
		                startActivity(i);
		                //  welcome.setEnabled(false);
		                
		              }
		            }
		          }).executeAsync();
		        }
		      }
		    });
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}
	
	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("error")) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(LoginActivity.this, "Connection problem",
							Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

}
