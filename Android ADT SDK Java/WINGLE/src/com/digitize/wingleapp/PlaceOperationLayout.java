package com.digitize.wingleapp;

import com.digitize.wingleapp.places.PlaceItem;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class PlaceOperationLayout extends FrameLayout {

	private TextView txtDistance;
	private TextView txtPeopleCount;
	private Button txtRefresh;
	TextView txtHeading;
	Button btnComeHere, btnDrawRoot, btnSeePeople;;
	OnClickListener comeHereBtnListener;
	OnClickListener drawRootBtnListener;
	OnClickListener seePeopleBtnListener;
	OnClickListener refreshListener;

	public static final String COMING_HERE_CAPTION = "Coming here";
	public static final String CANCEL_CAPTION = "Cancel";

	private PlaceItem placeItemInstanceVO = null;
	private Button[] btnCollection = null;

	public void alignBtn() {
		alignButtons(GlobalApp.screenWidth / 2, GlobalApp.screenHeight / 2,
				GlobalApp.screenWidth / 3);
	}

	private void initializeButtons() {
		btnCollection = new Button[5];
		btnCollection[0] = (Button) findViewById(R.id.btn_come_here1);
		btnCollection[1] = (Button) findViewById(R.id.btn_come_here2);
		btnCollection[2] = (Button) findViewById(R.id.btn_come_here3);
		btnCollection[3] = (Button) findViewById(R.id.btn_come_here4);
		btnCollection[4] = (Button) findViewById(R.id.btn_come_here5);

	}

	public void makeAllVisible(boolean flag) {
		for (int i = 0; i < btnCollection.length; i++) {
			if (flag)
				btnCollection[i].setVisibility(View.VISIBLE);
			else
				btnCollection[i].setVisibility(View.GONE);
		}
	}

	public void alignButtons(int pointx, int pointy, int radius) {

		for (int i = 0; i < btnCollection.length; i++) {
			double t = 2 * Math.PI * i / btnCollection.length;
			int x = (int) Math.round(pointx + radius * Math.cos(t));
			int y = (int) Math.round(pointy + radius * Math.sin(t));
			alignButton(x, y, btnCollection[i]);
		}
	}

	public void alignButton(int left, int top, Button btn) {
		Rect rectf = new Rect();
		btn.getLocalVisibleRect(rectf);
		FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) btn
				.getLayoutParams();
		params.setMargins(left - (rectf.width() / 2), top
				- (rectf.height() / 2), 0, 0);
		btn.setLayoutParams(params);
	}

	public PlaceOperationLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public PlaceOperationLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();
		((Activity) getContext()).getLayoutInflater().inflate(
				R.layout.place_operation_layout, this);
		txtHeading = (TextView) findViewById(R.id.txt_heading);
		txtDistance = (TextView) findViewById(R.id.txt_distance);
		txtPeopleCount = (TextView) findViewById(R.id.txt_peaople_count);
		txtRefresh = (Button) findViewById(R.id.txt_refresh);
		btnComeHere = (Button) findViewById(R.id.btn_come_here);
		btnDrawRoot = (Button) findViewById(R.id.btn_draw_root);
		btnSeePeople = (Button) findViewById(R.id.btn_see_people);
		initializeButtons();

	}

	public PlaceItem getPlaceItemInstanceVO() {
		return placeItemInstanceVO;
	}

	public void setPlaceItemInstanceVO(PlaceItem placeItemInstanceVO) {
		this.placeItemInstanceVO = placeItemInstanceVO;
	}

	public void setLocalPlaceId(long localPlaceId) {
		placeItemInstanceVO.setLocalPlaceId(localPlaceId);
	}

	public void setPeopleCount(int count) {
		txtPeopleCount.setText(String.valueOf(count));
	}

	public void setDrawRouteButtonText(String text) {
		btnDrawRoot.setText(text);
	}

	public void setComeRouteText(String comeHereText) {
		btnComeHere.setText(comeHereText);
	}

	public void setComeHereListener(OnClickListener comeHereListener) {
		this.comeHereBtnListener = comeHereListener;
	}

	public void setDrawRootListener(OnClickListener drawRootListener) {
		this.drawRootBtnListener = drawRootListener;
	}

	public void setSeePeopleListener(OnClickListener seePeopleListener) {
		this.seePeopleBtnListener = seePeopleListener;
	}

	public void setRefreshListener(OnClickListener seePeopleListener) {
		this.refreshListener = seePeopleListener;
		txtRefresh.setOnClickListener(refreshListener);
	}

	public void setDataInFields(String name, String distance, String people,
			String popularity) {
		txtHeading.setText(name);
		txtDistance.setText(distance);
		txtPeopleCount.setText(people);
		txtRefresh.setText(popularity);
		btnComeHere.setOnClickListener(comeHereBtnListener);
		btnDrawRoot.setOnClickListener(drawRootBtnListener);
		btnSeePeople.setOnClickListener(seePeopleBtnListener);
	}

}
