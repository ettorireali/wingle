package com.digitize.wingleapp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.digitize.wingleapp.imagepart.ImageLoader;
import com.digitize.wingleapp.pojo.PreferenceSet;
import com.digitize.wingleapp.pojo.ProfileData;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.UserActionService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SeePeopleDetailsFragment extends Fragment implements
		ServiceCallback {

	TextView txtPeopleFirstName, txtPeopleLastName, txtPeopleUserName,
			txtPeopleEmail, txtPeopleGender, txtPeopleDob, txtPeopleLikes,
			txtPeopleDislikes;
	TextView txtPeopleFavBar, txtPeopleShownToOther, txtPeopleAboutMe;
	ProfileData peopleDetail = null;
	ImageView imgUserProfile;
	Button btnFollow, btnChat;
	AQuery aQuery = null;
	boolean isOptionChanged = false;
	ProgressDialog progressDialog = null;
	UserActionService userActionService = null;
	PreferenceSet prefSet = null;
	SeePeopleFragmentActivity parentOfFragment = null;

	public void setPeopleData(ProfileData peopleDetail) {
		this.peopleDetail = peopleDetail;
		prefSet = new PreferenceSet();
		prefSet.setPrivacy(peopleDetail.getPrivacy());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.people_details, null);
		btnFollow = (Button) view.findViewById(R.id.btn_peopledetail_follow);
		btnChat = (Button) view.findViewById(R.id.btn_peopledetail_chat);
		txtPeopleFirstName = (TextView) view
				.findViewById(R.id.txt_people_detail_firstname);
		txtPeopleLastName = (TextView) view
				.findViewById(R.id.txt_people_detail_lastname);
		txtPeopleEmail = (TextView) view
				.findViewById(R.id.txt_people_detail_email);
		txtPeopleGender = (TextView) view
				.findViewById(R.id.txt_people_detail_gender);
		txtPeopleDob = (TextView) view.findViewById(R.id.txt_people_detail_dob);
		txtPeopleLikes = (TextView) view
				.findViewById(R.id.txt_people_detail_likes);
		txtPeopleDislikes = (TextView) view
				.findViewById(R.id.txt_people_detail_dislike);
		txtPeopleFavBar = (TextView) view
				.findViewById(R.id.txt_people_detail_fav_bars);
		txtPeopleShownToOther = (TextView) view
				.findViewById(R.id.txt_people_detail_shown_others);
		txtPeopleAboutMe = (TextView) view
				.findViewById(R.id.txt_people_detail_about_me);
		imgUserProfile = (ImageView) view.findViewById(R.id.img_profile);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		aQuery = new AQuery(getActivity());
		setDataInPeopleDetailFields();
		userActionService = new UserActionService(this);
		userActionService.setMyId(Long.parseLong(GlobalApp
				.getUidFromSharedPreference(getActivity())));
		btnFollow.setOnClickListener(followClickListener);
		btnChat.setOnClickListener(chatClickListener);
		if (peopleDetail.isFollowing()) {
			btnFollow.setText("Unfollow");
		} else {
			btnFollow.setText("Follow");
		}
		progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		progressDialog.setMessage("Please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);

		btnChat.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				parentOfFragment.switchChatFragment(peopleDetail);
			}
		});
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		parentOfFragment = (SeePeopleFragmentActivity) activity;
	}

	public OnClickListener followClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (btnFollow.getText().toString().equals("Follow")) {
				userActionService.setTargetId(Long.parseLong(peopleDetail
						.getUid()));
				userActionService
						.setActionType(UserActionService.ACTION_FOLLOW);
				new Thread(userActionService).start();
				btnFollow.setText("Unfollow");
			} else if (btnFollow.getText().toString().equals("Unfollow")) {
				userActionService.setTargetId(Long.parseLong(peopleDetail
						.getUid()));
				userActionService
						.setActionType(UserActionService.ACTION_UNFOLLOW);
				new Thread(userActionService).start();
				btnFollow.setText("Follow");
			}
		}
	};
	public OnClickListener chatClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

		}
	};

	public void setDataInPeopleDetailFields() {

		displayImage(peopleDetail.getImagePath());
		txtPeopleFirstName.setText(peopleDetail.getFirstName());
		txtPeopleLastName.setText(peopleDetail.getLastName());

		if (prefSet.getPrivacyStatus("email").equals("Default")
				|| prefSet.getPrivacyStatus("email").equals("Public")) {
			txtPeopleEmail.setText(peopleDetail.getEmail());
		} else if (prefSet.getPrivacyStatus("email").equals("Private")) {
			txtPeopleEmail.setText("Not shared with you");
		} else if (prefSet.getPrivacyStatus("email").equals("Followers Only")) {
			txtPeopleEmail.setText("Followers only");
		}

		if (peopleDetail.getGender() == 1) {
			txtPeopleGender.setText("Male");
		} else {
			txtPeopleGender.setText("Female");
		}

		Date resDob = stringToDate(peopleDetail.getDob());
		String strDob = dateToString(resDob);
		if (prefSet.getPrivacyStatus("dob").equals("Default")
				|| prefSet.getPrivacyStatus("dob").equals("Public")) {
			txtPeopleDob.setText(strDob);
		} else if (prefSet.getPrivacyStatus("dob").equals("Private")) {
			txtPeopleDob.setText("Not shared with you");
		} else if (prefSet.getPrivacyStatus("dob").equals("Followers Only")) {
			txtPeopleDob.setText("Followers only");
		}

		txtPeopleLikes.setText(peopleDetail.getLikes());
		txtPeopleDislikes.setText(peopleDetail.getDislike());
		txtPeopleFavBar.setText(peopleDetail.getFavBars());
		if (peopleDetail.getIsShownToOthers() == 1) {
			txtPeopleShownToOther.setText("Yes");
		} else {
			txtPeopleShownToOther.setText("No");
		}

		txtPeopleAboutMe.setText(peopleDetail.getAboutMe());
	}

	public Date stringToDate(String date) {
		Date d = new Date();
		Log.w("string_to_date", "string date is : " + date);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = format.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}

	public String dateToString(Date date) {
		String datetime = null;
		SimpleDateFormat dateformat = new SimpleDateFormat("dd MMM,yyyy");
		datetime = dateformat.format(date);
		return datetime;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		getActivity().getMenuInflater().inflate(R.menu.people_detail_option,
				menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();

		if (id == R.id.action_block) {
			userActionService
					.setTargetId(Long.parseLong(peopleDetail.getUid()));
			userActionService.setActionType(UserActionService.ACTION_BLOCK);
			new Thread(userActionService).start();
			isOptionChanged = true;

		} else if (id == R.id.action_unblock) {
			userActionService
					.setTargetId(Long.parseLong(peopleDetail.getUid()));
			userActionService.setActionType(UserActionService.ACTION_UNBLOCK);
			new Thread(userActionService).start();
			isOptionChanged = false;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onPrepareOptionsMenu(menu);
		if (peopleDetail.isBlocked()) {
			menu.clear();
			getActivity().getMenuInflater().inflate(
					R.menu.people_detail_unblock_option, menu);
		} else {
		}
	}

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase(UserActionService.SERVICE_NAME)) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		}
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase(UserActionService.SUCCESS_MESSAGE)) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					if (userActionService.getActionType().equals(
							UserActionService.ACTION_FOLLOW)) {
						Toast.makeText(getActivity(), "Followed",
								Toast.LENGTH_SHORT).show();
					} else if (userActionService.getActionType().equals(
							UserActionService.ACTION_UNFOLLOW)) {
						Toast.makeText(getActivity(), "Unfollowed",
								Toast.LENGTH_SHORT).show();
					} else if (userActionService.getActionType().equals(
							UserActionService.ACTION_BLOCK)) {
						Toast.makeText(getActivity(), "Blocked",
								Toast.LENGTH_SHORT).show();
						peopleDetail.setBlocked(true);
						getActivity().invalidateOptionsMenu();
					} else if (userActionService.getActionType().equals(
							UserActionService.ACTION_UNBLOCK)) {
						Toast.makeText(getActivity(), "Unblocked",
								Toast.LENGTH_SHORT).show();
						peopleDetail.setBlocked(false);
						getActivity().invalidateOptionsMenu();
					}
				}
			});
		} else if (msg.equalsIgnoreCase(UserActionService.ERROR_MESSAGE)) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT)
							.show();
				}
			});
		}
	}

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}

	public void displayImage(String url) {
		aQuery.id(imgUserProfile)
				.progress(R.id.icon_progress)
				.image(url, true, true, (int) (200), R.drawable.noimage,
						new BitmapAjaxCallback() {
							@Override
							protected void callback(String url, ImageView iv,
									Bitmap bm, AjaxStatus status) {
								// TODO Auto-generated method stub
								super.callback(url, iv, bm, status);
								Log.w("fired",
										url + " status : " + status.getCode());
							}
						});
	}

}
