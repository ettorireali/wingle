package com.digitize.wingleapp;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.places.GooglePlaces;
import com.digitize.wingleapp.places.Place;
import com.digitize.wingleapp.places.Place.Location;
import com.digitize.wingleapp.places.PlaceItem;
import com.digitize.wingleapp.places.PlacesList;
import com.digitize.wingleapp.pojo.PlaceBookVO;
import com.digitize.wingleapp.pojo.ProximityData;
import com.digitize.wingleapp.services.AppConfig;
import com.digitize.wingleapp.services.PlaceDecideService;
import com.digitize.wingleapp.services.ProximityService;
import com.digitize.wingleapp.services.ServiceCallback;
import com.digitize.wingleapp.services.TraverseNodesService;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.algo.GridBasedAlgorithm;
import com.google.maps.android.clustering.algo.PreCachingAlgorithmDecorator;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.google.maps.android.clustering.ClusterManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ext.SatelliteMenu;
import android.view.ext.SatelliteMenu.SateliteClickedListener;
import android.view.ext.SatelliteMenuItem;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("WrongCall")
public class MapFragment extends Fragment implements ServiceCallback,
		ClusterManager.OnClusterClickListener<PlaceItem>,
		ClusterManager.OnClusterInfoWindowClickListener<PlaceItem>,
		ClusterManager.OnClusterItemClickListener<PlaceItem>,
		ClusterManager.OnClusterItemInfoWindowClickListener<PlaceItem>,
		SurfaceHolder.Callback2 {

	private Marker myPlaceMarker;
	boolean drawRouteClicked = false;
	private boolean isResumed = false;
	ProgressDialog progressDialog;
	boolean panelFlag = false;
	private GoogleMap mMap;
	Marker marker = null;

	// Google Places
	GooglePlaces googlePlaces;
	// Places List
	PlacesList nearPlaces;
	private Activity parentActivity = null;
	ProgressDialog pDialog;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	PlaceOperationLayout placeOperationLayout = null;
	// ListItems data
	ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();
	ArrayList<ProximityData> proximityDataList;
	private LatLng mLatLng = null;
	private LatLng clickedPosition;
	ProximityService proximityService;
	private View view = null;
	private PlaceDecideService placeDecideService;
	private ClusterManager<PlaceItem> mClusterManager;
	ArrayList<PlaceItem> placeItem = new ArrayList<PlaceItem>();
	boolean firstAnim = false; // to restrict frequent camera update
	private WingleDB wingleDb = null;
	private String uId = null;
	private boolean isEntryExists = false;
	private MediaPlayer mp = null;

	ArrayList<Polyline> polyLineList = new ArrayList<Polyline>();
	private PreCachingAlgorithmDecorator<PlaceItem> cacheAlgo = null;
	private Vector<String> googleSearchId = new Vector<String>();
	public TraverseNodesService traverseNodeService = null;
	private FrameLayout parentOfMap = null;
	// private SatelliteMenu menu =null;
	private FrameLayout containerBtn = null;
	/**
	 * this is to have surface layer over mapfragment
	 */
	PaintSurface surfaceView = null;
	SurfaceHolder holder = null;
	public Projection projection = null;

	private Button[] btnCollection = null;

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		surfaceView.resume();
		if (GlobalApp.checkInternetConnection(getActivity())) {
			setUpMapIfNeeded("normal");
		} else {
			Toast.makeText(getActivity(), "Internet not available",
					Toast.LENGTH_SHORT).show();
		}
	}

	/*
	 * public void alignCenterPart(int left,int top){ FrameLayout.LayoutParams
	 * params=(FrameLayout.LayoutParams)menu.getLayoutParams();
	 * params.setMargins(left,0,top,0);
	 * 
	 * menu.setLayoutParams(params); }
	 */
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		surfaceView.pause();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		surfaceView.destroyDrawingCache();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (view != null) {
			ViewGroup parent = (ViewGroup) view.getParent();
			if (parent != null)
				parent.removeView(view);
		}
		try {
			view = inflater.inflate(R.layout.map_layout, container, false);
			placeOperationLayout = (PlaceOperationLayout) view
					.findViewById(R.id.linear_map_panel);
			surfaceView = (PaintSurface) view.findViewById(R.id.faded_surface);

			parentOfMap = (FrameLayout) view.findViewById(R.id.map_host_layout);
			containerBtn = (FrameLayout) view
					.findViewById(R.id.button_container);

		} catch (InflateException e) {
			/* map is already there, just return view as it is */
		}
		return view;
	}

	public void setPanelVisibility(boolean status) {
		if (status) {
			Animation an = AnimationUtils.loadAnimation(getActivity(),
					R.anim.slide_up);
			placeOperationLayout.startAnimation(an);
			placeOperationLayout.setVisibility(View.VISIBLE);

		} else {
			Animation ani = AnimationUtils.loadAnimation(getActivity(),
					R.anim.slide_down);
			placeOperationLayout.startAnimation(ani);
			placeOperationLayout.setVisibility(View.GONE);
		}
	}

	public void callProximityService(double lat, double lon) {
		if (existsOnMap(new LatLng(lat, lon))) {
			proximityService.setLatitude(lat);
			proximityService.setLongitude(lon);
			new Thread(proximityService).start();
		}
	}

	/**
	 * Draws profile photos inside markers (using IconGenerator). When there are
	 * multiple people in the cluster, draw multiple photos (using
	 * MultiDrawable).
	 */
	private class PlaceRenderer extends DefaultClusterRenderer<PlaceItem> {
		private final IconGenerator mIconGenerator = new IconGenerator(
				parentActivity.getApplicationContext());
		private final IconGenerator mClusterIconGenerator = new IconGenerator(
				parentActivity.getApplicationContext());
		private final FrameLayout mTextData;
		private final TextView txtCounter;

		public PlaceRenderer() {
			super(parentActivity.getApplicationContext(), mMap, mClusterManager);
			View multiProfile = parentActivity.getLayoutInflater().inflate(
					R.layout.marker_with_counter, null);
			mClusterIconGenerator.setContentView(multiProfile);
			View multiProfileChild = parentActivity.getLayoutInflater()
					.inflate(R.layout.marker_with_counter, null);
			mTextData = (FrameLayout) multiProfileChild
					.findViewById(R.id.marker_parent_layout);
			mIconGenerator.setContentView(mTextData);
			txtCounter = (TextView) mTextData.findViewById(R.id.txt_counter);
		}

		@Override
		protected void onBeforeClusterItemRendered(PlaceItem placeItem,
				MarkerOptions markerOptions) {
			// Draw a single PlaceItem.
			// Set the info window to show their name.
			txtCounter.setText(String.valueOf(placeItem.getCounter()));

			if (placeItem.areYouHere(GlobalApp
					.getUidFromSharedPreference(getActivity()))) {
				txtCounter.setBackgroundResource(R.drawable.grey_rounded_bg);
			} else {
				txtCounter.setBackgroundResource(R.drawable.white_rounded_bg);
			}

			Bitmap icon = mIconGenerator.makeIcon();
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
					.title(placeItem.getPlaceInstance().name)
					.anchor(0.5f, 0.5f);
		}

		@Override
		protected void onBeforeClusterRendered(Cluster<PlaceItem> cluster,
				MarkerOptions markerOptions) {
			// Draw multiple people.
		}

		@Override
		protected boolean shouldRenderAsCluster(Cluster cluster) {
			// Always render clusters.
			return false;
		}

		@Override
		public void onRemove() {
			// TODO Auto-generated method stub
			super.onRemove();
		}

		@Override
		protected void onClusterItemRendered(PlaceItem clusterItem,
				Marker marker) {
			// TODO Auto-generated method stub
			super.onClusterItemRendered(clusterItem, marker);
			// markerHashMap.put(clusterItem, marker);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		uId = GlobalApp.getUidFromSharedPreference(getActivity());
		wingleDb = new WingleDB(getActivity());
		progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
		progressDialog.setTitle("WINGLE");
		progressDialog.setMessage("Submitting please wait...");
		progressDialog.setCancelable(true);
		progressDialog.setCanceledOnTouchOutside(false);

		if (GlobalApp.checkInternetConnection(getActivity())) {
			setUpMapIfNeeded("normal");
		} else {
			Toast.makeText(getActivity(), "Internet not available",
					Toast.LENGTH_SHORT).show();
		}
		try {
			placeOperationLayout.setComeHereListener(panelComeHereBtnListener);
			placeOperationLayout.setDrawRootListener(panelDrawRootListener);
			placeOperationLayout.setSeePeopleListener(seePeopleListener);
			placeOperationLayout.setRefreshListener(refreshClickListener);
		} catch (NullPointerException nex) {

		}
		placeDecideService = new PlaceDecideService(MapFragment.this,
				getActivity());
		proximityService = new ProximityService(MapFragment.this, getActivity());
		proximityService.setUid(uId);
		mClusterManager = new ClusterManager<PlaceItem>(getActivity(), mMap);
		mClusterManager.setRenderer(new PlaceRenderer());
		mMap.setOnCameraChangeListener(mClusterManager);
		mMap.setOnMarkerClickListener(mClusterManager);
		mMap.setOnInfoWindowClickListener(mClusterManager);
		mClusterManager.setOnClusterClickListener(this);
		mClusterManager.setOnClusterInfoWindowClickListener(this);
		mClusterManager.setOnClusterItemClickListener(this);
		mClusterManager.setOnClusterItemInfoWindowClickListener(this);
		cacheAlgo = new PreCachingAlgorithmDecorator<PlaceItem>(
				new GridBasedAlgorithm<PlaceItem>());
		mClusterManager.setAlgorithm(cacheAlgo);
		AudioManager am = (AudioManager) getActivity().getSystemService(
				Context.AUDIO_SERVICE);
		am.setStreamVolume(AudioManager.STREAM_MUSIC,
				am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);

		mMap.setOnMapClickListener(new OnMapClickListener() {
			@Override
			public void onMapClick(LatLng point) {
				// TODO Auto-generated method stub
				setPanelVisibility(false);
			}
		});

		traverseNodeService = new TraverseNodesService(this);
		traverseNodeService.setUid(GlobalApp
				.getUidFromSharedPreference(getActivity()));
		if(surfaceView !=null){
		surfaceView.setZOrderOnTop(true);
		holder = surfaceView.getHolder();
		holder.setFormat(PixelFormat.TRANSLUCENT);
		holder.addCallback(this);

		surfaceView.setBackgroundColor(Color.TRANSPARENT);
		}

		parentOfMap.requestTransparentRegion(parentOfMap);
	}

	public void increaseCounter(LatLng targetLatLng, String targetUid) {
		final PlaceItem placeInstance = GlobalApp.getTargetPlaceItem(placeItem,
				targetLatLng);
		placeInstance.increaseCounter();
		placeInstance.addMe(targetUid);

		if (placeInstance != null) {
			mClusterManager.removeItem(placeInstance);
			cacheAlgo.removeItem(placeInstance);
		}

		mClusterManager.cluster();
		progressDialog.dismiss();

		uiHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				mClusterManager.addItem(placeInstance);
				mClusterManager.cluster();
			}
		}, 40);

		playBeep();
	}

	private boolean existsOnMap(LatLng latLng) {
		Iterator<PlaceItem> placeIterator = placeItem.iterator();
		PlaceItem tempInstance = null;
		boolean isExists = false;
		while (placeIterator.hasNext()) {
			tempInstance = placeIterator.next();
			if (SphericalUtil.computeDistanceBetween(latLng,
					tempInstance.getPosition()) < 10) {
				isExists = true;
				break;
			}
		}
		return isExists;
	}

	public OnClickListener refreshClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			callProximityService(placeOperationLayout.getPlaceItemInstanceVO()
					.getPosition().latitude, placeOperationLayout
					.getPlaceItemInstanceVO().getPosition().longitude);
		}
	};

	public OnClickListener panelComeHereBtnListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (GlobalApp.checkInternetConnection(getActivity())) {

				long localPlaceId = 0;
				wingleDb.open();
				if (!(isEntryExists = wingleDb.isEntryExists(uId,
						placeOperationLayout.getPlaceItemInstanceVO()
								.getPosition()))) {
					// localPlaceId=wingleDb.addPlaceToVisit(placeOperationLayout.getPlaceItemInstanceVO(),GlobalApp.getUidFromSharedPreference(getActivity()));
					placeOperationLayout.setLocalPlaceId(localPlaceId);
				} else {
					wingleDb.deletePlaceInfo(placeOperationLayout
							.getPlaceItemInstanceVO().getLivePlaceId());
					placeOperationLayout.getPlaceItemInstanceVO().removeMySelf(
							uId);
					placeOperationLayout
							.setComeRouteText(MapPanel.COMING_HERE_CAPTION);
					changeCounter(
							placeOperationLayout.getPlaceItemInstanceVO(),
							false);
				}
				wingleDb.close();

				if (!isEntryExists) {
					progressDialog.setMessage("Submitting please wait...");
					placeDecideService.setUid(uId);
					placeDecideService.setLat(placeOperationLayout
							.getPlaceItemInstanceVO().getPosition().latitude);
					placeDecideService.setLon(placeOperationLayout
							.getPlaceItemInstanceVO().getPosition().longitude);
					// placeDecideService.setPlaceInfo(placeOperationLayout.getPlaceItemInstanceVO().getPlaceInstance().formatted_address);
					placeDecideService.setPlaceInfo("formatted address");
					placeDecideService.setMessage("{\"pid\":\"" + uId
							+ "\",\"pos\":[" + placeDecideService.getLat()
							+ "," + placeDecideService.getLon() + "]}");
					// placeDecideService.setMessage("custom meessdsfa");
					new Thread(placeDecideService).start();
				}

			} else {
				Toast.makeText(getActivity(), "Internet not available",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	public OnClickListener panelDrawRootListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (GlobalApp.checkInternetConnection(getActivity())) {
				if (!drawRouteClicked) {
					if (!clickedPosition.equals(null)) {
						drawRoute(clickedPosition, myPlaceMarker.getPosition());
						drawRouteClicked = true;
						placeOperationLayout.setDrawRouteButtonText("");
						placeOperationLayout
								.setDrawRouteButtonText("Clear Route");
					} else {
						Toast.makeText(getActivity(),
								"Select current position", Toast.LENGTH_SHORT)
								.show();
					}
				} else {
					placeOperationLayout.setDrawRouteButtonText("");
					placeOperationLayout.setDrawRouteButtonText("Draw Route");
					removePolyline();
					drawRouteClicked = false;

				}
			} else {
				Toast.makeText(getActivity(), "Internet not available",
						Toast.LENGTH_SHORT).show();
			}
			// Toast.makeText(getActivity(), "Draw Root Clicked",
			// Toast.LENGTH_SHORT).show();
		}
	};
	public OnClickListener seePeopleListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent peopleFragIntent = new Intent(getActivity(),
					SeePeopleFragmentActivity.class);
			peopleFragIntent.putExtra("member_ids", placeOperationLayout
					.getPlaceItemInstanceVO().getUidCollection().toString());
			startActivity(peopleFragIntent);
			getActivity().overridePendingTransition(
					android.R.anim.slide_in_left,
					android.R.anim.slide_out_right);
		}
	};

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		parentActivity = activity;

	}

	public void setUpMapIfNeeded(String type) {
		try {
			if (mMap == null) {
				mMap = ((SupportMapFragment) getFragmentManager()
						.findFragmentById(R.id.map)).getMap();
			}
			if (mMap != null) {
				if (type.equals("normal")) {
					mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				} else if (type.equals("terrain")) {
					mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
				} else if (type.equals("satellite")) {
					mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
				} else if (type.equals("hybrid")) {
					mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
				}

				mMap.setTrafficEnabled(true);
				mMap.setIndoorEnabled(true);
				mMap.setMyLocationEnabled(true);
				mMap.getUiSettings().setCompassEnabled(true);
				projection = mMap.getProjection();

				// mMap.setOnMarkerClickListener(markerClickListener);
			}
		} catch (Exception e) {
			// Toast.makeText(getActivity().getBaseContext(),"Exception in setupMap() :"+e.toString(),Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	public void setMyMarker(LatLng ltlng) {
		if (myPlaceMarker != null) {
			myPlaceMarker.remove();
			myPlaceMarker = null;
		}
		mLatLng = ltlng;
		// Toast.makeText(getActivity().getBaseContext(),
		// "set my marker called "+ltlng.latitude+":"+ltlng.longitude,
		// Toast.LENGTH_LONG).();
		myPlaceMarker = mMap.addMarker(new MarkerOptions()
				.position(ltlng)
				.title("My Location")
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
		// myPlaceMarker.showInfoWindow();
		// myPlaceMarker=mMap.addMarker(new MarkerOptions().position(new
		// LatLng(20.54879856,
		// 70.597897979)).title("Marker").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
		// mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ltlng,12.5F));

		if (!firstAnim) {
			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(ltlng).tilt(50F).zoom(17.5F).bearing(300F).build();
			mMap.animateCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition));
			firstAnim = true;
		}
		new LoadPlaces().execute();
	}

	public OnMarkerClickListener markerClickListener = new OnMarkerClickListener() {
		@Override
		public boolean onMarkerClick(Marker marker) {
			// TODO Auto-generated method stub
			if (!marker.equals(myPlaceMarker)) {
				panelFlag = !panelFlag;
				setPanelVisibility(panelFlag);
				double distanceInKm = GlobalApp.round(GlobalApp
						.distanceFromCurrentLocation(
								myPlaceMarker.getPosition().latitude,
								myPlaceMarker.getPosition().longitude,
								marker.getPosition().latitude,
								marker.getPosition().longitude), 2, 0);
				placeOperationLayout.setDataInFields(marker.getTitle(),
						String.valueOf(distanceInKm) + " km", "200 were here",
						"Popularity");
				clickedPosition = marker.getPosition();
				return panelFlag;
			}
			return false;
		}
	};

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (mp != null)
			mp.release();
	}

	/**
	 * Background Async Task to Load Google places
	 * */
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			googlePlaces = new GooglePlaces();
			try {
				String types = AppConfig.DEFAULT_TYPE_TO_SEARCH; // Listing
																	// places
																	// only
																	// cafes,
																	// restaurants
				// Radius in meters - increase this value if you don't find any
				// places
				double radius = GlobalApp.getDefaultProximity(getActivity()) * 1000;// 1000
																					// meters
				// get nearest places
				nearPlaces = googlePlaces.search(mLatLng.latitude,
						mLatLng.longitude, radius, types);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			if (nearPlaces != null) {
				if (nearPlaces.results != null) {
					Iterator<Place> placeIterator = nearPlaces.results
							.iterator();
					Place tempPlace = null;
					BitmapDrawable bd = (BitmapDrawable) getResources()
							.getDrawable(R.drawable.ic_launcher);
					Bitmap b = bd.getBitmap();
					// Bitmap bhalfsize=Bitmap.createScaledBitmap(b,
					// b.getWidth()/2,b.getHeight()/2, false);
					PlaceItem pItem = null;

					while (placeIterator.hasNext()) {
						tempPlace = placeIterator.next();
						if (!googleSearchId.contains(tempPlace.id)) {
							pItem = new PlaceItem(tempPlace);
							placeItem.add(pItem);
							googleSearchId.add(tempPlace.id);
							traverseNodeService.feedNewLatLong(pItem
									.getPosition());
						}
					}

					/*
					 * wingleDb.open(); wingleDb.associatePlaceData(placeItem,
					 * GlobalApp.getUidFromSharedPreference(getActivity()));
					 * wingleDb.close();
					 */
					mClusterManager.addItems(placeItem);
					mClusterManager.cluster();
					new Thread(traverseNodeService).start();
				}
			}
		}

	}

	@Override
	public void serviceStarted(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase(PlaceDecideService.SERVICE_NAME)) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.show();
				}
			});
		} else if (msg.equalsIgnoreCase("proximity_started")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.setMessage("Getting data...");
					progressDialog.show();
				}
			});
		}
	}

	/**
	 * 
	 * @param placeItem
	 * @param flag
	 *            "true" for increase "false" to decrease
	 */
	public void changeCounter(final PlaceItem placeItem, boolean flag) {
		mClusterManager.removeItem(placeItem);
		cacheAlgo.removeItem(placeItem);
		mClusterManager.cluster();
		if (flag)
			placeItem.increaseCounter();
		else
			placeItem.decreaseCounter();

		uiHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				mClusterManager.addItem(placeItem);
				mClusterManager.cluster();
			}
		}, 40);
	}

	@Override
	public void serviceEnd(String msg) {
		// TODO Auto-generated method stub
		if (msg.equalsIgnoreCase("missing_params")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Params missing",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase(PlaceDecideService.SUCCESS_MESSAGE)) {
			wingleDb.open();
			wingleDb.updatePlaceLiveId(placeOperationLayout
					.getPlaceItemInstanceVO().getLocalPlaceId(),
					placeDecideService.getLivePlaceId());
			wingleDb.close();
			/*
			 * add self to uid collection
			 */
			placeOperationLayout.getPlaceItemInstanceVO().addMe(uId);
			changeCounter(placeOperationLayout.getPlaceItemInstanceVO(), true);
			placeOperationLayout.getPlaceItemInstanceVO().setLivePlaceId(
					placeDecideService.getLivePlaceId());
			placeOperationLayout.getPlaceItemInstanceVO().setLivePlaceId(
					placeDecideService.getLivePlaceId());
			placeOperationLayout.getPlaceItemInstanceVO().setLocalPlaceId(
					placeOperationLayout.getPlaceItemInstanceVO()
							.getLocalPlaceId());
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					placeOperationLayout
							.setComeRouteText(MapPanel.CANCEL_CAPTION);
					Toast.makeText(getActivity(), "Place decided..",
							Toast.LENGTH_SHORT).show();
				}
			});

		} else if (msg.equalsIgnoreCase("message")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();

				}
			});
		} else if (msg.equalsIgnoreCase("error")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Problem submitting place",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase("proximity_data")) {
			proximityDataList = proximityService.getProximityData();
			final PlaceItem placeInstance = GlobalApp
					.associateCountersWithMarkers(
							placeItem,
							proximityDataList,
							GlobalApp.getUidFromSharedPreference(getActivity()),
							wingleDb);

			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {

						if (placeInstance != null) {
							mClusterManager.removeItem(placeInstance);
							cacheAlgo.removeItem(placeInstance);
						}
						mClusterManager.cluster();
						progressDialog.dismiss();

						uiHandler.postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								mClusterManager.addItem(placeInstance);
								mClusterManager.cluster();
							}
						}, 80);
					} catch (ConcurrentModificationException cmodiEx) {
						Toast.makeText(getActivity(),
								"Concurrent modification exception ",
								Toast.LENGTH_LONG).show();
					}

					placeOperationLayout.setPeopleCount(placeInstance
							.getCounter());
					playBeep();
					// Toast.makeText(getActivity(), "Data arrived",
					// Toast.LENGTH_SHORT).show();
				}
			});

		} else if (msg.equalsIgnoreCase("proximity_error")) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressDialog.dismiss();
					Toast.makeText(getActivity(), "Problem in data parsing",
							Toast.LENGTH_SHORT).show();
				}
			});
		} else if (msg.equalsIgnoreCase(TraverseNodesService.SUCCESS_MESSAGE)) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					JSONArray result = traverseNodeService.getResult();
					traverseAndAssociateCounter(result);
				}
			});
		}
	}

	public void traverseAndAssociateCounter(JSONArray res) {
		if (null != res && res.length() > 0) {
			PlaceItem pItem = null;
			try {
				JSONObject jobj = null;
				LatLng tempPos = null;
				for (int i = 0; i < res.length(); i++) {
					jobj = res.getJSONObject(i);
					tempPos = new LatLng(jobj.getDouble("lat"),
							jobj.getDouble("lon"));
					pItem = GlobalApp.getTargetPlaceItem(this.placeItem,
							tempPos);
					pItem.associatePlaceInstance(jobj.getJSONArray("uid"));
				}
			} catch (JSONException jex) {

			}
			boolean changeFlag = false;
			wingleDb.open();
			changeFlag = wingleDb.associatePlaceData(placeItem,
					GlobalApp.getUidFromSharedPreference(getActivity()));
			wingleDb.close();

			if (changeFlag) {

				Iterator<PlaceItem> placeIterator = placeItem.iterator();
				while (placeIterator.hasNext()) {
					try {
						final PlaceItem pItemInstance = placeIterator.next();
						if (pItem != null) {
							mClusterManager.removeItem(pItemInstance);
							cacheAlgo.removeItem(pItemInstance);
						}
						mClusterManager.cluster();
						progressDialog.dismiss();

						uiHandler.postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								mClusterManager.addItem(pItemInstance);
								mClusterManager.cluster();
							}
						}, 80);
					} catch (ConcurrentModificationException modificationEx) {
						Toast.makeText(getActivity(), "modification exception",
								Toast.LENGTH_LONG).show();
					}
				}
			}
		}
	}

	Handler uiHandler = new Handler();
	Iterator<PlaceItem> placeItemIterator = null;
	PlaceItem tempPlaceItemInstance = null;

	@Override
	public void serviceInProgress(String msg) {
		// TODO Auto-generated method stub

	}

	public void drawRoute(final LatLng sourcePosition, final LatLng destPosition) {
		progressDialog.setMessage("Drawing route please wait...");
		progressDialog.show();
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				GMapV2Direction md = new GMapV2Direction();
				Document doc = md.getDocument(sourcePosition, destPosition,
						GMapV2Direction.MODE_DRIVING);

				ArrayList<LatLng> directionPoint = md.getDirection(doc);

				final PolylineOptions rectLine = new PolylineOptions().width(3)
						.color(Color.RED);

				for (int i = 0; i < directionPoint.size(); i++) {
					rectLine.add(directionPoint.get(i));
				}
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Polyline polyLine = mMap.addPolyline(rectLine);
						polyLineList.add(polyLine);
						progressDialog.dismiss();

					}
				});

			}
		}).start();

	}

	public void removePolyline() {
		Iterator iterate = polyLineList.iterator();
		while (iterate.hasNext()) {
			Polyline polyline = (Polyline) iterate.next();
			polyline.remove();
		}
	}

	@Override
	public void onClusterItemInfoWindowClick(PlaceItem item) {
		// TODO Auto-generated method stub

	}

	public void jumpToLocation(LatLng newPos) {
		CameraUpdate cameraPosition = CameraUpdateFactory.newLatLng(newPos);
		mMap.animateCamera(cameraPosition);
		mLatLng = newPos;
		new LoadPlaces().execute();
	}

	@Override
	public boolean onClusterItemClick(PlaceItem item) {
		// TODO Auto-generated method stub
		panelFlag = !panelFlag;
		setPanelVisibility(panelFlag);
		double distanceInKm = GlobalApp.round(GlobalApp
				.distanceFromCurrentLocation(
						myPlaceMarker.getPosition().latitude,
						myPlaceMarker.getPosition().longitude,
						item.getPosition().latitude,
						item.getPosition().longitude), 2, 0);
		placeOperationLayout.setDataInFields(item.getPlaceInstance().name,
				String.valueOf(distanceInKm) + " km", item.getCounter()
						+ " were here", "Refresh");

		if (item.areYouHere(uId))
			placeOperationLayout.setComeRouteText(MapPanel.CANCEL_CAPTION);
		else
			placeOperationLayout.setComeRouteText(MapPanel.COMING_HERE_CAPTION);

		clickedPosition = item.getPosition();
		placeOperationLayout.setPlaceItemInstanceVO(item);
		Point point = mMap.getProjection().toScreenLocation(clickedPosition);
		markerTapped(point);

		// surfaceView.setPoint(projection.toScreenLocation(clickedPosition));
		surfaceView.setVisibility(View.VISIBLE);
		CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(
				clickedPosition, 17.5F);
		mMap.animateCamera(yourLocation);
		placeOperationLayout.bringToFront();
		placeOperationLayout.alignBtn();
		// Toast.makeText(getActivity(),
		// "total persons "+item.getUidCollection().toString(),
		// Toast.LENGTH_LONG).show();
		return true;
	}

	@Override
	public void onClusterInfoWindowClick(Cluster<PlaceItem> cluster) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean onClusterClick(Cluster<PlaceItem> cluster) {
		// TODO Auto-generated method stub
		return false;
	}

	public void playBeep() {
		mp = MediaPlayer.create(getActivity(), R.raw.ping);
		if (mp.isPlaying()) {
			mp.stop();
			mp.reset();
			mp = MediaPlayer.create(getActivity(), R.raw.ping);
		}
		mp.start();
	}

	public void markerTapped(Point point) {
		Canvas c = null;
		for (int i = 1; i <= 2; i++) {
			try {
				c = holder.lockCanvas(null);
				if (c != null) {
					synchronized (holder) {
						this.onDraw(c, point);
					}
				}
			} finally {
				// do this in a finally so that if an exception is thrown
				// during the above, we don't leave the Surface in an
				// inconsistent state
				if (c != null) {
					holder.unlockCanvasAndPost(c);
				}
			}
		}
	}

	protected void onDraw(Canvas canvas, Point point) {
		// TODO Auto-generated method stub
		Paint clearPaint = new Paint();
		// clearPaint.setAntiAlias(true);
		clearPaint.setAlpha(127);
		clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		canvas.drawRect(0, 0, GlobalApp.screenWidth, GlobalApp.screenHeight,
				clearPaint);

		Paint paint = new Paint();
		paint.setColor(Color.RED);

		canvas.drawColor(Color.argb(90, 30, 30, 30));
		paint.setAntiAlias(true);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		float radius = (float) (GlobalApp.screenWidth * .10);
		canvas.drawCircle(point.x, point.y, radius, paint);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		Canvas c = null;
		for (int i = 1; i <= 2; i++) {
			try {
				c = holder.lockCanvas(null);
				synchronized (holder) {
					this.onDraw(c, new Point(0, 0));
				}
			} finally {
				// do this in a finally so that if an exception is thrown
				// during the above, we don't leave the Surface in an
				// inconsistent state
				if (c != null) {
					holder.unlockCanvasAndPost(c);
				}
			}
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceRedrawNeeded(SurfaceHolder holder) {
		// TODO Auto-generated method stub

	}

}
