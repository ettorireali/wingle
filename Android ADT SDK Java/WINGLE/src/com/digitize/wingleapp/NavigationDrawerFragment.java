package com.digitize.wingleapp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.imagepart.ImageLoader;
import com.digitize.wingleapp.pojo.CustomDrawerAdapter;
import com.digitize.wingleapp.pojo.DrawerItem;
import com.digitize.wingleapp.pojo.ProfileData;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Fragment used for managing interactions for and presentation of a navigation
 * drawer. See the <a href=
 * "https://developer.android.com/design/patterns/navigation-drawer.html#Interaction"
 * > design guidelines</a> for a complete explanation of the behaviors
 * implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

	/**
	 * Remember the position of the selected item.
	 */
	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	/**
	 * Per the design guidelines, you should show the drawer on launch until the
	 * user manually expands it. This shared preference tracks this.
	 */
	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

	/**
	 * A pointer to the current callbacks instance (the Activity).
	 */
	private NavigationDrawerCallbacks mCallbacks;

	/**
	 * Helper component that ties the action bar to the navigation drawer.
	 */
	private ActionBarDrawerToggle mDrawerToggle;

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	private TextView txtUsername;
	private TextView txtUseremail;
	private ImageView imgProfilePic;
	private View mFragmentContainerView;
	ImageLoader imgLoader;
	WingleDB wingleDB;
	String galleryImageUrl = null;
	private int mCurrentSelectedPosition = 1;
	private boolean mFromSavedInstanceState;
	private boolean mUserLearnedDrawer;
	ArrayList<ProfileData> userList;
	CustomDrawerAdapter adapter = null;
	List<DrawerItem> dataList = null;
	LinearLayout linearFollowing, linearFollower, linearBlocked;
	TextView txtFollowing, txtFollower, txtBlocked;
	AQuery aQuery;
	String imagePath;
	JSONArray followerArray;
	JSONArray followingArray;
	JSONArray blockedArray;

	public void prepareNavBar() {
		dataList.add(new DrawerItem("Profile", R.drawable.nav_icon_profile));
		dataList.add(new DrawerItem("Nearby", R.drawable.nav_icon_nearby));
		dataList.add(new DrawerItem("NearbyFriends", R.drawable.nav_icon_nearby));
		dataList.add(new DrawerItem("Random Places", R.drawable.nav_icon_nearby));
		dataList.add(new DrawerItem("My Places", R.drawable.nav_bites));
		dataList.add(new DrawerItem("NewsFeeds", R.drawable.nav_bites));
		dataList.add(new DrawerItem("Settings", R.drawable.nav_icon_settings));
		dataList.add(new DrawerItem("Logout", R.drawable.logout_icon));
	}

	public void setFollowers(JSONArray jsonFollowers) {
		this.followerArray = jsonFollowers;
		txtFollower.setText("" + followerArray.length());
	}

	public void setFollowing(JSONArray jsonFollowing) {
		this.followingArray = jsonFollowing;
		txtFollowing.setText("" + followingArray.length());
	}

	public void setBlocked(JSONArray jsonBlocked) {
		this.blockedArray = jsonBlocked;
		txtBlocked.setText("" + blockedArray.length());
	}

	public NavigationDrawerFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Read in the flag indicating whether or not the user has demonstrated
		// awareness of the
		// drawer. See PREF_USER_LEARNED_DRAWER for details.
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState
					.getInt(STATE_SELECTED_POSITION);
			mFromSavedInstanceState = true;
		}
		// Select either the default item (0) or the last selected item.

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of
		// actions in the action bar.
		setHasOptionsMenu(true);
		aQuery = new AQuery(getActivity());
		wingleDB = new WingleDB(getActivity());
		wingleDB.open();
		userList = wingleDB.getUserData(GlobalApp
				.getUidFromSharedPreference(getActivity()));
		wingleDB.close();

		Iterator<ProfileData> profileDataIterator = userList.iterator();
		ProfileData profileData;
		while (profileDataIterator.hasNext()) {
			profileData = profileDataIterator.next();
			txtUsername.setText(profileData.getFirstName() + " "
					+ profileData.getLastName());
			txtUseremail.setText(profileData.getEmail());
			try {
				displayImage(profileData.getImagePath());
			} catch (Exception e) {
				e.printStackTrace();
				imgProfilePic
						.setImageResource(R.drawable.com_facebook_profile_picture_blank_square);
			}
		}

		dataList = new ArrayList<DrawerItem>();
		prepareNavBar();
		adapter = new CustomDrawerAdapter(getActivity(),
				R.layout.nav_cell_item, dataList);
		mDrawerListView.setAdapter(adapter);
		selectItem(mCurrentSelectedPosition);
		imgLoader = new ImageLoader(getActivity());
		getActivity().supportInvalidateOptionsMenu();
		linearBlocked.setOnClickListener(blockedClickListener);
		linearFollower.setOnClickListener(followerClickListener);
		linearFollowing.setOnClickListener(followingClickListener);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View parentLayout = (View) inflater.inflate(
				R.layout.fragment_navigation_drawer, container, false);
		mDrawerListView = (ListView) parentLayout
				.findViewById(R.id.lst_nav_options);
		txtUsername = (TextView) parentLayout
				.findViewById(R.id.txt_main_username);
		txtUseremail = (TextView) parentLayout
				.findViewById(R.id.txt_main_email);
		linearFollowing = (LinearLayout) parentLayout
				.findViewById(R.id.linear_following);
		linearFollower = (LinearLayout) parentLayout
				.findViewById(R.id.linear_follower);
		linearBlocked = (LinearLayout) parentLayout
				.findViewById(R.id.linear_blocked);
		txtFollower = (TextView) parentLayout
				.findViewById(R.id.txt_nav_followers);
		txtFollowing = (TextView) parentLayout
				.findViewById(R.id.txt_nav_following);
		txtBlocked = (TextView) parentLayout.findViewById(R.id.txt_nav_blocked);
		imgProfilePic = (ImageView) parentLayout
				.findViewById(R.id.img_profile_pic);
		mDrawerListView
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						selectItem(position);
					}
				});
		mDrawerListView.setAdapter(new ArrayAdapter<String>(getActionBar()
				.getThemedContext(), android.R.layout.simple_list_item_1,
				android.R.id.text1, new String[] {
						getString(R.string.title_section1),
						getString(R.string.title_section2),
						getString(R.string.title_section3), }));
		mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
		return parentLayout;
	}

	public OnClickListener blockedClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try {
				Intent peopleFragIntent = new Intent(getActivity(),
						SeePeopleFragmentActivity.class);
				peopleFragIntent
						.putExtra("member_ids", blockedArray.toString());
				startActivity(peopleFragIntent);
				getActivity().overridePendingTransition(
						android.R.anim.slide_in_left,
						android.R.anim.slide_out_right);

			} catch (Exception e) {
				e.printStackTrace();
			}
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
	};
	public OnClickListener followerClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try {
				Intent peopleFragIntent = new Intent(getActivity(),
						SeePeopleFragmentActivity.class);
				peopleFragIntent.putExtra("member_ids",
						followerArray.toString());
				startActivity(peopleFragIntent);
				getActivity().overridePendingTransition(
						android.R.anim.slide_in_left,
						android.R.anim.slide_out_right);

			} catch (Exception e) {
				e.printStackTrace();
			}
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
	};
	public OnClickListener followingClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try {
				Intent peopleFragIntent = new Intent(getActivity(),
						SeePeopleFragmentActivity.class);
				peopleFragIntent.putExtra("member_ids",
						followingArray.toString());
				startActivity(peopleFragIntent);
				getActivity().overridePendingTransition(
						android.R.anim.slide_in_left,
						android.R.anim.slide_out_right);

			} catch (Exception e) {
				e.printStackTrace();
			}
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
	};

	public boolean isDrawerOpen() {
		return mDrawerLayout != null
				&& mDrawerLayout.isDrawerOpen(mFragmentContainerView);
	}

	/**
	 * Users of this fragment must call this method to set up the navigation
	 * drawer interactions.
	 * 
	 * @param fragmentId
	 *            The android:id of this fragment in its activity's layout.
	 * @param drawerLayout
	 *            The DrawerLayout containing this fragment's UI.
	 */
	public void setUp(int fragmentId, DrawerLayout drawerLayout) {
		mFragmentContainerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;

		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		// set up the drawer's list view with items and click listener

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the navigation drawer and the action bar app icon.
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.navigation_drawer_open, /*
										 * "open drawer" description for
										 * accessibility
										 */
		R.string.navigation_drawer_close /*
										 * "close drawer" description for
										 * accessibility
										 */
		) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				if (!isAdded()) {
					return;
				}
				// getActivity().supportInvalidateOptionsMenu(); // calls
				// onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if (!isAdded()) {

					return;
				}

				if (!mUserLearnedDrawer) {
					// The user manually opened the drawer; store this flag to
					// prevent auto-showing
					// the navigation drawer automatically in the future.
					mUserLearnedDrawer = true;
					SharedPreferences sp = PreferenceManager
							.getDefaultSharedPreferences(getActivity());
					sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true)
							.apply();
				}

				// getActivity().supportInvalidateOptionsMenu(); // calls
				// onPrepareOptionsMenu()
			}
		};

		// If the user hasn't 'learned' about the drawer, open it to introduce
		// them to the drawer,
		// per the navigation drawer design guidelines.
		if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
			mDrawerLayout.openDrawer(mFragmentContainerView);
		}

		// Defer code dependent on restoration of previous instance state.
		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});

		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	private void selectItem(int position) {
		mCurrentSelectedPosition = position;
		if (mDrawerListView != null) {
			mDrawerListView.setItemChecked(position, true);
		}
		if (mDrawerLayout != null) {
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerItemSelected(position);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallbacks = (NavigationDrawerCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		wingleDB.open();
		imagePath = wingleDB.getProfilePic(GlobalApp
				.getUidFromSharedPreference(getActivity()));
		wingleDB.close();
		displayImage(imagePath);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// If the drawer is open, show the global app actions in the action bar.
		// See also
		// showGlobalContextActionBar, which controls the top-left area of the
		// action bar.
		if (mDrawerLayout != null && isDrawerOpen()) {
			inflater.inflate(R.menu.global, menu);
			// showGlobalContextActionBar();
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private ActionBar getActionBar() {
		return ((ActionBarActivity) getActivity()).getSupportActionBar();
	}

	/**
	 * Callbacks interface that all activities using this fragment must
	 * implement.
	 */
	public static interface NavigationDrawerCallbacks {
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerItemSelected(int position);
	}

	public void displayImage(String url) {

		aQuery.id(imgProfilePic).image(url, true, true, (int) (200),
				R.drawable.noimage, new BitmapAjaxCallback() {
					@Override
					protected void callback(String url, ImageView iv,
							Bitmap bm, AjaxStatus status) {
						// TODO Auto-generated method stub
						super.callback(url, iv, bm, status);
						Log.w("fired", url + " status : " + status.getCode());
					}
				});
	}

}
