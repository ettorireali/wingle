package com.digitize.wingleapp;

import java.util.ArrayList;

import com.digitize.wingleapp.SeePeopleAdapter.ProfileCell;
import com.digitize.wingleapp.pojo.NewsFeedData;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class NewsFeedAdapter extends BaseAdapter {

	ArrayList<NewsFeedData> newsFeedList;
	LayoutInflater layoutinfalter;
	Context context;

	public void setNewsData(ArrayList<NewsFeedData> newsList) {
		this.newsFeedList = newsList;
	}

	public NewsFeedAdapter(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.layoutinfalter = LayoutInflater.from(context);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return newsFeedList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return newsFeedList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		NewsCell newsCell;
		NewsFeedData newsFeedData = newsFeedList.get(position);
		if (convertView == null) {
			convertView = layoutinfalter.inflate(
					R.layout.news_feeds_single_item, null);
			newsCell = new NewsCell();
			newsCell.txtHeadline = (TextView) convertView
					.findViewById(R.id.txt_headline);
			newsCell.txtMessage = (TextView) convertView
					.findViewById(R.id.txt_message);

			convertView.setTag(newsCell);
		} else {
			newsCell = (NewsCell) convertView.getTag();
		}
		newsCell.txtHeadline.setText(newsFeedData.getHeadline());
		newsCell.txtMessage.setText(newsFeedData.getMessage());

		return convertView;
	}

	class NewsCell {
		TextView txtHeadline;
		TextView txtMessage;
	}
}
