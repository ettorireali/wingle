package com.digitize.wingleapp;

import java.util.ArrayList;

import com.digitize.wingleapp.pojo.PlaceBookVO;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyPlaceAdapter extends BaseAdapter {

	Context context;
	ArrayList<PlaceBookVO> placeDataList;
	LayoutInflater layoutinfalter;

	public void setPlaceData(ArrayList<PlaceBookVO> placeData) {
		this.placeDataList = placeData;
	}

	public MyPlaceAdapter(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.layoutinfalter = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return placeDataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return placeDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		PlaceCell placeCell;
		PlaceBookVO placebookVO = placeDataList.get(position);
		if (convertView == null) {
			convertView = layoutinfalter.inflate(R.layout.myplaces_cell_item,
					null);
			placeCell = new PlaceCell();
			placeCell.txtPlaceName = (TextView) convertView
					.findViewById(R.id.txt_place_name);
			placeCell.txtPlaceDesc = (TextView) convertView
					.findViewById(R.id.txt_place_latlon);

			convertView.setTag(placeCell);
		} else {
			placeCell = (PlaceCell) convertView.getTag();
		}
		placeCell.txtPlaceName.setText(placebookVO.getPlaceInfo());
		double lat = placebookVO.getPos().latitude;
		double lon = placebookVO.getPos().longitude;
		placeCell.txtPlaceDesc.setText("Latitude : " + lat + " Longitude : "
				+ lon);

		return convertView;
	}

	class PlaceCell {
		TextView txtPlaceName;
		TextView txtPlaceDesc;
	}

}
