/* 
 * Copyright (C) 2007 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.vcity.androidim.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import org.jivesoftware.smack.SmackAndroid;
import org.jivesoftware.smack.SmackException.NotConnectedException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

import com.digitize.wingleapp.ChatCache;
import com.digitize.wingleapp.ChatFragment;
import com.digitize.wingleapp.GlobalApp;
import com.digitize.wingleapp.MainActivity;
import com.digitize.wingleapp.R;
import com.digitize.wingleapp.SeePeopleFragmentActivity;
import com.digitize.wingleapp.db.WingleDB;
import com.digitize.wingleapp.pojo.ProfileData;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import at.vcity.androidim.communication.SocketOperator;
import at.vcity.androidim.interfaces.IAppManager;
import at.vcity.androidim.interfaces.ISocketOperator;
import at.vcity.androidim.interfaces.IUpdateData;
import at.vcity.androidim.tools.FriendController;
import at.vcity.androidim.tools.MessageController;
import at.vcity.androidim.tools.XMLHandler;
import at.vcity.androidim.types.FriendInfo;
import at.vcity.androidim.types.MessageInfo;

/**
 * This is an example of implementing an application service that runs locally
 * in the same process as the application. The {@link LocalServiceController}
 * and {@link LocalServiceBinding} classes show how to interact with the
 * service.
 * 
 * <p>
 * Notice the use of the {@link NotificationManager} when interesting things
 * happen in the service. This is generally how background services should
 * interact with the user, rather than doing something more disruptive such as
 * calling startActivity().
 */
public class IMService extends XMPPChatDemoActivity {
	// private NotificationManager mNM;
	WingleDB wingleDB = null;
	public static final String TAKE_MESSAGE = "Take_Message";
	public static final String FRIEND_LIST_UPDATED = "Take Friend List";
	public static final String MESSAGE_LIST_UPDATED = "Take Message List";
	public ConnectivityManager conManager = null;
	private final int UPDATE_TIME_PERIOD = 15000;
	// private static final int LISTENING_PORT_NO = 8956;
	public static String rawFriendList = new String();
	public static String rawMessageList = new String();
	private boolean notify = false;

	ISocketOperator socketOperator = new SocketOperator(this);

	private final IBinder mBinder = new IMBinder();
	private String username;
	private String password;
	private String userKey;
	private boolean authenticatedUser = false;
	// timer to take the updated data from server
	private Timer timer;

	private NotificationManager mNM;

	private IAppManager appManager;

	public class IMBinder extends Binder {
		public IAppManager getService() {
			return IMService.this;
		}
	}

	@Override
	public void onCreate() {
		SmackAndroid.init(getApplicationContext());
		wingleDB = new WingleDB(this);
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		// Display a notification about us starting. We put an icon in the
		// status bar.
		// showNotification();
		conManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

		// Timer is used to take the friendList info every UPDATE_TIME_PERIOD;
		timer = new Timer();

		connect();

		/*
		 * Thread thread = new Thread() {
		 * 
		 * @Override public void run() {
		 * //socketOperator.startListening(LISTENING_PORT_NO); int tryCount = 0;
		 * while (socketOperator.startListening(GlobalApp.PORT_TO_LISTEN) == 0 )
		 * { tryCount++; if (tryCount > 10) { // if it can't listen a port after
		 * trying 10 times, give up... break; }
		 * 
		 * } } }; thread.start();
		 */
	}

	/*
	 * @Override public void onDestroy() { // Cancel the persistent
	 * notification. mNM.cancel(R.string.local_service_started);
	 * 
	 * // Tell the user we stopped. Toast.makeText(this,
	 * R.string.local_service_stopped, Toast.LENGTH_SHORT).show(); }
	 */

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public void sendMessage(MessageInfo msgInfo) {
		Bundle bundle = new Bundle();
		bundle.putString("recipient", msgInfo.getRecipientEmail());
		bundle.putString("receipientId", msgInfo.getRecipientId());
		bundle.putString("sender", msgInfo.getSenderEmail());
		bundle.putString("senderId", msgInfo.getSenderId());
		bundle.putString("senderPwd", msgInfo.getSenderPwd());
		bundle.putString("message", msgInfo.getMessage());
		// return socketOperator.sendHttpRequest(bundle);
		sendMessage(msgInfo.getRecipientId(), msgInfo.getMessage());
	}

	public void messageReceived(String userId, String message) {

		storeMessage(userId,message);
		Intent i = new Intent(TAKE_MESSAGE);
		i.putExtra(MessageInfo.USERID, userId);
		i.putExtra(MessageInfo.MESSAGETEXT, message);
		sendBroadcast(i);
		if(notify){
			ProfileData senderProfileData = getProfileForUsername(userId);
			if(senderProfileData == null){
				// New User TODO
			}
			showNotification(userId,message,senderProfileData);//can be dialog
			ChatCache.incrementUnreadMessages(userId);
		}
		
		
		Log.i("TAKE_MESSAGE broadcast sent by im service", "");
	}

	private ProfileData getProfileForUsername(String username) {
		// TODO Auto-generated method stub
		ProfileData profileData = GlobalApp.getProfileMap().get(username);
		if(profileData == null){
			profileData = wingleDB.getUserDataForUserName(username);
			GlobalApp.getProfileMap().put(username, profileData);
		}
		
		return profileData;
	}
	
	private void showNotification(String username, String msg, ProfileData senderProfileData) {
		// Set the icon, scrolling text and timestamp
		int icon = R.drawable.ic_launcher;//
		Context context = getApplicationContext();
		Log.w("gennoti", "notification");
		long when = System.currentTimeMillis();
		Notification notification = new Notification(icon, msg, when);
		String title = context.getString(R.string.app_name);
		Intent notificationIntent = new Intent(context,
				SeePeopleFragmentActivity.class);
		notificationIntent.putExtra("from_random_places", senderProfileData);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, msg, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND;
		mNM.notify(0, notification);

	}
	private String getAuthenticateUserParams(String usernameText,
			String passwordText) {
		String params = "username="
				+ URLEncoder.encode(usernameText)
				+ "&password="
				+ URLEncoder.encode(passwordText)
				+ "&action="
				+ URLEncoder.encode("authenticateUser")
				+ "&port="
				+ URLEncoder.encode(Integer.toString(socketOperator
						.getListeningPort())) + "&";

		return params;
	}

	public void setUserKey(String value) {
		this.userKey = value;
	}

	public boolean isNetworkConnected() {
		return conManager.getActiveNetworkInfo().isConnected();
	}

	public boolean isUserAuthenticated() {
		return authenticatedUser;
	}

	public String getLastRawFriendList() {
		return this.rawFriendList;
	}

	@Override
	public void onDestroy() {
		Log.i("IMService is being destroyed", "...");
		super.onDestroy();
	}

	public void exit() {
		timer.cancel();
		socketOperator.exit();
		socketOperator = null;
		try {
			connection.disconnect();
		} catch (NotConnectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.stopSelf();
	}

	private void parseFriendInfo(String xml) {
		try {
			SAXParser sp = SAXParserFactory.newInstance().newSAXParser();
			sp.parse(new ByteArrayInputStream(xml.getBytes()), new XMLHandler(
					IMService.this));
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void parseMessageInfo(String xml) {
		try {
			SAXParser sp = SAXParserFactory.newInstance().newSAXParser();
			sp.parse(new ByteArrayInputStream(xml.getBytes()), new XMLHandler(
					IMService.this));
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updateData(MessageInfo[] messages, FriendInfo[] friends,
			FriendInfo[] unApprovedFriends, String userKey) {
		this.setUserKey(userKey);
		// FriendController.
		MessageController.setMessagesInfo(messages);
		// Log.i("MESSAGEIMSERVICE","messages.length="+messages.length);

		int i = 0;
		while (i < messages.length) {
			// messageReceived(messages[i].userid,messages[i].messagetext);
			// appManager.messageReceived(messages[i].userid,messages[i].messagetext);
			i++;
		}

		FriendController.setFriendsInfo(friends);
		FriendController.setUnapprovedFriendsInfo(unApprovedFriends);

	}

	@Override
	public String authenticateUser(String usernameText, String passwordText) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void storeMessage(String sender,String msg){
	ArrayList<Object> userChatHist;
	if((userChatHist = ChatCache.getChatHistoryForUsername(sender))==null){
		wingleDB.open();
		userChatHist = wingleDB.getChatHistoryForSender(sender);
		wingleDB.close();
		ChatCache.putChatHistoryForUsername(sender, userChatHist);
		Log.i("chat history", userChatHist.toString());
	}
	MessageInfo msgInfo = new MessageInfo();
	msgInfo.setMessage(msg);
	msgInfo.setSenderId(sender);
	msgInfo.setRecipientId(GlobalApp.getUserNameFromSharedPreference(getApplicationContext()));
	userChatHist.add(msgInfo);
	wingleDB.open();
	msgInfo.setTimeinmillis(System.currentTimeMillis());
	wingleDB.addChat(msgInfo);
	wingleDB.close();
	}

	@Override
	public void setNotifyFlag(boolean b) {
		// TODO Auto-generated method stub
		notify = b;
		
	}
	
	

}