package at.vcity.androidim.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.harmony.javax.security.sasl.SaslException;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.util.StringUtils;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import at.vcity.androidim.interfaces.IAppManager;
import at.vcity.androidim.interfaces.IUpdateData;
import at.vcity.androidim.types.FriendInfo;
import at.vcity.androidim.types.MessageInfo;

import com.digitize.wingleapp.GlobalApp;

public abstract class XMPPChatDemoActivity extends Service implements
		IAppManager, IUpdateData {
	public static final String HOST = "198.50.237.65";
	public static final int PORT = 5222;
	public static final String SERVICE = "server-tq8837fh";
	public static final String TAKE_MESSAGE = "Take_Message";
	public String userName;
	public String pwd;

	protected XMPPConnection connection;
	private ArrayList<String> messages = new ArrayList<String>();
	private Handler mHandler = new Handler();

	public void sendMessage(String to, String text) {
		Log.i("XMPPChatDemoActivity", "Sending text " + text + " to " + to);
		Message msg = new Message(to + "@" + SERVICE, Message.Type.chat);
		msg.setBody(text);
		if (connection != null) {
			try {
				connection.sendPacket(msg);
			} catch (NotConnectedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Called by Settings dialog when a connection is establised with the XMPP
	 * server
	 * 
	 * @param connection
	 */
	public void setConnection() {
		if (connection != null) {
			// Add a packet listener to get messages sent to us
			PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
			connection.addPacketListener(new PacketListener() {
				@Override
				public void processPacket(Packet packet) {
					final Message message = (Message) packet;
					if (message.getBody() != null) {
						final String fromName = StringUtils
								.parseBareAddress(message.getFrom());
						Log.i("XMPPChatDemoActivity", "Text Recieved "
								+ message.getBody() + " from " + fromName);
						messages.add(fromName + ":");
						messages.add(message.getBody());
						// Add the incoming message to the list view
						mHandler.post(new Runnable() {
							@Override
							public void run() {
								messageReceived(fromName.replace("@"+SERVICE,""), message.getBody());
							}

						});
					}
				}
			}, filter);
		}
	}

	protected void disconnect() {
		try {
			if (connection != null)
				connection.disconnect();
		} catch (Exception e) {

		}
	}

	public void connect() {

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				// Create a connection
				ConnectionConfiguration connConfig = new ConnectionConfiguration(
						HOST, PORT, SERVICE);
				connConfig.setDebuggerEnabled(true);
				connConfig.setSecurityMode(SecurityMode.disabled);
				connConfig.setReconnectionAllowed(true);
				connConfig.setRosterLoadedAtLogin(true);
				connConfig.setSendPresence(true);
				connection = new XMPPTCPConnection(connConfig);
				connection.setPacketReplyTimeout(15000);
				try {
					connection.connect();
					Log.i("XMPPChatDemoActivity",
							"Connected to " + connection.getHost());
				} catch (XMPPException ex) {
					Log.e("XMPPChatDemoActivity", "Failed to connect to "
							+ connection.getHost());
					Log.e("XMPPChatDemoActivity", ex.toString());
					setConnection();
				} catch (SmackException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					// SASLAuthentication.supportSASLMechanism("PLAIN", 0);
					userName = GlobalApp
							.getUserNameFromSharedPreference(getApplicationContext());
					pwd = GlobalApp
							.getPasswordFromSharedPreference(getApplicationContext());
					connection.login(userName, pwd);
					Log.i("XMPPChatDemoActivity",
							"Logged in as " + connection.getUser());

					// Set the status to available
					Presence presence = new Presence(Presence.Type.available);
					connection.sendPacket(presence);
					setConnection();

					Roster roster = connection.getRoster();
					Collection<RosterEntry> entries = roster.getEntries();
					for (RosterEntry entry : entries) {
						Log.d("XMPPChatDemoActivity",
								"--------------------------------------");
						Log.d("XMPPChatDemoActivity", "RosterEntry " + entry);
						Log.d("XMPPChatDemoActivity",
								"User: " + entry.getUser());
						Log.d("XMPPChatDemoActivity",
								"Name: " + entry.getName());
						Log.d("XMPPChatDemoActivity",
								"Status: " + entry.getStatus());
						Log.d("XMPPChatDemoActivity",
								"Type: " + entry.getType());
						Presence entryPresence = roster.getPresence(entry
								.getUser());

						Log.d("XMPPChatDemoActivity", "Presence Status: "
								+ entryPresence.getStatus());
						Log.d("XMPPChatDemoActivity", "Presence Type: "
								+ entryPresence.getType());
						Presence.Type type = entryPresence.getType();
						if (type == Presence.Type.available)
							Log.d("XMPPChatDemoActivity", "Presence AVIALABLE");
						Log.d("XMPPChatDemoActivity", "Presence : "
								+ entryPresence);

					}
				} catch (XMPPException ex) {
					Log.e("XMPPChatDemoActivity", "Failed to log in as "
							+ userName);
					Log.e("XMPPChatDemoActivity", ex.toString());
					setConnection();
				} catch (SaslException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SmackException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		t.start();

	}

	@Override
	public abstract IBinder onBind(Intent arg0);

	@Override
	public abstract void updateData(MessageInfo[] messages,
			FriendInfo[] friends, FriendInfo[] unApprovedFriends, String userKey);

	@Override
	public abstract void sendMessage(MessageInfo msgInfo);

	@Override
	public abstract String authenticateUser(String usernameText,
			String passwordText);

	@Override
	public abstract boolean isNetworkConnected();

	@Override
	public abstract boolean isUserAuthenticated();

	@Override
	public abstract String getLastRawFriendList();

	@Override
	public abstract void exit();

	@Override
	public abstract void messageReceived(String fromName, String body);
}