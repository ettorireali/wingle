package at.vcity.androidim.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import android.os.Bundle;
import android.util.Log;
import at.vcity.androidim.interfaces.IAppManager;
import at.vcity.androidim.interfaces.ISocketOperator;

public class SocketOperator implements ISocketOperator {
	private int listeningPort = 0;

	private HashMap<InetAddress, Socket> sockets = new HashMap<InetAddress, Socket>();

	private ServerSocket serverSocket = null;

	private boolean listening;

	private IAppManager appManager;

	private class ReceiveConnection extends Thread {
		Socket clientSocket = null;

		public ReceiveConnection(Socket socket) {
			this.clientSocket = socket;
			SocketOperator.this.sockets.put(socket.getInetAddress(), socket);
			System.out.println("this mobile id address:--"
					+ socket.getInetAddress());
		}

		@Override
		public void run() {
			try {
				// PrintWriter out = new
				// PrintWriter(clientSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						clientSocket.getInputStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {

					if (inputLine.equals("exit") == false) {
						appManager.messageReceived("", inputLine);
					} else {
						clientSocket.shutdownInput();
						clientSocket.shutdownOutput();
						clientSocket.close();
						SocketOperator.this.sockets.remove(clientSocket
								.getInetAddress());
					}
				}

			} catch (IOException e) {
				Log.e("ReceiveConnection.run: when receiving connection ", "");
			}
		}
	}

	public SocketOperator(IAppManager appManager) {
		this.appManager = appManager;
	}

	public String sendHttpRequest(final Bundle params) {
		new Thread() {
			public void run() {
				try {
					Socket packet = new Socket(params.getString("ip"),
							params.getInt("port"));
					OutputStream outStream = packet.getOutputStream();
					outStream.write(params.getString("message").getBytes());
					outStream.flush();
					outStream.close();
					packet.close();
				} catch (Exception ex) {
					Log.w("socket_err", ex.toString());
				}
			}
		}.start();

		return null;
	}

	public int startListening(int portNo) {
		listening = true;
		Log.w("service", String.valueOf(portNo));
		try {
			serverSocket = new ServerSocket(portNo);
			this.listeningPort = portNo;
		} catch (IOException e) {

			// e.printStackTrace();
			this.listeningPort = 0;
			return 0;
		}
		ReceiveConnection receiveConnection = null;
		while (listening) {
			try {
				receiveConnection = new ReceiveConnection(serverSocket.accept());
				receiveConnection.start();

			} catch (IOException e) {
				// e.printStackTrace();
				return 2;
			}
		}

		try {
			serverSocket.close();
		} catch (IOException e) {
			Log.e("Exception server socket",
					"Exception when closing server socket");
			return 3;
		}

		return 1;
	}

	public void stopListening() {
		this.listening = false;
	}

	private Socket getSocket(InetAddress addr, int portNo) {
		Socket socket = null;
		if (sockets.containsKey(addr) == true) {
			socket = sockets.get(addr);
			// check the status of the socket
			if (socket.isConnected() == false
					|| socket.isInputShutdown() == true
					|| socket.isOutputShutdown() == true
					|| socket.getPort() != portNo) {
				// if socket is not suitable, then create a new socket
				sockets.remove(addr);
				try {
					socket.shutdownInput();
					socket.shutdownOutput();
					socket.close();
					socket = new Socket(addr, portNo);
					sockets.put(addr, socket);
				} catch (IOException e) {
					Log.e("getSocket: when closing and removing", "");
				}
			}
		} else {
			try {
				socket = new Socket(addr, portNo);
				sockets.put(addr, socket);
			} catch (IOException e) {
				Log.e("getSocket: when creating", "");
			}
		}
		return socket;
	}

	public void exit() {
		for (Iterator<Socket> iterator = sockets.values().iterator(); iterator
				.hasNext();) {
			Socket socket = (Socket) iterator.next();
			try {
				socket.shutdownInput();
				socket.shutdownOutput();
				socket.close();
			} catch (IOException e) {
			}
		}

		sockets.clear();
		this.stopListening();
		appManager = null;
		// timer.cancel();
	}

	public int getListeningPort() {

		return this.listeningPort;
	}

}
