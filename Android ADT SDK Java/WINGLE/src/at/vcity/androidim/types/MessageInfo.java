package at.vcity.androidim.types;

import java.util.Calendar;

public class MessageInfo {

	public static final String MESSAGE_LIST = "messageList";
	public static final String USERID = "from";
	public static final String SENDT = "sendt";
	public static final String MESSAGETEXT = "text";

	private String recipientEmail;
	private String recipientId;
	private String senderEmail;
	private String senderId;
	private String senderPwd;
	private String message;
	private long timeinmillis;

	public long getTimeinmillis() {
		return timeinmillis;
	}

	public void setTimeinmillis(long timeinmillis) {
		this.timeinmillis = timeinmillis;
	}

	public String getRecipientEmail() {
		return recipientEmail;
	}

	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSenderPwd() {
		return senderPwd;
	}

	public void setSenderPwd(String senderPwd) {
		this.senderPwd = senderPwd;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
