package at.vcity.androidim.interfaces;

import at.vcity.androidim.types.MessageInfo;

public interface IAppManager {

	public void sendMessage(MessageInfo msgInfo);

	public String authenticateUser(String usernameText, String passwordText);

	public void messageReceived(String username, String message);

	// public void setUserKey(String value);
	public boolean isNetworkConnected();

	public boolean isUserAuthenticated();

	public String getLastRawFriendList();

	public void exit();

	public void setNotifyFlag(boolean b);

}
