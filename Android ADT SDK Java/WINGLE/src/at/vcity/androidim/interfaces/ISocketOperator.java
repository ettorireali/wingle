package at.vcity.androidim.interfaces;

import android.os.Bundle;

public interface ISocketOperator {

	public String sendHttpRequest(Bundle params);

	public int startListening(int port);

	public void stopListening();

	public void exit();

	public int getListeningPort();

}
