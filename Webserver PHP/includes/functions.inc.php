<?php

/*
 * General funtion will be here.
 */

/**
 * Redirect to another pages within website
 * @param string $location
 * @param string $msg
 * @param string $class
 */
function redirect($location = 'index.php', $msg = '', $class = 'success')
{
    setSessionMsg($msg, $class);

    ob_clean();
    header('Location:' . $location);
    exit;
}

/**
 * set message in session
 * @param string $msg
 * @param string $class
 * @return boolean
 */
function setSessionMsg($msg = '', $class)
{
    if (is_array($msg))
    {
        $msg = implode('</li>' . "\n" . '<li>', $msg);
        $msg = '<ol><li>' . $msg . '</li><ol>';
    }
    if (!empty($msg))
        $_SESSION['message'] = $msg;

    if (!empty($class))
        $_SESSION['message_class'] = $class;

    if (!empty($_SESSION['hash']))
        unset($_SESSION['hash']);
    return true;
}

function isEmail($email='')
{
    return preg_match('/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/',$email);
}

function generateHash($flag=0)
{
    if(!$flag)
        $hash = substr(md5(md5(time().rand().$_SERVER['REMOTE_ADDR'].microtime()).time()),0,16);
    else
        $hash=substr(md5(md5(time().rand().$_SERVER['REMOTE_ADDR'].microtime()).time()),0,32);

    return $hash;
}

//Generate Random string
function genRandomString($length=10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';


    for($p = 0; $p < $length; $p++)
    {
        $string .= $characters[mt_rand(0,strlen($characters) - 1)];
    }

    return $string;
}

/* Creates a directory according to given id and parent dir path */
function createDir($id,$parent)
{
    $nd_dirlimit = 100;
    $mod = ceil($id / $nd_dirlimit) - 1;

    $dirs = $mod * $nd_dirlimit + 1;
    $dire = $dirs + $nd_dirlimit - 1;

    $tempDir = $dirs.'_'.$dire;

    if(!is_dir($parent.'/'.$tempDir))
        mkdir($parent.'/'.$tempDir,0775);

    return $tempDir;
}

/**
 * TO Calculate Age From Birthdate.
 * Date eg. (yyyy-mm-dd).
 */
function age_from_dob($dob) {

	list($y,$m,$d) = explode('-', $dob);
	if (($m = (date('m') - $m)) < 0) {
		$y++;
	} elseif ($m == 0 && date('d') - $d < 0) {
		$y++;
	}
	return date('Y') - $y;
}

/**
 * To Convert String(eg. Bytes To KB or Bytes To MB And Etc..)
 * @param $size 
 * @param $precision
 * @return unknown_type
 */
function formatBytes($size, $precision = 2)
{
	$base = log($size) / log(1024);
	$suffixes = array('Bytes', 'KB', 'MB', 'GB', 'TB');

	return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

/**
 * Remove directory and all its content recursively
 * @param <string> $dir
 * @return <boolean>
 */
function rrmdir($dir)
{
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file)
    {
        // if sub file is directory then call same function again.
        if (is_dir($dir . DIRECTORY_SEPARATOR . $file))
            rrmdir($dir . DIRECTORY_SEPARATOR . $file);

        // Unlink file
        unlink($dir . DIRECTORY_SEPARATOR . $file);
    }
    return rmdir($dir);
}
function getAdminConfig($db, $query)
{
	$bas_data = $db->getData($query);
	
	$head_data = array();
	
	foreach($bas_data as $bd):
	$head_data[$bd['slug']] = $bd['description'];
	endforeach;
	return $head_data;
}
