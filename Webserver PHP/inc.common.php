<?php
$dashboard_name = '/admin';
define('BASE_DIR', dirname(__FILE__));
// Define constants for urls
date_default_timezone_set('Asia/Kolkata');
define('BASE_URL_FRONT', 'http://' . $_SERVER['HTTP_HOST'] . str_replace('admin', '', dirname($_SERVER['PHP_SELF'])));
define('BASE_URL', 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']));
define('HOME_URL', str_replace($dashboard_name, '', preg_replace('/\/$/', '', BASE_URL)));
define('DASHBOARD_URL', HOME_URL . $dashboard_name);
// Define constants for Absolute path
define('DS', DIRECTORY_SEPARATOR);
define('IMAGE_UPLOAD_PATH',BASE_DIR.DS.'assets');
define('IMAGE_URL', HOME_URL . DS.'assets');
define('INCLUDE_DIR', BASE_DIR . DS . 'includes');
define('LIB_DIR', BASE_DIR . DS.'lib');
define('FB_APP_ID', '315150331969558');
$timezone = "Asia/Calcutta";
date_default_timezone_set($timezone);
require_once INCLUDE_DIR . DS . 'functions.inc.php';
require_once LIB_DIR . DS . 'mysql.action.php';
require_once LIB_DIR . DS . 'class.upload.php';
$db = new mysql_action();