<?php
require_once 'config/connection.php';
require_once 'inc.common.php';
$option = $_REQUEST['option'];
if($option == 'register')
{
	if($_POST['username'] && $_POST['email'] && $_POST['password'])
	{
		$data['firstname'] = $_POST['firstname'];
		$data['lastname'] = $_POST['lastname'];
		$data['username'] = $_POST['username'];
		$data['dob'] = date("Y/m/d",strtotime($_POST['dob']));
		$data['email'] = $_POST['email'];
		$data['gender'] = $_POST['gender'];
		$data['password'] = md5($_POST['password']);
		$data['deviceid'] = $_POST['deviceid'];
		$data['date_created'] = date("Y/m/d h:i:s a");
		$select_data = $db->getData('SELECT `username`,`email` FROM `user_master` WHERE `username` = "'.$data['username'].'" OR  `email` = "'.$data['email'].'"');
		$response = '';
		if(!empty($select_data) && ($select_data[0]['username'] == $data['username'] || $select_data[0]['email'] == $data['email']))
		{
			if($select_data[0]['username'] == $data['username'] && $select_data[0]['email'] == $data['email'])
			{
				$response['msg'] = 'User name and email already exist';
				$response['flg'] = '0';
			}
			else
			{
				if($select_data[0]['username'] == $data['username'])
				{
					$response['msg'] = 'User name is already exist';
					$response['flg'] = '0';
				}
				if($select_data[0]['email'] == $data['email'])
				{
					$response['msg'] = 'User email is already exist';
					$response['flg'] = '0';
				}
			}
		}
		else
		{
			$db->mysqlInsert($data, 'user_master');
			$response['user_id'] = mysql_insert_id();
			$response['msg'] = 'User registeration successfuly';
			$response['flg'] = '1';
		}	
	}
	else
	{
		$response['msg'] = 'Basic Params Missing for registration';
		$response['flg'] = '0';
	}
	echo json_encode($response);exit;
}
if($option == 'login')
{
	if($_POST['username'] && $_POST['password'])
	{
		$data['username'] = $_POST['username'];
		$data['password'] = md5($_POST['password']);
		$response = '';
		$set['is_login'] = '1';
		$set['date_modified'] = date("Y/m/d h:i:s a");
		$uid = $db->mysqlSelect(array('uid','date_modified'),'user_master',$data);
		if($uid[0]['uid']){
			if($db->mysqlUpdate($set, 'user_master', $data))
			{
		
				$response['msg'] = 'User Login Successfully';
				$response['flg'] = '1';
				$response['uid'] = $uid[0]['uid'];
				$response['date_modified'] = $uid[0]['date_modified'];
			}
		}
		else
		{
			$response['msg'] = 'User Login facing a problem';
			$response['flg'] = '0';
		}	
	}
	else 
	{
		$response['msg'] = 'Basic Params Missing for Login';
		$response['flg'] = '0';
	}
	echo json_encode($response);exit;
}
if($option == 'logout')
{
	if($_POST['uid']){
		$data['uid'] = $_POST['uid'];
		$response = '';
		$set['is_login'] = '0';
		$uid = $db->mysqlSelect(array('uid'),'user_master',$data);
		if($uid[0]['uid']){
			if($db->mysqlUpdate($set, 'user_master', $data))
			{
				$response['msg'] = 'User Logout Successfully';
				$response['flg'] = '1';
			}
			else
			{
				$response['msg'] = 'User Logout facing a problem';
				$response['flg'] = '0';
			}
		}
		else
		{
			$response['msg'] = 'User Logout facing a problem';
			$response['flg'] = '0';
		}	
	}
	else
	{
		$response['msg'] = 'Basic Params Missing for Logout';
		$response['flg'] = '0';
	}
	echo json_encode($response);exit;
}
if($option == 'profile')
{
	if($_POST['uid']){
		$data['uid'] = $_POST['uid'];
		$user = $db->mysqlSelect('','user_master',$data);
		if($user[0]['uid']){
			$response = $user[0];
		}
		else
		{
			$response['msg'] = 'User not found';
			$response['flg'] = '0';
		}
	}
	else
	{
		$response['msg'] = 'Basic Params Missing for Logout';
		$response['flg'] = '0';
	}
	echo json_encode($response);exit;
}
if($option == 'add_media')
{
	ini_set('upload_max_filesize', '10M');
	ini_set('post_max_size', '10M');
	ini_set('max_input_time', 300);
	ini_set('max_execution_time', 300);
	$response = '';
	foreach($_SERVER as $name => $value)
	{
		if (substr($name, 0, 5) == 'HTTP_'){
			$name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
			$headers[$name] = $value;
		}
	}
	if(!empty($headers['uid']) && !empty($headers['Imagename'])){
		$image_name = $headers['Imagename'];
		if(is_dir(BASE_DIR.DS.'uploads')){
			mkdir(BASE_DIR.DS.'uploads');
		}
		if(is_dir(BASE_DIR.DS.'uploads'.DS.$headers['uid'])){
			mkdir(BASE_DIR.DS.'uploads'.DS.$headers['uid']);
		}
		$upload_mode = $headers['Mode'];
		
		$putdata = fopen("php://input", "r");
		
		/* Open a file for writing */
		$fp = fopen(BASE_DIR.DS.'uploads'.DS.$headers['uid'].DS.$image_name, $upload_mode);
		
		
		/* Read the data 1 KB at a time
		 and write to the file */
		while ($data = fread($putdata, 1024)){
			fwrite($fp, $data);
			//  echo $data;
		}
		/* Close the streams */
		fclose($fp);
		fclose($putdata);
		$data['uid'] = $headers['uid'];
		$data['media_path'] = BASE_DIR.DS.'uploads'.DS.$headers['uid'].DS.$image_name;
		$data['updated_date'] = date("Y/m/d h:i:s a");
		$data['media_type'] = $headers['media_type'];
		if($db->mysqlInsert($data, 'user_media'))
		{
			$response['msg'] = 'Image Upload successfully';
			$response['flg'] = '1';
		}else{
			$response['msg'] = 'Some Problem in image upload';
			$response['flg'] = '0';
		}	
	}else{
		$response['msg'] = 'Missing basic params';
		$response['flg'] = '0';
	}
	echo json_encode($response);exit;
}
if($option == 'update_profile')
{
	$data = '';
	$where = '';
	if($_POST['likes'])
	{
		$data['likes'] = $_POST['likes'];
	}
	if($_POST['dislikes'])
	{
		$data['dislikes'] = $_POST['dislikes'];
	}
	if($_POST['fav_bars'])
	{
		$data['fav_bars'] = $_POST['fav_bars'];
	}
	if($_POST['dob'])
	{
		$data['dob'] = date("Y/m/d",strtotime($_POST['dob']));
	}
	if($_POST['status'])
	{
		$data['status'] = $_POST['status'];
	}
	if($_POST['gender'])
	{
		$data['gender'] = $_POST['gender'];
	}
	if($_POST['password'])
	{
		$data['password'] = md5($_POST['password']);
	}
	if($_POST['deviceid'])
	{
		$data['deviceid'] = $_POST['deviceid'];
	}
	if($_POST['is_shown_to_other'])
	{
		$data['is_shown_to_other'] = $_POST['is_shown_to_other'];
	}
	if($_POST['about_me'])
	{
		$data['about_me'] = $_POST['about_me'];
	}
	if($_POST['firstname'])
	{
		$data['firstname'] = $_POST['firstname'];
	}
	if($_POST['lastname'])
	{
		$data['lastname'] = $_POST['lastname'];
	}
	if($_POST['tagline'])
	{
		$data['tagline'] = $_POST['tagline'];
	}
	if($_POST['uid']){
		$where['uid'] = $_POST['uid']; 
	}
	if($data && $where)
	{
		if($db->mysqlUpdate($data, 'user_master', $where))
		{
			$response['msg'] = 'User Updated Successfuly';
			$response['flg'] = '1';
		}
		else
		{
			$response['msg'] = 'User Update facing a problem';
			$response['flg'] = '0';
		}		
	}
	else
	{
		$response['msg'] = 'Missing basic params for Update';
		$response['flg'] = '0';
	}
	echo json_encode($response);exit;
}
if($option == 'placebook'){
	$result = '';
	if($_POST['uid'] && $_POST['lat'] && $_POST['lon'] && $_POST['place_info'])
	{
		$data['uid'] = $_POST['uid'];
		$data['lat'] = $_POST['lat'];
		$data['lon'] = $_POST['lon'];
		$data['place_info'] = $_POST['place_info'];
		$data['noted_date_time'] = date("Y/m/d h:i:s a");;
		if($db->mysqlInsert($data, 'book_master'))
		{
			$result['msg'] = 'Book Info Updated';
			$result['flg'] = '1';
			$result['id'] = mysql_insert_id();
		}
		else
		{
			$result['msg'] = 'Problem in Book Info Updation';
			$result['flg'] = '0';
		}
	}
	else
	{
		$result['msg'] = 'Missing basic params';
		$result['flg'] = '0';
	}
	echo json_encode($result);exit;
}
if($option == 'proximity'){
	$result = '';
	$latitude = $_POST['lat'];
	$longitude = $_POST['lon'];
	$query = 'select `uid`, `lat`, `lon`, (6373* acos (cos ( radians( `lat` ) )* cos( radians( "'.$latitude.'" ) )* cos( radians("'.$longitude .'") - radians( lon ) )+ sin ( radians( lat ) )* sin( radians("'.$latitude.'" ) ))) AS distance FROM book_master WHERE noted_date_time BETWEEN DATE_ADD(NOW(), INTERVAL -12 HOUR) AND NOW()';
	$result = $db->getData($query);
	if($result[0])
	{
		echo json_encode($result);exit;
	}
	else
	{
		$result['msg'] = 'Book info is not Available';
		$result['flg'] = '0';
		echo json_encode($result);exit;
	}
}
if($option == 'delete_plcbook'){
	$result = '';
	if($_POST['id']){
		$condition['id'] = $_POST['id'];
		
		if($db->mysqlDelete('book_master', $condition))
		{
			$result['msg'] = 'Book info is Removed';
			$result['flg'] = '1';
			echo json_encode($result);exit;
		}else{
			$result['msg'] = 'Book info is not Removed';
			$result['flg'] = '0';
			echo json_encode($result);exit;
		}	
	}else{
		$result['msg'] = 'Require Params is missing';
		$result['flg'] = '0';
		echo json_encode($result);exit;
	}
	
	
}