<?php
require_once 'config/connection.php';
require_once 'inc.common.php';
ini_set('upload_max_filesize', '10M');
ini_set('post_max_size', '10M');
ini_set('max_input_time', 300);
ini_set('max_execution_time', 300);
$response = '';
foreach($_SERVER as $name => $value)
{
	if (substr($name, 0, 5) == 'HTTP_'){
		$name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
		$headers[$name] = $value;
	}
}

if(!empty($headers['Uid']) && !empty($headers['Imagename'])){
	$image_name = $headers['Imagename'];
	if(!is_dir(BASE_DIR.DS.'uploads')){
		mkdir(BASE_DIR.DS.'uploads');
	}
	if(!is_dir(BASE_DIR.DS.'uploads'.DS.$headers['Uid'])){
		mkdir(BASE_DIR.DS.'uploads'.DS.$headers['Uid']);
	}
	$upload_mode = $headers['Mode'];
	$putdata = fopen("php://input", "r");
	/* Open a file for writing */
	$fp = fopen(BASE_DIR.DS.'uploads'.DS.$headers['Uid'].DS.$image_name, $upload_mode);
	/* Read the data 1 KB at a time
	 and write to the file */
	while ($data = fread($putdata, 1024)){
		fwrite($fp, $data);
		//  echo $data;
	}
	/* Close the streams */
	fclose($fp);
	fclose($putdata);
	$data['uid'] = $headers['Uid'];
	$data['media_path'] = BASE_URL.'/uploads/'.$headers['Uid'].'/'.$image_name;
	$data['updated_date'] = date("Y/m/d h:i:s a");
	$data['media_type'] = $headers['Media-Type'];
	$data['media_name'] = $image_name;
	if($db->mysqlInsert($data, 'user_media')){
		$db->mysqlUpdate(array('profile_pic'=>mysql_insert_id()), 'user_master', array('uid'=>$headers['Uid']));
		$response['msg'] = 'Image Upload successfully';
		$response['media_path']=$data['media_path'];
		$response['flg'] = '1';
	}else{
		$response['msg'] = 'Some Problem in image upload';
		$response['flg'] = '0';
	}
}
else
{
	$response['msg'] = 'Missing basic params';
	$response['flg'] = '0';
}
echo json_encode($response);exit;
