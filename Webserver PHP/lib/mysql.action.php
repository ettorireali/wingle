<?php
class mysql_action{
	var $url = '';
	var $sql = '';
	var $pageIndex = 1;
	var $rowPerPage = 10;
	var $totalRecords = 0;
	var $totalPages = 0;
	var $startIndex = 0;
	var $endIndex = 0;
	var $nextLink = '';
	var $previousLink = '';
	var $qr_unorder  = false;
	var $pagination = false;
	var $pageIndexVar = 'pid';
	function mysql_action()
	{
		$index = isset($_REQUEST[ $this->pageIndexVar ]) ? $_REQUEST[ $this->pageIndexVar ] : 1;
		$this->pageIndex = ((int) $index == 0) ? 1 : $index;
	}
	function mysqlInsert($datas, $table)
	{
		$count = count($datas);
		$element ='';
		$column='';
		$keys = array_keys($datas);
		$column = implode(', ', $keys);
		$element = implode('", "', $datas);
		$qr = 'INSERT INTO `'.$table.'` ('.$column.') VALUES("'.$element.'")';
		if(mysql_query($qr)){
			return 1;
		}else{
			return 0;
		}
	}
	function mysqlSelect($select = null, $table = null, $wheres = null)
	{
		//echo 'insql='.$wheres;
		//echo $table;exit;
		$count = count($wheres);
		$selectcount = count($select);
		$whereclows ='';
		$selectclows = '';
		$i=0;
		if(!empty($select)){
			$selectclows = implode('`, `', $select);
		}
		if(!empty($wheres))
		{
			foreach($wheres as $key=>$where)
			{
				$i++;
				if($i != $count)
				{
			
					$whereclows .= '`'.$key.'` = "'.$where.'" AND ';
				}
				else
				{
					$whereclows .= '`'.$key.'` = "'.$where.'"';
				}
					
			}
		}
		$qr = 'SELECT ';
		$qr .= ($selectclows)?'`'.$selectclows.'`':'*';
		$qr .= ' FROM `'.$table.'`';
		
		//echo "whereclows=".$whereclows;
		if(!empty($whereclows))
		{
			$qr .= ' WHERE '.$whereclows;
		}
		//echo $qr;exit;
		if($this->qr_unorder){
			$qr .= ' ORDER BY RAND() ';
		} 
		if($this->pagination){
			$this->getTotalRecords($table);
			$this->getTotalPages();
			$this->setPageIndex();
			$this->setStartIndex();
			$this->setEndIndex();
			
			// link for next page
			$this->nextLink = $this->url . '?' . $this->pageIndexVar . '=' . ($this->pageIndex + 1);
			
			// link for previous page
			$this->previousLink = $this->url . '?' . $this->pageIndexVar . '=' . ($this->pageIndex - 1);
			
			$qr .= ' LIMIT ' . $this->startIndex . ', ' .  $this->rowPerPage;
		}
		
		// echo $qr;exit; 
		$result = mysql_query($qr);

		$rescount = mysql_num_rows($result);
		if(!empty($rescount))
		{
			$i = 0;
			while($data = mysql_fetch_assoc($result))
			{
				$newdata[$i] = $data;
				$i++;	
			}
			
			return $newdata;
		}
		else
		{
			return 0;	
		}
	}
	/**
	 * 
	 * @param $data data which need to update
	 * @param $table table on which you need to update4
	 * @param $condition condition for update query
	 */
	function mysqlUpdate($data, $table, $condition)
	{
// 		echo "<pre>";
// 		print_r($condition);
// 		exit;		
		
		$wherecount = count($condition);
		$datacount = count($data);
		$whereclows ='';
		$dataclows = '';
		$i=0;
		
		if(!empty($condition))
		{
			foreach($condition as $key=>$cont)
			{
				$i++;
				if($i != $wherecount)
				{
						
					$whereclows .= '`'.$key.'` = "'.$cont.'" AND ';
				}
				else
				{
					$whereclows .= '`'.$key.'` = "'.$cont.'"';
				}
					
			}
		}
		$i = 0;
		if(!empty($data))
		{
			
			foreach($data as $dkey=>$dt)
			{
				$i++;
				if($i != $datacount)
				{
			
					$dataclows .= '`'.$dkey.'` = "'.$dt.'", ';
				}
				else
				{
					$dataclows .= '`'.$dkey.'` = "'.$dt.'"';
				}
					
			}			
		}
		$qr = 'UPDATE `'.$table.'` SET ';
		$qr .= $dataclows;
		if(!empty($whereclows))
		{
			$qr .= ' WHERE '.$whereclows;
		}
		//echo $qr;exit;
		
		
		if(mysql_query($qr))
		{
			return 1;exit;
		}
		else
		{
			return 0;exit;
		}
	}
	
	/**
	 * 
	 * @param $table: Name of the Table
	 * @param $condition: Codition for deleting record
	 */
	function mysqlDelete($table, $condition)
	{
		$wherecount = count($condition);
		
		$whereclows ='';
		
		$i=0;
		
		if(!empty($condition))
		{
			foreach($condition as $key=>$cont)
			{
				$i++;
				if($i != $wherecount)
				{
		
					$whereclows .= '`'.$key.'` = "'.$cont.'", AND ';
				}
				else
				{
					$whereclows .= '`'.$key.'` = "'.$cont.'"';
				}
					
			}
		}
		
		$qr = 'DELETE FROM `'.$table.'`';
		if(!empty($whereclows))
		{
			$qr .= ' WHERE '.$whereclows;
		}
		//echo $qr;exit;
		if(mysql_query($qr))
		{
			return 1;exit;
		}
		else
		{
			return 0;exit;
		}		
	}
	
	function getData($query)
	{
		$result = mysql_query($query);
		$rescount = mysql_num_rows($result);
		if(!empty($rescount))
		{
			$i = 0;
			while($data = mysql_fetch_assoc($result))
			{
				$newdata[$i] = $data;
				$i++;
			}
			
			return $newdata;
		}
		else
		{
			return '';
		}		
	}
	function setExicuteQuery($query){
		if(mysql_query($query)){
			return true;
		}else{
			return false;
		}
	}
	function loadResultArray($result)
	{
		//echo '<pre>';print_r($result);
		foreach($result as $key=> $res)
		{
			$k = array_keys($res);
			//print_r($k);
			$response[] = $res[$k[0]];
		}
		return $response;
	}
	function getRangeText()
	{
		if($this->totalRecords > 0)
		{
			$rangeStart = $this->startIndex + 1;
	
			if(($this->totalRecords < $this->rowPerPage) || ($this->pageIndex == $this->totalPages))
			{
				$rangeEnd = $this->totalRecords;
			}
			else
			{
				$rangeEnd = $this->endIndex;
			}
	
			return 'Showing '  . $rangeStart . ' - ' . $rangeEnd . ' of ' . $this->totalRecords;
		}
	}
	
	
	
	
	function getNumberLinks()
	{
		$links = '';
		$pno=$_REQUEST[ $this->pageIndexVar ];
		//echo '<pre>';print_r($this->totalPages);
		$links .= '<ul class="pagination">';
		for($i = 1; $i <= $this->totalPages; $i++)
		{
			if(empty($pno) && $i ==1)
			{
				$links .= '<li><a href="javascript:void(0)" class="p_selected">' . $i . '</a></li>';
			}elseif($pno == $i){
				$links .= '<li><a href="javascript:void(0)" class="p_selected">' . $i . '</a></li>';
			}else{
				$links .= '<li><a href="' . $this->url . '?'  . $this->pageIndexVar . '=' . $i .'">' . $i . '</a></li>';
			}
		}
		$links .= '</ul>';
		return $links;
	}
	function getTotalRecords($tablename)
	{
		//counting total number of rows
		$sql = 'SELECT COUNT(*) FROM (' . $tablename . ')';
		$query = mysql_query($sql);
		$row = mysql_fetch_row($query);
		$this->totalRecords = $row[0];
		return $this->totalRecords;
	}
	function getTotalPages()
	{
		$this->totalPages = (int) ceil($this->totalRecords/$this->rowPerPage);
		return $this->totalPages;
	}
	function setPageIndex()
	{
		if($this->pageIndex > $this->totalPages)
		{
			$pageIndex = $totalPages;
		}
	}
	function setStartIndex()
	{
		$this->startIndex = ($this->pageIndex - 1) * $this->rowPerPage;
	}
	function setEndIndex()
	{
		if($this->totalRecords > $this->rowPerPage)
		{
			$this->endIndex = $this->startIndex + $this->rowPerPage;
		}
		else
		{
			$endIndex = $this->totalRecords;
		}
	
		if($this->pageIndex == $this->totalPages)
		{
			if($this->totalRecords < $this->totalPages * $this->rowPerPage)
			{
				$endIndex = $this->totalRecords;
			}
		}
	}
}
