<?php
require_once 'config/connection.php';
require_once 'inc.common.php';
$response = '';
if($_POST['mid'] && $_POST['uid'])
{
	$data['profile_pic'] = $_POST['mid'];
	$where['uid'] = $_POST['uid'];
	if($db->mysqlUpdate($data, 'user_master', $where)){
		$response['msg'] = 'User profile pic updated successfully';
		$response['flg'] = '1';
	}else{
		$response['msg'] = 'Problem in User profile pic updation';
		$response['flg'] = '0';
	}
}
else
{
	$response['msg'] = 'Basic Params Missing';
	$response['flg'] = '0';
}
echo json_encode($response);exit;