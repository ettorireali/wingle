<?php
require_once 'config/connection.php';
require_once 'inc.common.php';
$data = '';
$where = '';
$response = '';
if($_POST['likes'])
{
	$data['likes'] = $_POST['likes'];
}
if($_POST['dislikes'])
{
	$data['dislikes'] = $_POST['dislikes'];
}
if($_POST['fav_bars'])
{
	$data['fav_bars'] = $_POST['fav_bars'];
}
if($_POST['dob'])
{
	$data['dob'] = date("Y/m/d",strtotime($_POST['dob']));
}
if($_POST['status'])
{
	$data['status'] = $_POST['status'];
}
if($_POST['gender'])
{
	$data['gender'] = $_POST['gender'];
}
if($_POST['password'])
{
	$data['password'] = md5($_POST['password']);
}
if($_POST['deviceid'])
{
	$data['deviceid'] = $_POST['deviceid'];
}
if($_POST['is_shown_to_other'])
{
	$data['is_shown_to_other'] = $_POST['is_shown_to_other'];
}
if($_POST['about_me'])
{
	$data['about_me'] = $_POST['about_me'];
}
if($_POST['firstname'])
{
	$data['firstname'] = $_POST['firstname'];
}
if($_POST['lastname'])
{
	$data['lastname'] = $_POST['lastname'];
}
if($_POST['tagline'])
{
	$data['tagline'] = $_POST['tagline'];
}
if($_POST['privacy_pref']){
	$data['privacy_pref'] = addslashes($_POST['privacy_pref']);
}
if($_POST['uid']){
	$where['uid'] = $_POST['uid'];
}

if($data && $where)
{
	//echo '<pre>';print_r($data);print_r($where);exit;
	if($db->mysqlUpdate($data, 'user_master', $where))
	{
		$response['msg'] = 'User Updated Successfuly';
		$response['flg'] = '1';
	}
	else
	{
		$response['msg'] = 'User Update facing a problem';
		$response['flg'] = '0';
	}
}
else
{
	$response['msg'] = 'Missing basic params for Update';
	$response['flg'] = '0';
}
echo json_encode($response);exit;
