<?php
require_once 'config/connection.php';
require_once 'inc.common.php';
$response = '';
if(!empty($_POST['uid']) && !empty($_POST['registration_id'])){
	$data['registration_id'] = $_POST['registration_id'];
	$condition['uid'] = $_POST['uid'];
	if($db->mysqlUpdate($data, 'user_master', $condition)){
		$response['msg'] = 'Registration id update successfuly';
		$response['flg'] = '1';
	}else{
		$response['msg'] = 'Registration id not updating';
		$response['flg'] = '0';
	}
}else{
	$response['msg'] = 'Basic Params Missing';
	$response['flg'] = '0';
}
echo json_encode($response);exit;