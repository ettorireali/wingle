<?php
require_once 'config/connection.php';
require_once 'inc.common.php';
$response = '';
if($_POST['mid'])
{
	$data['mid'] = $_POST['mid'];
	$check = $db->mysqlSelect(array('uid'), 'user_master', array('profile_pic'=>$data['mid']));
	if($check[0]['uid']){
		$media = $db->mysqlSelect(array('media_name', 'uid'),'user_media',$data);
		if(unlink(BASE_DIR.DS.'uploads'.DS.$media[0]['uid'].DS.$media[0]['media_name']))
		{
			if($db->mysqlDelete('user_media', $data))
			{
				$response['msg'] = 'Media deleted successfuly';
				$response['flg'] = '1';
			}
			else
			{
				$response['msg'] = 'Problem in Deleting Media from database';
				$response['flg'] = '0';
			}
		}
		else
		{
			$response['msg'] = 'Problem in Deleting Media file from server';
			$response['flg'] = '0';
		}	
	}else{
		$response['msg'] = 'Picture is already set as Profile pic';
		$response['flg'] = '0';
	}
}
else
{
	$response['msg'] = 'Basic Params Missing';
	$response['flg'] = '0';
}
echo json_encode($response);exit;