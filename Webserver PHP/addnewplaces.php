<?php
require_once 'config/connection.php';
require_once 'inc.common.php';
$result = '';
if($_POST['uid'] && $_POST['lat'] && $_POST['lon'] && $_POST['placetitle'] && $_POST['description'])
{
	$data['uid'] = $_POST['uid'];
	$data['lat'] = $_POST['lat'];
	$data['lon'] = $_POST['lon'];
	$data['place_title'] = $_POST['placetitle'];
	$data['place_info'] = $_POST['description'];
	$data['date_created'] = date("Y/m/d H:i:s a");
	if($db->mysqlInsert($data, 'random_places'))
	{
		$result['msg'] = 'You have added a new place';
		$result['flg'] = '1';
		$result['id'] = mysql_insert_id();
		
	}
	else
	{
		$result['msg'] = 'Problem in Book Info Updation';
		$result['flg'] = '0';
	}
}
else
{
	$result['msg'] = 'Missing basic params';
	$result['flg'] = '0';
}
echo json_encode($result);exit;
