<?php
require_once 'config/connection.php';

$array_login = array('home.php','logout.php','password_change.php','profile.php');

$file = basename($_SERVER['PHP_SELF']);
require_once 'inc.common.php';

if(in_array($file,$array_login) && empty($_SESSION['front_userid'])):
header('Location: index.php');exit;
endif;
